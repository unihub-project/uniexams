**_UNIEXAMS_**

A web application for upload and answer exam topics of universities.


**API Documentation**

_Authentication (Using Passport)_

**POST** /api/users

Creates new user

request (application/json) : **email** : String, **password** : String, **confirmPassword** : String, defaultDepartment : Department

response (application/json) : 
 
 HTTP 200 (OK) 
 
    token : [String],
    user : [User]
 
 
 HTTP 422 (Unprocessable Entity) IF email/password/confirmPassword are null or user already exists
 
      title: [String],
      error: {message: [String])
 
 **POST** /api/users/authenticate
 
 Authenticate a user
 
 request (application/json) : **email** : String, **password** : String
 
 response (application/json) : 
  
  HTTP 200 (OK) 
  
     token : [String],
     user : [User]
  
  
  HTTP 422 (Unproccessable Entity) IF email/password are null or wrong
  
  HTTP 401 (Unauthorized) IF isConfirmed is false
  

**PATCH** /api/users/:id

Updaters user's fields

request : email : String or/and (oldPassword: String, newPasssword : String

 response (application/json) : 
  
  HTTP 200 (OK) 
  
     title: [String],
     obj: [User]
     
     
  HTTP 401 (Not Found) IF requested user is not authorized to update the user
  
  
  HTTP 404 (Not Found) IF user not found
  
  
  HTTP 422 (Unproccessable Entity) IF oldPassword is wrong
