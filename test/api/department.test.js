'use strict';

const expect = require('chai').expect,
    University = require('../../app/models/university'),
    Department = require('../../app/models/department'),
    server = require('../../bin/www'),
    request = require('superagent'),
    // adminToken = require('./tokens.test').adminToken,
    userToken = require('./tokens.test').userToken,
    i18n = require('i18n'),
    assert = require('chai').assert,
    mongoose = require('mongoose'),
    config = require('./data.json'),
    init = require('./init'),
    async = require('async');

describe('Testing Department (department.controller)', function () {
    const baseURL = 'http://localhost:4001';
    const adminToken = config.users.admin.token;

    //Before ALL Department  tests, clean the collection
    //and make a existing University
    before(function (done) {
        server.listen(4001, function (err) {
            if (err)  return done(err);
            async.series([
                    function (cb) {
                        init.saveUniversities(cb);
                    },
                    function (cb) {
                        init.saveDepartments(cb);
                    }
                ],
                function (err) {
                    if (err) done(err);
                    done();
                });
        });
    });


//After ALL department tests, clean the collection
    after(function (done) {
        University.remove({})
            .then(function () {
                Department.remove({});
            })
            .then(function () {
                server.close(function (err) {
                    if (err) return done(err);
                    done();
                });
            })
            .catch(function (err) {
                return done(err);
            });
    });


    describe('Testing createDepartment ', function () {

        after(function (done) {

            Department.remove({shortName: "TD"})
                .then(function () {
                    done();
                })
                .catch(function (err) {
                    return done(err);
                });
        });

        it('should NOT create a Department, name missing', function (done) {
            request
                .post(baseURL + '/api/departments/university/' + config.universities.existingUniversity1._id)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    shortName: 'TD',
                    information: 'Created for testing'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('DEPARTMENT_NO_REQUIRED_FIELDS'));
                    assert.equal(res.status, 422);
                    done();
                });


        });

        it('should NOT create a Department, shortName missing', function (done) {
            request
                .post(baseURL + '/api/departments/university/' + config.universities.existingUniversity1._id)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'Testing Dep',
                    information: 'Created for testing'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('DEPARTMENT_NO_REQUIRED_FIELDS'));
                    assert.equal(res.status, 422);
                    done();
                });


        });

        it('should create a new Department', function (done) {
            request
                .post(baseURL + '/api/departments/university/' + config.universities.existingUniversity1._id)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'Testing Dep',
                    shortName: 'TD',
                    information: 'Created for testing'
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 201);
                    assert.equal(res.body.title, i18n.__('DEPARTMENT_CREATE_SUCCESS_TITLE'));
                    done();
                });
        });

        it('should NOT create an existing Deparment', function (done) {
            request
                .post(baseURL + '/api/departments/university/' + config.universities.existingUniversity1._id)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send(config.departments.existingDepartment1)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('DEPARTMENT_CREATE_ERROR_EXISTS '));
                    assert.equal(res.status, 422);
                    done();
                });


        });

        it('should NOT create a new Department,wrong Uni ID', function (done) {
            request
                .post(baseURL + '/api/departments/university/' + config.wrongID)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'Testing Dep',
                    shortName: 'TD',
                    information: 'Created for testing'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });
        });
    });

    describe('Testing getDepartmentList method', function () {

        it('should get all departments', function (done) {
            request
                .get(baseURL + '/api/departments?page=1')

                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.length, config.departments.totalDepartments);
                    done();
                });

        });

        it('should get all departments for a  university', function (done) {
            request
                .get(baseURL + '/api/departments?page=1&uniId=' + config.universities.existingUniversity1._id)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.length, 1);
                    done();
                });

        });

        it('should get all departments based on name', function (done) {
            request
                .get(baseURL + '/api/departments?page=1&name=1')
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.length, 1);
                    done();
                });
        });

        it('should get all departments based on shortname', function (done) {
            request
                .get(baseURL + '/api/departments?page=1&shortName=1')
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.length, 1);
                    done();
                });
        });

        it('should get the map of all departments of university', function (done) {
            request
                .get(baseURL + '/api/departments?uniId='+config.universities.existingUniversity1._id)

                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.title, i18n.__('DEPARTMENT_GET_MAP_SUCCESS_TITLE'));
                    assert.equal(res.body.obj.length, 1);
                    done();
                });

        });


    });

    describe('Testing getDepartmentById method', function () {

        it("should not find the department, wrong id", function (done) {
            request
            //random ID, hope to be always not found
                .get(baseURL + '/api/departments/' + config.wrongID)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('DEPARTMENT_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });

        });


        it('should find the department', function (done) {

            request
                .get(baseURL + '/api/departments/' + config.departments.existingDepartment1._id)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    // assert.equal(res.body._id,config.existingDepartment1._id )
                    done();
                });

        });

    });

    describe('Testing updateDepartment method', function () {
        let debToBeUpdate;
        before(function (done) {
            debToBeUpdate = new Department(config.departments.departmentToBeUpdate);
            debToBeUpdate.save(function (err) {
                if (err) {
                    return next(err);
                }
                done();
            });
        });

        after(function (done) {
            Department.findByIdAndRemove(debToBeUpdate._id, function (err) {
                if (err) {
                    return next(err);
                }
                done();
            });
        });

        it('should update a department', function (done) {


            request
                .patch(baseURL + '/api/departments/' + config.departments.departmentToBeUpdate._id)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'New Name',
                    shortName: 'new short name',
                    information: 'new info',
                    universityId: config.departments.departmentToBeUpdate.university
                })
                .end(function (err, res) {

                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.title, i18n.__('DEPARTMENT_UPDATE_TITLE'));
                    assert.equal(res.body.obj.name, 'New Name');
                    assert.equal(res.body.obj.shortName, 'new short name');
                    assert.equal(res.body.obj.information, 'new info');
                    done();
                });


        });

        it('should NOT update a department, UNIVERSITY not found', function (done) {
            request
                .patch(baseURL + '/api/departments/' +  config.departments.departmentToBeUpdate._id)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'New Name',
                    shortName: 'new short name',
                    information: 'new info',
                    universityId: config.wrongID
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_NOT_FOUND'));
                    assert.equal(res.status, 404);

                    done();
                });


        });
        it('should NOT update a department, DEPARTMENT not found', function (done) {
            request
                .patch(baseURL + '/api/departments/' + config.wrongID)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'New Name',
                    shortName: 'new short name',
                    information: 'new info',
                    universityId: config.departments.departmentToBeUpdate.university
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('DEPARTMENT_NOT_FOUND'));
                    assert.equal(res.status, 404);

                    done();
                });


        });


    });

    describe('Testing deleteDepartment method', function () {

        let depToBeDeleted;
        before(function (done) {

            depToBeDeleted = new Department(config.departments.departmentToBeDeleted);
            depToBeDeleted.save(function (err) {
                if (err) {
                    return done(err);
                }
                done();
            });
        });

        it('should delete get a department', function (done) {

            request
                .del(baseURL + '/api/departments/' + depToBeDeleted._id)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.title, i18n.__('DEPARTMENT_DELETE_SUCCESS_TITLE'));
                    done();
                });
        });


        it('should NOT delete get a department, wrong ID', function (done) {
            request
                .del(baseURL + '/api/departments/' + config.wrongID)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('DEPARTMENT_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });
        });

    });

    // describe('Testing getMyDepartment method', function () {
    //     before(done){
    //
    //     }
    //
    //     it('should get the my department', function(done){
    //
    //     })
    // })

    describe('Testing getAllDepartmentsOfUniversity method', function () {
        it('should get all departments for a university', function (done) {

            request
                .get(baseURL + '/api/departments/university/' + config.universities.existingUniversity1._id)
                .set('Authorization', userToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.departments.length, 1);
                    done();
                });
        })
    })
});