'use strict';

// require('./profile.test');

const University = require('../../app/models/university'),
    server = require('../../bin/www'),
    request = require('superagent'),
    // adminToken = require('./tokens.test.js').adminToken,
    userToken = require('./tokens.test.js').userToken,
    i18n = require('i18n'),
    assert = require('chai').assert,
    mongoose = require('mongoose'),
    config = require('./data.json'),
    init = require('./init'),
    async = require('async');

describe('Testing University (university.controller)', function () {

    const baseURL = 'http://localhost:4001';
    const adminToken = config.users.admin.token;
    //Before ALL university tests, clean the collection
    //and make a existing University
    before(function (done) {
        server.listen(4001, function (err) {
            async.series([
                    function (cb) {
                        init.saveUniversities(cb);
                    },
                ],
                function (err) {
                    if (err) done(err);
                    done();
                });
        })
    });


//After ALL university tests, clean the collection
    after(function (done) {
        University.remove({})
            .then(function () {
                server.close(function (err) {
                    if (err) return done(err);
                    done();
                });
            })
            .catch(function (err) {
                return done(err);
            });
    });

    describe('Testing createUniversity ', function () {

        it('should NOT create a university, name missing', function (done) {
            request
                .post(baseURL + '/api/universities')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    shortName: 'TU',
                    information: 'Created for testing'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_CREATE_ERROR_NAME '));
                    assert.equal(res.status, 422);
                    done();
                });


        });

        it('should NOT create a university, shortName missing', function (done) {
            request
                .post(baseURL + '/api/universities')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'Testing Uni',
                    information: 'Created for testing'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_CREATE_ERROR_SHORT_NAME '));
                    assert.equal(res.status, 422);
                    done();
                });


        });

        it('should create a new university', function (done) {
            request
                .post(baseURL + '/api/universities')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'Testing Uni',
                    shortName: 'TU',
                    information: 'Created for testing'
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 201);
                    assert.equal(res.body.title, i18n.__('UNIVERSITY_CREATE_SUCCESS_TITLE'));

                    University.findByIdAndRemove(res.body.obj._id, function (err) {
                        if (err) {
                            return next(err);
                        }
                        done();
                    });


                });

        });

        it('should NOT create an existing university', function (done) {
            request
                .post(baseURL + '/api/universities')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send(config.universities.existingUniversity1)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_CREATE_ERROR_EXISTS '));
                    assert.equal(res.status, 422);
                    done();
                });


        });
    });

    describe('Testing getAllUniversities method', function () {
        it('should get all universities', function (done) {
            request
                .get(baseURL + '/api/universities?page=1')

                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.length, config.universities.totalUniversities);
                    done();
                });

        });

        it('should get all universities based on name', function (done) {
            request
                .get(baseURL + '/api/universities?page=1&name=existing')
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.length, config.universities.totalUniversities);
                    done();
                });
        })

        it('should get all universities based on shortname', function (done) {
            request
                .get(baseURL + '/api/universities?page=1&shortName=existing')
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.length, config.universities.totalUniversities);
                    done();
                });
        })

        it('should get the map of all universities', function (done) {
            request
                .get(baseURL + '/api/universities')

                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.title, i18n.__('UNIVERSITY_GET_MAP_SUCCESS_TITLE'));
                    assert.equal(res.body.obj.length, config.universities.totalUniversities);
                    done();
                });

        });
    });

    describe('Testing getUniversityById method', function () {
        it('should NOT get the Test university', function (done) {

            request
                .get(baseURL + '/api/universities/' + config.wrongID)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });

        });


        it('should get the Test university', function (done) {

            request
                .get(baseURL + '/api/universities/' + config.universities.existingUniversity1._id)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    done();
                });

        });

    });

    describe('Testing deleteUniversity method', function () {
        before(function (done) {

            var uniToBeDeleted = new University(config.universities.uniToBeDeleted);
            uniToBeDeleted.save()
                .then(function () {
                    done();
                })
                .catch(function (err) {
                    return done(err);
                });
        });

        it('should delete a university', function (done) {

            request
                .del(baseURL + '/api/universities/' + config.universities.uniToBeDeleted._id)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.title, i18n.__('UNIVERSITY_DELETE_TITLE'));
                    done();
                });
        });


        it('should NOT delete get a university, wrong ID', function (done) {
            request
                .del(baseURL + '/api/universities/' + config.wrongID)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });
        });

    });

    describe('Testing updateUniversity ', function () {

        before(function (done) {
            var uniToBeDeleted = new University(config.universities.uniToBeUpdate);
            uniToBeDeleted.save()
                .then(function () {
                    done();
                })
                .catch(function (err) {
                    return done(err);
                });
        });

        it('should update a university', function (done) {


            request
                .patch(baseURL + '/api/universities/' + config.universities.uniToBeUpdate._id)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'New Name',
                    shortName: 'new short name',
                    information: 'new info'
                })
                .end(function (err, res) {

                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.title, i18n.__('UNIVERSITY_UPDATE_TITLE'));
                    assert.equal(res.body.obj.name, 'New Name');
                    assert.equal(res.body.obj.shortName, 'new short name');
                    assert.equal(res.body.obj.information, 'new info');
                    done();
                });


        });

        it('should NOT update a university, not found', function (done) {


            request
                .patch(baseURL + '/api/universities/' + config.wrongID)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    name: 'New Name',
                    shortName: 'new short name',
                    information: 'new info'
                })
                .end(function (err, res) {

                    assert.equal(err.response.body.error.message, i18n.__('UNIVERSITY_NOT_FOUND'));
                    assert.equal(res.status, 404);

                    done();
                });


        });
    });
});