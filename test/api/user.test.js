/**
 * Created by bdrosatos on 28/9/2016.
 */
'use strict';

const assert = require('chai').assert,
    i18n = require('i18n'),
    request = require('superagent'),
    server = require('../../bin/www'),
    config = require('../../app/config/app'),
    jwt = require('jsonwebtoken'),
    userToken = require('./tokens.test.js').userToken,
    adminToken = require('./tokens.test.js').adminToken,
    User = require('../../app/models/user');

describe('Testing User (auth.controller)', function () {

    const baseURL = 'http://localhost:4001';
    let userId;

    before(function (done) {
        server.listen(4001, function (err) {
            if (err) return done();
            done();
        })
    });

    after(function (done) {
        User.findByIdAndRemove(userId, function (err) {
            if (err) done(err);
            done();
        });
    });

    describe('Testing register', function () {

        it('should NOT create a user, email missing', function (done) {
            request
                .post(baseURL + '/api/users')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    password: '123456'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('NO_EMAIL'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should NOT create a user, password missing', function (done) {
            request
                .post(baseURL + '/api/users')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'test@test.com'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('NO_PASSWORD'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should create a user', function (done) {
            request
                .post(baseURL + '/api/users')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'testing@test.com',
                    password: '123456'
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 201);
                    assert.equal(res.body.obj.user.email, 'testing@test.com');
                    assert.notEqual(res.body.obj.user.password, '123456');

                    User.findOne({email: 'testing@test.com'}, function (err, user) {
                        if (err) done(err);

                        if (!user) done('User Not Found');

                        userId = user._id;

                        user.comparePassword('123456', function (err, isMatch) {
                            if (err) done(err);
                            assert.isTrue(isMatch);
                            done();
                        });

                    })
                });
        });

        it('should not create a user, email exists', function (done) {
            request
                .post(baseURL + '/api/users')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'testing@test.com',
                    password: '123456'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('EXIST_EMAIL'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

    });

    describe('Testing confirmUser', function () {
        let userId;
        let confirmToken = 'AA1234';
        before(function (done) {
            let user = new User({
                email: 'testeing2@testing.com',
                password: '2010020',
                confirmToken : confirmToken
            });

            user.save(function (err, user) {
                if (err) done(err);

                userId = user._id;
                done();
            })

        });

        after(function (done) {
            User.findByIdAndRemove(userId, function (err) {
                if (err) done(err);
                done();
            });
        });


        it('should NOT confirm user, no user found with this token', function (done) {
            request
                .patch(baseURL + '/api/users/confirm/DJHD22I')
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('NO_USER_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });
        });

        it('should confirm user', function (done) {
            request
                .patch(baseURL + '/api/users/confirm/' + confirmToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    done();
                });
        });

        it('should NOT confirm user, user already confirmed', function (done) {
            request
                .patch(baseURL + '/api/users/confirm/' + confirmToken)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('ACCOUNT_CONFIRMED'));
                    assert.equal(res.status, 422);
                    done();
                });
        });


    });

    describe('Testing login', function () {

        it('should NOT login user, wrong credentials (email)', function (done) {
            request
                .post(baseURL + '/api/users/authenticate')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'foo@test.com',
                    password: '123456'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('LOGIN_ERROR_WRONG_CREDENTIALS'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should NOT login user, wrong credentials (password)', function (done) {
            request
                .post(baseURL + '/api/users/authenticate')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'user@user.com',
                    password: '12345678'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('LOGIN_ERROR_WRONG_CREDENTIALS'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should login user with role Undergraduate', function (done) {
            request
                .post(baseURL + '/api/users/authenticate')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'user@user.com',
                    password: '123456'
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.user.email, 'user@user.com');
                    assert.equal(res.body.obj.user.role, 'Undergraduate');
                    done();
                });
        });

        it('should login user with role Admin', function (done) {
            request
                .post(baseURL + '/api/users/authenticate')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'admin@admin.com',
                    password: '123456'
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);
                    assert.equal(res.body.obj.user.email, 'admin@admin.com');
                    assert.equal(res.body.obj.user.role, 'Admin');
                    done();
                });
        });

    });

    describe('Testing updateUser', function () {
        let updateUserId;
        let jwtToken;
        before(function (done) {
            let user = new User({
                email: 'testeing3@testing.com',
                password: '123456',
                isConfirmed : true
            });

            user.save(function (err, user) {
                if (err) done(err);

                updateUserId = user._id;
                jwtToken = 'JWT ' + jwt.sign({
                        _id: user._id,
                        email: user.email,
                        role: user.role,
                        profile: user.profile
                    }, config.secret);
                done();
            })

        });

        after(function (done) {
            User.findByIdAndRemove(updateUserId, function (err) {
                if (err) done(err);
                done();
            });
        });

        it('should NOT update user, not authorized', function (done) {
            request
                .patch(baseURL + '/api/users/' + updateUserId)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    email: 'testing4@testing.com',
                })
                .end(function (err, res) {
                    assert.equal(err.message,
                        i18n.__('WORD_UNAUTHORIZED'));
                    assert.equal(res.status, 401);
                    done();
                });
        });

        it('should NOT update user password, old password error', function (done) {
            request
                .patch(baseURL + '/api/users/' + updateUserId)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', jwtToken)
                .send({
                    oldPassword: 'foofoofoo',
                    newPassword: '0349308043',
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message,
                        i18n.__('OLD_PASSWORD_ERROR'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should update user email', function (done) {
            request
                .patch(baseURL + '/api/users/' + updateUserId)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', jwtToken)
                .send({
                    email: 'testing4@testing.com',
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.user.email, 'testing4@testing.com');
                    assert.equal(res.status, 200);
                    done();
                });
        });

        it('should update user password by same user', function (done) {
            request
                .patch(baseURL + '/api/users/' + updateUserId)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', jwtToken)
                .send({
                    oldPassword: '123456',
                    newPassword: '0349308043',
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);

                    User.findById(updateUserId, function (err, user) {
                        if (err) done(err);

                        if (!user) done('User Not Found');

                        user.comparePassword('0349308043', function (err, isMatch) {
                            if (err) done(err);
                            assert.isTrue(isMatch);
                            done();
                        });

                    })
                });
        });

        it('should update user password by same admin', function (done) {
            request
                .patch(baseURL + '/api/users/' + updateUserId)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', adminToken)
                .send({
                    oldPassword: '0349308043',
                    newPassword: '123456',
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.status, 200);

                    User.findById(updateUserId, function (err, user) {
                        if (err) done(err);

                        if (!user) done('User Not Found');

                        user.comparePassword('123456', function (err, isMatch) {
                            if (err) done(err);
                            assert.isTrue(isMatch);
                            done();
                        });

                    })
                });
        });

    });

});