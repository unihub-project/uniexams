/**
 * Created by stavr on 30-Jan-17.
 */

//Checks if tokens need to be updated
require('./updateTokens');

//Tests
require('./university.test');
require('./department.test');
// require('./course.test')
