/**
 * Created by stavros on 18/10/2016.
 */

const expect = require('chai').expect,
    Comment = require('../../app/models/comment'),
    Profile = require('../../app/models/profile'),
    Reply = require('../../app/models/reply'),
    server = require('../../bin/www'),
    request = require('superagent'),
    adminToken = require('./tokens.test.js').adminToken,
    userToken = require('./tokens.test.js').userToken,
    i18n = require('i18n'),
    assert = require('chai').assert,
    mongoose = require('mongoose'),
    async = require('async'),
    User = require('../../app/models/user');

describe('Testing comment ( comment.controller', function () {
    const baseURL = 'http://localhost:4001';
    let replyID;
    let profileID = '580209b024a27c2e6c7ce4ed';

    before(function (done) {
        server.listen(4001, function (err) {
            if (err)  return done(err);
            Comment.remove({}, function (err) {

                if (err)  return done(err);

                const newProfile = new Profile({
                    firstName: 'Comment Profile'
                });

                const newUser = new User({});


                const newReply = new Reply({
                    fileType: 'TEXT',
                    textContent: 'comment text'
                });

                async.parallel([
                    function (callback) {
                        Profile.remove({firstName: 'Comment Profile'}, function (err) {
                            if (err) done(err);
                            callback(null, null);
                        });
                    },
                    function (callback) {
                        Reply.remove({textContent: 'comment text'}, function (err) {
                            if (err) done(err);
                            callback(null, null);
                        });
                    }

                ], function (err, result) {
                    async.parallel({
                            profile: function (callback) {
                                newProfile.save(function (err, result) {
                                    if (err) return done(err);
                                    callback(null, result._id);
                                })
                            },
                            reply: function (callback) {
                                newReply.save(function (err, result) {
                                    if (err) return done(err);
                                    callback(null, result._id);
                                })
                            }
                        },
                        function (err, result) {
                            const existingComment = new Comment({
                                content: "a comment",
                                isPartOfSolution: false,
                                reply: result.reply,
                                profile: profileID
                            });

                            replyID = result.reply;
                            // profileID = result.profile;

                            existingComment.save(function (err) {
                                if (err) {
                                    return done(err);
                                }
                                done();
                            });
                        }
                    );
                });


            });
        });
    });

    after(function (done) {
        async.parallel([
            function (callback) {
                Profile.remove({firstName: 'Comment Profile'}, function (err) {
                    if (err) done(err);
                    callback(null, null);
                });
            },
            function (callback) {
                Reply.remove({textContent: 'comment text'}, function (err) {
                    if (err) done(err);
                    callback(null, null);
                });
            },
            function (callback) {
                Comment.remove({}, function (err) {
                    if (err) done(err);
                    callback(null, null);
                });
            }

        ], function (err, result) {
            server.close(function (err) {
                if (err) return done(err);
                done();
            });
        });

    });


    describe('Testing createComment', function () {
        it('should not create new comment, Reply not found', function (done) {
            request
                .post(baseURL + '/api/comments/reply/000000000000000000000000')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    content: "new comment",
                    isPartOfSolution: false,
                    reply: '000000000000000000000000',
                    profile: profileID
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('COMMENT_CREATE_ERROR_REPLY_NOT_EXISTS'));
                    assert.equal(res.status, 404);
                    done();
                })
        });

        it('should create new comment', function (done) {
            request
                .post(baseURL + '/api/comments/reply/' + replyID)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    content: "new comment",
                    isPartOfSolution: false,
                    reply: replyID,
                    profile: profileID
                })
                .end(function (err, res) {
                    assert.equal(res.body.title, i18n.__('COMMENT_CREATE_TITLE_SUCCESS'));
                    assert.equal(res.status, 200);
                    done();
                })
        });


    });

});