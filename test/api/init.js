/**
 * Created by stavr on 30-Jan-17.
 */
const University = require('../../app/models/university'),
    Department = require('../../app/models/department'),
    config = require('./data.json');

exports.saveUniversities = function (cb) {
    University.remove({})
        .then(function () {
            const existingUniversity = new University(config.universities.existingUniversity1);
            existingUniversity.save();
        })
        .then(function () {
            const existingUniversity2 = new University(config.universities.existingUniversity2);
            existingUniversity2.save();
        })
        .then(function () {
            return cb(null)
        })
        .catch(function (err) {
            cb(err);
        });
};

exports.saveDepartments = function (cb) {

    Department.remove({})
        .then(function () {
            const dep = new Department(config.departments.existingDepartment1);
            dep.save();
        })
        .then(function () {
            const dep = new Department(config.departments.existingDepartment2);
            dep.save();
        })
        .then(function () {
            return cb(null)
        })
        .catch(function (err) {
            cb(err);
        });
}