/**
 * Created by bdrosatos on 16/10/2016.
 */
'use strict';

const assert = require('chai').assert,
    i18n = require('i18n'),
    request = require('superagent'),
    server = require('../../bin/www'),
    config = require('../../app/config/app'),
    userToken = require('./tokens.test.js').userToken,
    user2Token = require('./tokens.test.js').user2Token,
    adminToken = require('./tokens.test.js').adminToken,
    Exam = require('../../app/models/exam'),
    Course = require('../../app/models/course');


describe('Testing Exam (exam.controller)', function () {

    const baseURL = 'http://localhost:4001';
    let courseId, examImgsId, examPdfsId, examTxtId;

    before(function (done) {
        server.listen(4001, function (err) {
            if (err) return done();
            done();
        })
    });

    after(function (done) {
        Course.findById(courseId, function (err, course) {
            if (err) done(err);
            course.remove(function (err) {
                if (err) done(err);
                done();
            });

        });
    });


    describe('Testing createExam', function () {

        before(function (done) {
            const course = new Course({
                name: 'Test Course',
                courseType: 'Kormou',
            });

            course.save(function (err, newCourse) {
                if (err) done(err);
                courseId = newCourse._id;
                done();
            })
        });

        it('should NOT create a new Exam, year required', function (done) {

            request
                .post(baseURL + '/api/exams/course/58042a984452c22b84f6a617')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    examType: 'Ektakti',
                    fileType: 'PDF',
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('COURSE_YEAR_REQUIRED'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should NOT create a new Exam, examType required', function (done) {

            request
                .post(baseURL + '/api/exams/course/58042a984452c22b84f6a617')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    year: 2012,
                    fileType: 'PDF',
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('COURSE_EXAM_TYPE_REQUIRED'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should NOT create a new Exam, fileType required', function (done) {

            request
                .post(baseURL + '/api/exams/course/58042a984452c22b84f6a617')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    year: 2012,
                    examType: 'Spring',
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('COURSE_FILE_TYPE_REQUIRED'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should NOT create a new Exam, course not found', function (done) {

            request
                .post(baseURL + '/api/exams/course/58042a984452c22b84f6a617')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    year: 2012,
                    examType: 'Spring',
                    fileType: 'PDF'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('COURSE_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });
        });

        it('should create a new Exam, 2 image files attached', function (done) {

            request
                .post(baseURL + '/api/exams/course/' + courseId)
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', userToken)
                .field('year', 2012)
                .field('examType', 'Spring')
                .field('fileType', 'IMG')
                .attach('imgFiles', 'test/api/files.test/image1.png')
                .attach('imgFiles', 'test/api/files.test/image2.png')
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.year, 2012);
                    assert.equal(res.body.obj.examType, 'Spring');
                    assert.equal(res.body.obj.fileType, 'IMG');
                    assert.equal(res.body.obj.filePath.length, 2);
                    assert.equal(res.body.obj.filePath[0], `courses/${courseId}/exams/2012/Spring/image1.png`);
                    assert.equal(res.body.obj.filePath[1], `courses/${courseId}/exams/2012/Spring/image2.png`);
                    assert.equal(res.status, 201);
                    examImgsId = res.body.obj._id;
                    done();
                });
        });

        it('should create a new Exam, 2 pdf files attached', function (done) {

            request
                .post(baseURL + '/api/exams/course/' + courseId)
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', userToken)
                .field('year', 2013)
                .field('examType', 'Winter')
                .field('fileType', 'PDF')
                .attach('pdfFiles', 'test/api/files.test/pdf1.pdf')
                .attach('pdfFiles', 'test/api/files.test/pdf2.pdf')
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.year, 2013);
                    assert.equal(res.body.obj.examType, 'Winter');
                    assert.equal(res.body.obj.fileType, 'PDF');
                    assert.equal(res.body.obj.filePath.length, 2);
                    assert.equal(res.body.obj.filePath[0], `courses/${courseId}/exams/2013/Winter/pdf1.pdf`);
                    assert.equal(res.body.obj.filePath[1], `courses/${courseId}/exams/2013/Winter/pdf2.pdf`);
                    assert.equal(res.status, 201);
                    examPdfsId = res.body.obj._id;
                    done();
                });
        });

        it('should create a new Exam, text content', function (done) {

            request
                .post(baseURL + '/api/exams/course/' + courseId)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    year: 2014,
                    examType: 'Spring',
                    fileType: 'TXT',
                    textContent: 'TEST CONTENT'
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.year, 2014);
                    assert.equal(res.body.obj.examType, 'Spring');
                    assert.equal(res.body.obj.fileType, 'TXT');
                    assert.equal(res.body.obj.textContent, 'TEST CONTENT');
                    assert.equal(res.body.obj.filePath.length, 1);
                    assert.equal(res.body.obj.filePath[0], `courses/${courseId}/exams/2014/Spring/exam.txt`);
                    assert.equal(res.status, 201);
                    examTxtId = res.body.obj._id;
                    done();
                });
        });

    });

    describe('Testing getExamList', function () {

        it('should NOT get a list of all exams, unauthorized', function (done) {

            request
                .get(baseURL + '/api/exams?page=1')
                .end(function (err, res) {
                    assert.equal(err.message, i18n.__('WORD_UNAUTHORIZED'));
                    assert.equal(res.status, 401);
                    done();
                });

        });

        it('should get a list of all exams', function (done) {

            request
                .get(baseURL + '/api/exams?page=1')
                .set('Authorization', userToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.total_pages, 1);
                    assert.isFalse(res.body.has_more);
                    assert.equal(res.body.obj.length, 3);
                    assert.equal(res.status, 200);
                    done();
                });

        });

        it('should get a list of exams by courseId', function (done) {

            request
                .get(baseURL + '/api/exams?page=1&courseId=' + courseId)
                .set('Authorization', userToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.total_pages, 1);
                    assert.isFalse(res.body.has_more);
                    assert.equal(res.body.obj.length, 3);
                    assert.equal(res.status, 200);
                    done();
                });

        });

    });

    describe('Testing getExamById', function () {

        it('should NOT get an exam by id, exam not found', function (done) {

            request
                .get(baseURL + '/api/exams/58040ead8f9ad4045cfdb6c8')
                .set('Authorization', userToken)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('EXAM_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });


        });

        it('should NOT get an exam by id, unauthorized', function (done) {

            request
                .get(baseURL + '/api/exams/' + examImgsId)
                .end(function (err, res) {
                    assert.equal(err.message, i18n.__('WORD_UNAUTHORIZED'));
                    assert.equal(res.status, 401);
                    done();
                });


        });

        it('should get an exam by id, img files', function (done) {

            request
                .get(baseURL + '/api/exams/' + examImgsId)
                .set('Authorization', userToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.filePath.length, 2);
                    assert.equal(res.body.obj.filePath[0], `https://s3.eu-central-1.amazonaws.com/uni-exams-uploads/courses/${courseId}/exams/${res.body.obj.year}/${res.body.obj.examType}/image1.png`);
                    assert.equal(res.body.obj.filePath[1], `https://s3.eu-central-1.amazonaws.com/uni-exams-uploads/courses/${courseId}/exams/${res.body.obj.year}/${res.body.obj.examType}/image2.png`);
                    assert.equal(res.status, 200);
                    done();
                });

        });

    });

    describe('Testing deleteExam', function () {

        it('should NOT delete an exam by id, unauthorized', function (done) {

            request
                .del(baseURL + '/api/exams/' + examImgsId)
                .end(function (err, res) {
                    assert.equal(err.message, i18n.__('WORD_UNAUTHORIZED'));
                    assert.equal(res.status, 401);
                    done();
                });


        });

        it('should delete an exam by id by same user, img files', function (done) {

            request
                .del(baseURL + '/api/exams/' + examImgsId)
                .set('Authorization', userToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    Course.findById(courseId, function (err, course) {
                        assert.equal(res.body.obj.fileType, 'IMG');
                        assert.equal(res.status, 200);
                        done();
                    });

                });

        });


        it('should delete an exam by id by admin user, pdf files', function (done) {

            request
                .del(baseURL + '/api/exams/' + examPdfsId)
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    Course.findById(courseId, function (err, course) {
                        assert.equal(res.body.obj.fileType, 'PDF');
                        assert.equal(res.status, 200);
                        done();
                    });

                });

        });

    });


});