/**
 * Created by bdrosatos on 30/9/2016.
 */
'use strict';

require('./user.test');

const assert = require('chai').assert,
    i18n = require('i18n'),
    request = require('superagent'),
    server = require('../../bin/www'),
    config = require('../../app/config/app'),
    userToken = require('./tokens.test.js').userToken,
    user2Token = require('./tokens.test.js').user2Token,
    adminToken = require('./tokens.test.js').adminToken,
    User = require('../../app/models/user'),
    Profile = require('../../app/models/profile');


describe('Testing profile (profile.controller)', function () {

    const baseURL = 'http://localhost:4001';
    let userId;
    let profileId;

    before(function (done) {
        server.listen(4001, function (err) {
            if (err) return done();
            done();
        })
    });

    after(function (done) {
        Profile.findOne({user: userId}, function (err, profile) {

            if (err) done(err);

            if (!profile) {
                done('Profile not found');
            }

            profile.remove(function (err) {
                if (err) done(err);
                User.findById(userId, function (err, user) {
                    if (err) done(err);

                    if (!user) {
                        done('User not found');
                    }

                    user.profile = undefined;

                    user.save(function (err) {
                        if (err) done(err);
                        done();
                    })
                })
            });

        });
    });

    describe('Testing createProfile', function () {

        it('should NOT create a new Profile, not authorized (401)', function (done) {

            request
                .post(baseURL + '/api/profiles')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                    firstName: 'Test',
                    lastName: 'Testopoulos',
                })
                .end(function (err, res) {
                    assert.equal(res.status, 401);
                    done();
                });
        });

        it('should NOT create a new Profile, firstName required Unprocessable Entity(422)', function (done) {

            request
                .post(baseURL + '/api/profiles')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    lastName: 'Testopoulos'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('PROFILE_CREATE_ERROR_FIRSTNAME'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

        it('should create a new Profile', function (done) {

            request
                .post(baseURL + '/api/profiles')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', user2Token)
                .send({
                    firstName: 'Test',
                    lastName: 'Testopoulos'
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.firstName, 'Test');
                    assert.equal(res.body.obj.lastName, 'Testopoulos');
                    assert.isNotNull(res.body.obj.user);
                    assert.equal(res.status, 201);
                    userId = res.body.obj.user;
                    profileId = res.body.obj._id;
                    done();
                });
        });

        it('should NOT create a new Profile, user has already a profile', function (done) {

            request
                .post(baseURL + '/api/profiles')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', userToken)
                .send({
                    firstName: 'Test',
                    lastName: 'Testopoulos'
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('PROFILE_CREATE_ERROR_ALREADY'));
                    assert.equal(res.status, 422);
                    done();
                });
        });

    });

    describe('Testing getProfileList', function () {

        it('should NOT get a list of profiles, unauthorized user', function (done) {

            request
                .get(baseURL + '/api/profiles?page=1')
                .set('Authorization', user2Token)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('NOT_AUTHORIZED'));
                    assert.equal(res.status, 401);
                    done();
                });

        });

        it('should get a list of profiles', function (done) {

            request
                .get(baseURL + '/api/profiles?page=1')
                .set('Authorization', adminToken)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.total_pages, 1);
                    assert.isFalse(res.body.has_more);
                    assert.equal(res.body.obj.length, 2);
                    assert.equal(res.status, 200);
                    done();
                });

        });

    });

    describe('Testing getProfileById', function () {

        it('should NOT get a profile,  profile Not found', function (done) {

            request
                .get(baseURL + '/api/profiles/580209b024a27c2e6c7ce8bf')
                .set('Authorization', userToken)
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('PROFILE_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });

        });

        it('should get profile by id', function (done) {

            request
                .get(baseURL + '/api/profiles/' + profileId)
                .set('Authorization', user2Token)
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.firstName, 'Test');
                    assert.equal(res.body.obj.lastName, 'Testopoulos');
                    assert.equal(res.body.obj.lastName, 'Testopoulos');
                    assert.equal(res.body.obj.user._id, userId);
                    assert.equal(res.status, 200);
                    done();
                });

        });

    });

    describe('Testing updateProfile', function () {

        it('should NOT update a profile, profile Not found', function (done) {

            request
                .patch(baseURL + '/api/profiles/580209b024a27c2e6c7ce8bf')
                .set('Authorization', user2Token)
                .send({
                    firstName: 'Test',
                    lastName: 'Testopoulos',
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('PROFILE_NOT_FOUND'));
                    assert.equal(res.status, 404);
                    done();
                });

        });

        it('should NOT update a profile, firstName required', function (done) {

            request
                .patch(baseURL + '/api/profiles/' + profileId)
                .set('Authorization', user2Token)
                .send({
                    lastName: 'Testopoulos',
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('PROFILE_UPDATE_ERROR_FIRSTNAME'));
                    assert.equal(res.status, 422);
                    done();
                });

        });

        it('should NOT update a profile, not authorized', function (done) {

            request
                .patch(baseURL + '/api/profiles/' + profileId)
                .set('Authorization', userToken)
                .send({
                    firstName: 'Update',
                    lastName: 'Updatedtopoulos',
                    uniRegistrationYear: 2011
                })
                .end(function (err, res) {
                    assert.equal(err.response.body.error.message, i18n.__('NOT_AUTHORIZED'));
                    assert.equal(res.status, 401);
                    done();
                });

        });

        it('should update a profile by owner user', function (done) {

            request
                .patch(baseURL + '/api/profiles/' + profileId)
                .set('Authorization', user2Token)
                .send({
                    firstName: 'Update',
                    lastName: 'Updatedtopoulos',
                    uniRegistrationYear: 2011
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.firstName, 'Update');
                    assert.equal(res.body.obj.lastName, 'Updatedtopoulos');
                    assert.equal(res.body.obj.uniRegistrationYear, 2011);
                    assert.equal(res.status, 200);
                    done();
                });

        });

        it('should update a profile by admin user', function (done) {

            request
                .patch(baseURL + '/api/profiles/' + profileId)
                .set('Authorization', adminToken)
                .send({
                    firstName: 'UpdateAdmin',
                    lastName: 'Updatedtopoulos',
                    uniRegistrationYear: 2012
                })
                .end(function (err, res) {
                    assert.isNull(err);
                    assert.equal(res.body.obj.firstName, 'UpdateAdmin');
                    assert.equal(res.body.obj.lastName, 'Updatedtopoulos');
                    assert.equal(res.body.obj.uniRegistrationYear, 2012);
                    assert.equal(res.status, 200);
                    done();
                });

        });

    });


});