/**
 * Created by stavros on 22/2/2017.
 */

const expect = require('chai').expect,
    University = require('../../app/models/university'),
    Department = require('../../app/models/department'),
    server = require('../../bin/www'),
    request = require('superagent'),
    adminToken = require('./tokens.test').adminToken,
    userToken = require('./tokens.test').userToken,
    i18n = require('i18n'),
    assert = require('chai').assert,
    mongoose = require('mongoose'),
    config = require('./data.json'),
    init = require('./init'),
    async = require('async');

const baseURL = 'http://localhost:4001';

//Update tokens
describe('Check if tokens are ok', function () {
    before(function (done) {
        server.listen(4001, function (err) {
            if (err)  return done(err);
            done()
        });
    });

    describe('Check if tokens are valid', function () {
        it('should check admin token', function (done) {
            request
                .get(baseURL + '/test/testJWTAdmin')
                .set('Authorization',config.users.admin.token)
                .end(function (err, res) {
                        if (res.statusCode == 200) {
                            return done();
                        } else if (res.statusCode == 401) {
                            request
                                .post(baseURL + '/test/login')
                                .set('Content-Type', 'application/x-www-form-urlencoded')
                                .send({
                                    email: config.users.admin.data.email,
                                    password: config.users.admin.data.password
                                })
                                .end(function (err, res) {
                                    if (res.statusCode == 200) {
                                        if (config.users.admin.token == res.body.obj.token) {
                                            return done();
                                        } else {
                                            return done(new Error('wrong token. new is \r\n' + res.body.obj.token +'\r\n'))
                                        }
                                    }
                                });
                        } else {
                            return done(new Error('"Wrong admin credentials"'))
                        }
                    }
                );
        })
    })
});