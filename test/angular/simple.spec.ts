/**
 * Created by bdrosatos on 24/10/2016.
 */
describe('1st tests', () => {
    it('true is true', () => expect(true).toBe(true));
    it('true is not false', () => expect(true).toBe(false));
});
