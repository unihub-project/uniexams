const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Department = require('./department');

const UniversitySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    shortName: {
        type: String,
        required: true
    },
    information: {
        type: String
    },
    departments: [
        {type: Schema.Types.ObjectId, ref: 'Department'}
    ]
});

UniversitySchema.pre('remove', function (next) {
    Department.find({university: this._id}, function (err, departments) {
        if (err) next(err);
        departments.forEach((department) => {

            department.remove(function (err) {
                if (err) next(err);
            });
        });
        next();
    });


});

UniversitySchema.plugin(require('mongoose-paginate'));

module.exports = mongoose.model('University', UniversitySchema);
