const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const ActivityLogSchema = new Schema({
        activityId: {
            type: String
        },
        activityType: {
            type: String,
            enum: ['Reply', 'Comment']
        },
        profile: [
            {type: Schema.Types.ObjectId, ref: 'Profile'}
        ]
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model('ActivityLog', ActivityLogSchema);

