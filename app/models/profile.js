/**
 * Created by bdrosatos on 27/9/2016.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const ProfileSchema = new Schema({
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String
        },
        birthDate: {
            type: Date
        }
        ,
        imgUrl: {
            type: String
        },
        uniRegistrationYear: {
            type: String
        },
        defaultDepartment: {
            type: Schema.Types.ObjectId, ref: 'Department'
        },
        followCourses: [
            {type: Schema.Types.ObjectId, ref: 'Course'}
        ],
        user: {
            type: Schema.Types.ObjectId, ref: 'User'
        }

    },
    {
        timestamps: true
    });

ProfileSchema.statics = {
    //Method to find profile by user id
    findByUser: function (userId, cb) {
        return this.findOne({'user': userId}).exec(cb);
    }
};

ProfileSchema.plugin(require('mongoose-paginate'));

module.exports = mongoose.model('Profile', ProfileSchema);
