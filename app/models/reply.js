const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Comment = require('./comment');

const ReplySchema = new Schema({
        upvotes: {
            type: Number,
            default: 0
        },
        upvoteUsers: [
            {type: Schema.Types.ObjectId, ref: 'User'}
        ],
        isApproved: {
            type: Boolean,
            default: false
        },
        topicNumber : {
            type: Number,
            default: 1,
            required: true
        },
        fileType: {
            type: String,
            enum: ['PDF', 'IMG', 'TXT'],
            required: true
        },
        filePath: {
            type: String
        },
        textContent: {
            type: String
        },
        comments: [
            {type: Schema.Types.ObjectId, ref: 'Comment'}
        ],
        exam: {
            type: Schema.Types.ObjectId, ref: 'Exam'
        },
        profile: {
            type: Schema.Types.ObjectId, ref: 'Profile'
        }
    },
    {
        timestamps: true
    });

ReplySchema.pre('remove', function (next) {
    Comment.find({reply: this._id}, (err, comments) => {
        comments.forEach((comment) => {
            comment.remove((err)=> {
                if (err) next(err);
            });
        })
    });

    next();
});

ReplySchema.plugin(require('mongoose-paginate'));

module.exports = mongoose.model('Reply', ReplySchema);

