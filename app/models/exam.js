const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Reply = require('./reply');

const ExamSchema = new Schema({
    year: {
        type: Number,
        required: true
    },
    examType: {
        type: String,
        enum: ['SPRING', 'WINTER', 'SEPTEMBER', 'EKTAKTI'],
        required: true
    },
    numberOfTopics: {
        type: Number,
        required: true
    },
    fileType: {
        type: String,
        enum: ['PDF', 'IMG', 'TXT'],
        required: true
    },
    filePath: [
        {type: String}
    ],
    textContent: {
        type: String
    },
    information: {
        type: String
    },
    course: {
        type: Schema.Types.ObjectId, ref: 'Course'
    },
    profile: {
        type: Schema.Types.ObjectId, ref: 'Profile'
    }
});

ExamSchema.pre('remove', function (next) {
    Reply.find({exam: this._id}, (err, replies) => {
        replies.forEach((reply) => {
            reply.remove((err)=> {
                if (err) next(err);
            });
        })
    });

    next();
});


ExamSchema.index({course: 1, year: 1, examType: 1}, {unique: true});

ExamSchema.plugin(require('mongoose-paginate'));
ExamSchema.plugin(require('mongoose-unique-validator'));

module.exports = mongoose.model('Exam', ExamSchema);
