/**
 * Created by bdrosatos on 25/9/2016.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');

const UserSchema = new Schema({
        email: {
            type: String,
            lowercase: true,
            unique: true,
            required: true
        },
        facebook: {
            id: {
                type: String
            },
            token: {
                type: String
            }
        },
        password: {
            type: String
        },
        role: {
            type: String,
            enum: ['Undergraduate', 'Postgraduate', 'Teacher', 'Admin'],
            default: 'Undergraduate'
        },
        isConfirmed: {
            type: Boolean,
            default: false
        },
        confirmToken: {
            type: String
        },
        resetPasswordToken: {
            type: String
        },
        resetPasswordExpires: {
            type: Date
        },
        profile: {
            type: Schema.Types.ObjectId, ref: 'Profile'
        }
    },
    {
        timestamps: true
    });

//Hash password pre save/update
UserSchema.pre('save', function (next) {
    const user = this,
        SALT_FACTOR = 10;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, null, function (err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

// Method to compare password for login
UserSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }

        cb(null, isMatch);
    });
};

UserSchema.statics = {
    //Method to find one user by email
    findByEmail: function (email, cb) {
        return this.findOne({'email': email}).exec(cb);
    },

    findByProfile: function (profile, cb) {
        return this.findOne({'profile': profile}).exec(cb);
    },

    //Method to find users by role
    findByRole: function (role, cb) {
        return this.find({'role': role}).exec(cb);
    }

};

UserSchema.plugin(require('mongoose-paginate'));

module.exports = mongoose.model('User', UserSchema);
