const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Course = require('./course');

const DepartmentSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    shortName: {
        type: String,
        required: true
    },
    information: {
        type: String
    },
    courses: [
        {type: Schema.Types.ObjectId, ref: 'Course'}
    ],
    university: {
        type: Schema.Types.ObjectId,
        ref: 'University',
        required: true,
    }
});

DepartmentSchema.pre('remove', function (next) {
    Course.find({department: this._id}, (err, courses) => {
        courses.forEach((course) => {
            course.remove((err)=> {
                if (err) next(err);
            });
        })
    });

    next();
});

DepartmentSchema.plugin(require('mongoose-paginate'));

module.exports = mongoose.model('Department', DepartmentSchema);
