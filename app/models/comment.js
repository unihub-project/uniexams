const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const CommentSchema = new Schema({
        content: {
            type: String
        },
        isPartOfSolution: {
            type: Boolean,
            default: false
        },
        reply: {
            type: Schema.Types.ObjectId, ref: 'Reply'
        },
        profile: {
            type: Schema.Types.ObjectId, ref: 'Profile'
        }

    },
    {
        timestamps: true
    });


CommentSchema.plugin(require('mongoose-paginate'));


module.exports = mongoose.model('Comment', CommentSchema);

