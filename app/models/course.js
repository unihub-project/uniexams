'use strict';

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Exam = require('./exam');

const aws = require('aws-sdk');
aws.config.loadFromPath('app//config/aws-config.json');

const CourseSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    courseType: {
        type: String,
        required: true
    },
    information: {
        type: String
    },
    exams: [
        {type: Schema.Types.ObjectId, ref: 'Exam'}
    ],
    department: {
        type: Schema.Types.ObjectId, ref: 'Department'
    },
    isFollowed : {
        type: Boolean,
        default: false
    }
});


CourseSchema.pre('remove', function (next) {
    Exam.find({course: this._id}, (err, exams) => {
        if (exams.length > 0) {

            exams.forEach((exam) => {
                exam.remove((err, deletedExam) => {
                    if (err) {
                        next(err);
                    }

                    //Delete aws s3 file key
                    let params = {Bucket: 'uni-exams-uploads'};
                    params.Delete = {Objects: []};
                    deletedExam.filePath.forEach((key) => {
                        params.Delete.Objects.push({Key: key});
                    });
                    const s3 = new aws.S3({signatureVersion: 'v4', params: params});
                    s3.deleteObjects(params, function (err, data) {
                        if (err) throw err;
                        next();
                    });
                });
            })
        }else{
            next();
        }
    });

});

CourseSchema.plugin(require('mongoose-paginate'));


module.exports = mongoose.model('Course', CourseSchema);
