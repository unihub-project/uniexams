/**
 * Created by bdrosatos on 25/9/2016.
 */

"use strict";


const jwt = require('jsonwebtoken'),
    crypto = require('crypto'),
    qs = require('query-string'),
    User = require('../models/user'),
    config = require('../config/app'),
    i18n = require('i18n'),
    randomString = require('randomstring'),
    url = require('url'),
    passport = require('passport'),
    passportService = require('../config/passport'),
    nodemailer = require('nodemailer'),
    sgTransport = require('nodemailer-sendgrid-transport'),
    logger = require('noogger'),
    mailer = nodemailer.createTransport(sgTransport(config.email_options));


//Private methods
function generateToken(user) {
    return jwt.sign(user, config.secret
        //     ,
        //     {
        //     expiresIn: 86400000 // TODO: Change this on production
        // }
    );
}

// Set user info from request
function setUserInfo(request) {
    return {
        _id: request._id,
        email: request.email,
        role: request.role,
        profile: request.profile
    };

}

function sendRegisterEmail(req, res, user) {

    let urlString = url.format({
        protocol: req.protocol,
        hostname: req.hostname + ':' + config.port
    });

    urlString += '/api/users/confirm/' + user.confirmToken;
    urlString = urlString.replace(/\[/g, "").replace(/]/g, "");

    logger.debug(`Url String ${urlString}`);
    const email = {
        to: [user.email],
        from: 'no-reply@uni-exams.com',
        subject: 'Welcome to UniHub!',
        text: 'Welcome',
        html: `<h1>Welcome to UniHub!</h1>
               <div style="margin-top:10px">
                    Your confirmation code is: ${user.confirmToken} <br/>
                    <a href="${urlString}">Press here to activate your account!</a>
               </div>
        `
    };

    mailer.sendMail(email, function (err, res) {
        if (err) {
            logger.critical(JSON.stringify(err));
            return res.status(400).json({
                title: i18n.__('EMAIL_ERROR_TITLE'),
                error: { message: 'Email Error' }
            });
        }
    });
}

//


//========================================
// Login Route
//========================================
exports.login = (req, res, next) => {
    logger.info(`POST: /user/authenticate/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)}  : login (${'email:' + (req.body['email'])})`);

    passport.authenticate('local', (err, user, info) => {

        //Check for errors
        if (err) {
            logger.critical(JSON.stringify(err));
            return next(err)
        }

        //Wrong credentials
        if (!user) {
            return res.status(422).json({
                title: i18n.__('LOGIN_ERROR_TITLE'),
                error: info
            });
        }

        //Not Confirmed
        if (!user.isConfirmed) {
            return res.status(401).json({
                title: i18n.__('LOGIN_ERROR_TITLE'),
                error: {message: i18n.__('LOGIN_ERROR_NOT_CONFIRMED')}
            });
        }

        let userInfo = setUserInfo(user);

        //Logged in
        res.status(200).json({
            title: i18n.__('LOGIN_SUCCESS_TITLE'),
            obj: {
                token: 'JWT ' + generateToken(userInfo),
                user: userInfo
            }
        });
    })(req, res, next);
};

exports.loginFromFacebook = (req, res, next) => {
    logger.info(`POST: /user/authenticate/facebook/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)}  : loginFromFacebook (${JSON.stringify(req.body)})`);


    passport.authenticate('facebook', {failureRedirect: '/#/auth/signin'}, (err, user) => {
        if (err) {
            logger.critical(JSON.stringify(err));
            return res.redirect('/#/?' + qs.stringify(err));
        }
        let userInfo = setUserInfo(user);
        const jsonResponse = {
            token: 'JWT ' + generateToken(userInfo),
            userId: user._id,
            email: user.email,
            role: user.role,
            profileId: user.profile._id,
            firstName: user.profile.firstName,
            lastName: user.profile.lastName,
        };

        //Logged in
        return res.redirect('/home?' + qs.stringify(jsonResponse));
    })(req, res, next);

};

//========================================
// Registration Route
//========================================
exports.register = (req, res, next) => {
    logger.info(`POST: /user/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)}  : register (${'email:' + (req.body['email'])})`);

    // Check for registration errors
    const email = req.body.email;
    const password = req.body.password;

    // Return error if no email provided
    if (!email) {
        return res.status(422).json({
            title: i18n.__('REGISTER_ERROR_TITLE'),
            error: {message: i18n.__('NO_EMAIL')}
        });
    }

    // Return error if no password provided
    if (!password) {
        return res.status(422).json({
            title: i18n.__('REGISTER_ERROR_TITLE'),
            error: {message: i18n.__('NO_PASSWORD')}
        });
    }

    User.findOne({email: email}).exec()
        .then((existingUser) => {
            // If user is not unique, return error
            if (existingUser) {
                return res.status(422).json({
                    title: i18n.__('REGISTER_ERROR_TITLE'),
                    error: {message: i18n.__('EXIST_EMAIL')}
                });
            }

            // If email is unique and password was provided, create account
            let user = new User({
                email: email,
                password: password,
                confirmToken: randomString.generate({
                    length: 6,
                    charset: 'numeric'
                })
            });

            return user.save();
        })
        .then((user) => {
            sendRegisterEmail(req, res, user);

            let userInfo = setUserInfo(user);

            // Respond with JWT if user was created
            res.status(201).json({
                title: i18n.__('REGISTER_SUCCESS_TITLE'),
                obj: {
                    token: 'JWT ' + generateToken(userInfo),
                    user: userInfo
                }
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        });
};

//========================================
// Update user Route
//========================================
exports.updateUser = (req, res, next) => {
    logger.info(`PATCH: /user/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)}  : UpdateUser`);

    const email = req.body.email;
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const isConfirmed = req.body.isConfirmed;
    const role = req.body.role;

    //Get User from authorization header
    let requestedUser = req.user;

    if (!requestedUser) {
        return res.status(404).json({
            title: i18n.__('UPDATE_ERROR_TITLE'),
            error: {message: i18n.__('NO_USER_FOUND')}
        });
    }

    User.findById(req.params.id).exec()
        .then((user) => {
            if (!user) {
                return res.status(404).json({
                    title: i18n.__('UPDATE_ERROR_TITLE'),
                    error: {message: i18n.__('NO_USER_FOUND')}
                });
            }

            //Change email
            if (email !== user.email) {
                user.email = email;
                user.isConfirmed = false;
                user.confirmToken = randomString.generate({
                    length: 6,
                    charset: 'numeric'
                });
                sendRegisterEmail(req, res, user);
            }

            //Change password
            if (newPassword) {
                return user.comparePassword(oldPassword, (err, isMatch) => {
                    if (err) {
                        logger.critical(JSON.stringify(err));
                        return res.status(500).json({
                            title: i18n.__('UPDATE_ERROR_TITLE'),
                            error: err
                        })
                    }

                    if (!isMatch) {
                        return res.status(422).json({
                            title: i18n.__('UPDATE_ERROR_TITLE'),
                            error: {message: i18n.__('OLD_PASSWORD_ERROR')}
                        });
                    }

                    user.password = newPassword;

                    user.save((err, updatedUser) => {
                        if (err) {
                            logger.critical(JSON.stringify(err));
                            return next(err);
                        }

                        return res.status(200).json({
                            title: i18n.__('UPDATE_USER_SUCCESS_TITLE'),
                            obj: setUserInfo(updatedUser)
                        });
                    });
                });
            }

            if (user.role != role) {
                user.role = role;
            }

            if (user.isConfirmed != isConfirmed) {
                user.isConfirmed = isConfirmed;
            }

            user.save((err, updatedUser) => {
                if (err) {
                    logger.critical(JSON.stringify(err));
                    return next(err);
                }

                res.status(200).json({
                    title: i18n.__('UPDATE_USER_SUCCESS_TITLE'),
                    obj: {user: setUserInfo(updatedUser)}
                });
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        });
};

//========================================
// Confirmation Route
//========================================
exports.confirmUser = (req, res, next) => {
    logger.info(`GET: /user/confirm/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : confirmUser (${JSON.stringify(req.body)})`);

    var token = req.params.token;

    User.findOne({confirmToken: token}).exec()
        .then((user) => {
            //User with this confirmToken does not exist
            if (!user) {
                return res.status(404).json({
                    title: i18n.__('CONFIRMATION_ERROR_TITLE'),
                    error: {message: i18n.__('NO_USER_FOUND')}
                });
            }

            //Already confirmed
            if (user.isConfirmed) {
                return res.status(422).json({
                    title: i18n.__('CONFIRMATION_ERROR_TITLE'),
                    error: {message: i18n.__('ACCOUNT_CONFIRMED')}
                });
            }

            //Confirm User
            user.isConfirmed = true;

            return user.save();
        })
        .then((confirmedUser) => {
            confirmedUser.isConfirmed = true;

            res.status(200).json({
                title: i18n.__('CONFIRMATION_SUCCESS_TITLE'),
                obj: setUserInfo(confirmedUser)
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        });
};

//========================================
// Authorization Middleware
//========================================

// Role authorization check
exports.roleAuthorization = (role) => {
    return (req, res, next) => {
        const user = req.user;

        User.findById(user._id).exec()
            .then((foundUser) => {

                if (!foundUser) {
                    return res.status(404).json({
                        error: {message: i18n.__('NO_USER_FOUND')}
                    });
                }

                // If user is found, check role.
                if (foundUser.role == role) {
                    return next();
                }

                return res.status(401).json({
                    title: i18n.__('WORD_UNAUTHORIZED'),
                    error: {message: i18n.__('NOT_AUTHORIZED')}
                });
            })
            .catch((err) => {
                logger.critical(JSON.stringify(err));
                return next(err);
            })


    }
};