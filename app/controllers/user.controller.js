/**
 * Created by stavr on 15-Jan-17.
 */

"use strict";


const i18n = require('i18n'),
    User = require('../models/user'),
    paginate = require('express-paginate'),
    logger = require('noogger');

//Private methods

// Set user info from request
function setUserInfo(request) {
    return {
        _id: request._id,
        email: request.email,
        role: request.role,
        isConfirmed: request.isConfirmed,
        profile: request.profile
    };

}

function setUserInfoList(request) {
    return {
        _id: request._id,
        email: request.email,
        role: request.role,
    };

}

//

exports.getUsersList = (req, res, next)=> {
    logger.info(`GET: /users/:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)}  : getUsersList (${req.body})`);

    let query = {};

    if (req.query.userId) {
        query = {
            department: req.query.userId
        }
    }

    User.paginate(query, {
        page: req.query.page,
        sort: 'username',
    }).then((result) => {
            let users = result.docs;

            res.status(200).json({
                has_more: paginate.hasNextPages(req)(result.pages),
                total_pages: result.pages,
                total: result.total,
                obj: users.map((user) => {
                    return setUserInfoList(user)
                })
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        });

};


exports.getUserByID = (req, res, next)=> {
    logger.info(`GET: /users/:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)}  : getUserByID (${req.body})`);

    User.findById(req.params.id).populate('profile').exec()
        .then((user) => {

            if (!user) {
                return res.status(404).json({
                    title: i18n.__('USER_GET_ERROR_TITLE'),
                    error: {message: i18n.__('USER_NOT_FOUND')}
                });
            }

            res.status(200).json({
                title: i18n.__('USER_GET_SUCCESS_TITLE'),
                obj: setUserInfo(user)
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            next(err);
        })

};


exports.deleteUser = (req, res, next) => {
    logger.info(`DELETE: /users/:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)}  : deleteUser (${req.body})`);

    User.findById(req.params.id).exec()
        .then((user) => {

            if (!user) {
                return res.status(404).json({
                    title: i18n.__('USER_DELETE_ERROR_TITLE'),
                    error: {message: i18n.__('USER_NOT_FOUND')}
                });
            }

            user.remove((err) => {
                if (err){
                    logger.critical(JSON.stringify(err));
                    next(err);
                }
                res.status(200).json({
                    title: i18n.__('USER_DELETE_SUCCESS_TITLE'),
                    obj: user
                });
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            next(err);
        })

};