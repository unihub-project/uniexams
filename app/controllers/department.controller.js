/**
 * Created by bdrosatos on 3/10/2016.
 */

"use strict";


const i18n = require('i18n'),
    Department = require('../models/department'),
    University = require('../models/university'),
    Profile = require('../models/profile'),
    paginate = require('express-paginate'),
    logger = require('noogger');


//Private methods


//

//========================================
// Create new department Route
//========================================

exports.createDepartment = (req, res, next) => {
    const name = req.body.name;
    const shortName = req.body.shortName;
    const information = req.body.information;

    const universityId = req.params.id;

    if (!name || !shortName) {
        return res.status(422).json({
            title: i18n.__('DEPARTMENT_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('DEPARTMENT_NO_REQUIRED_FIELDS')}
        });
    }

    University.findById(universityId)
        .then((university) => {

            if (!university) {
                return res.status(404).json({
                    title: i18n.__('DEPARTMENT_CREATE_ERROR_TITLE'),
                    error: {message: i18n.__('UNIVERSITY_NOT_FOUND')}
                });
            }

            Department.findOne({'name': name})
                .then((existDepartment)=> {


                    if (existDepartment) {
                        return res.status(422).json({
                            title: i18n.__('DEPARTMENT_CREATE_ERROR_TITLE'),
                            error: {message: i18n.__('DEPARTMENT_CREATE_ERROR_EXISTS ')}
                        })
                    }

                    const department = new Department({
                        name: name,
                        shortName: shortName,
                        information: information,
                        university: universityId
                    });


                    department.save()
                        .then((newDepartment)=> {
                            university.departments.push(newDepartment);
                            university.save();
                            return newDepartment;
                        })
                        .then((newDepartment)=> {
                            return res.status(201).json({
                                title: i18n.__('DEPARTMENT_CREATE_SUCCESS_TITLE'),
                                obj: newDepartment
                            })
                        })
                })
        })
        .then((response)=> {
            return response;
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        })


};


//========================================
// Get List Departments Route
//========================================

exports.getDepartmentList = (req, res, next) => {

    console.log(req.query.page)

    if (req.query.page) {


        let queryParams = {};

        if (req.query.uniId) {
            queryParams["university"] = req.query.uniId
        }

        if (req.query.name) {
            queryParams['name'] = {'$regex': req.query.name, '$options': 'i'};
        }

        if (req.query.shortName) {
            queryParams['shortName'] = {'$regex': req.query.shortName, '$options': 'i'};
        }

        Department.paginate(queryParams, {
            page: req.query.page,
            sort: 'university.shortName',
            populate: 'university'
        }, (err, result) => {
            if (err)
                return next(err);

            let departments = result.docs;

            res.status(200).json({
                has_more: paginate.hasNextPages(req)(result.pages),
                total_pages: result.pages,
                total: result.total,
                obj: departments
            });

        });
    } else {
        let queryParams = {};
        if (req.query.uniId) {
            queryParams['university'] = req.query.uniId;
        }

        Department.find(queryParams)
            // .select('_id name')
            .exec()
            .then((result) => {
                    return res.status(200).json({
                        title: i18n.__('DEPARTMENT_GET_MAP_SUCCESS_TITLE'),
                        obj: result
                    });
                }
            )
            .then((response)=> {
                return response;
            })
            .catch((err) => {
                logger.critical(JSON.stringify(err));
                return next(err);
            })
    }

};

//========================================
// Get Department By ID
//========================================

exports.getDepartmentById = (req, res, next) => {

    Department.findById(req.params.id).populate('university courses').exec((err, department) => {
        if (err) {
            return next(err);
        }

        if (!department) {
            return res.status(404).json({
                title: i18n.__('DEPARTMENT_GET_ERROR_TITLE'),
                error: {message: i18n.__('DEPARTMENT_NOT_FOUND')}
            });
        }

        res.status(200).json({
            title: i18n.__('DEPARTMENT_GET_SUCCESS_TITLE'),
            obj: department
        });
    });

};

//========================================
// Update Department By ID
//========================================
exports.updateDepartment = (req, res, next) => {

    const newName = req.body.name;
    const newShortName = req.body.shortName;
    const newInformation = req.body.information;
    const newUniversityId = req.body.universityId;

    University.findById(newUniversityId)
        .then((university) => {

            if (!university) {
                return res.status(404).json({
                    title: i18n.__('DEPARTMENT_UPDATE_ERROR_TITLE'),
                    error: {message: i18n.__('UNIVERSITY_NOT_FOUND')}
                });
            }

            // Department.findById(req.params.id).select('_id name shortName information').exec((err, department) => {
            Department.findById(req.params.id).select('_id name shortName information').exec()
                .then((department) => {

                    if (!department) {
                        return res.status(404).json({
                            title: i18n.__('DEPARTMENT_UPDATE_ERROR_TITLE'),
                            error: {message: i18n.__('DEPARTMENT_NOT_FOUND')}
                        });
                    }

                    department.name = newName;
                    department.shortName = newShortName;
                    department.information = newInformation;
                    department.university = newUniversityId;

                    department.save()
                        .then((updatedDepartment) => {

                            return res.status(200).json({
                                title: i18n.__('DEPARTMENT_UPDATE_TITLE'),
                                obj: updatedDepartment
                            });
                        });

                });
        })
        .then((response)=> {
            return response;
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        })

};

//========================================
// Delete Department By ID
//========================================

exports.deleteDepartment = (req, res, next) => {

    const departmentId = req.params.id;

    Department.findById(departmentId)
        .then((department)=> {

            if (!department) {
                return res.status(404).json({
                    title: i18n.__('DEPARTMENT_DELETE_ERROR_TITLE'),
                    error: {message: i18n.__('DEPARTMENT_NOT_FOUND')}
                });
            }

            University.findOneAndUpdate({_id: department.university}, {$pull: {departments: departmentId}});

            department.remove();

            return res.status(200).json({
                title: i18n.__('DEPARTMENT_DELETE_SUCCESS_TITLE'),
                obj: department
            });
        })
        .then((response)=> {
            return response;
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        })

};

exports.getMyDepartment = (req, res, next) => {
    const userId = req.user._id;

    Profile.findOne({user: userId}, (err, profile)=> {
        if (profile && profile.defaultDepartment) {

            const defaultDepartment = profile.defaultDepartment;

            Department.findById(defaultDepartment, (err, department) => {
                if (err) {
                    return next(err);
                }

                if (!department) {
                    return res.status(404).json({
                        title: i18n.__('DEPARTMENT_GET_ERROR_TITLE'),
                        error: {message: i18n.__('DEPARTMENT_NOT_FOUND')}
                    });
                }

                res.status(200).json({
                    title: i18n.__('DEPARTMENT_GET_SUCCESS_TITLE'),
                    obj: department

                });
            });
        } else {

            return res.status(404).json({
                title: i18n.__('MY_DEPARTMENT_GET_ERROR_TITLE'),
                error: {message: i18n.__('NO_DEFAULT_DEPARTMENT')}
            });

        }
    });

};

exports.getAllDepartmentsOfUniversity = (req, res, next)=> {

    University.findById(req.params.id)
        .populate('departments', "_id name")
        // .select('department.name')
        .exec()
        .then((result) => {
            return res.status(200).json({
                title: i18n.__('DEPARTMENT_GET_ALL_SUCCESS_TITLE'),
                obj: result
            });
        })
        .then((response)=> {
            return response;
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        })

};