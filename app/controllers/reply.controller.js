/**
 * Created by bdrosatos on 20/10/2016.
 */

"use strict";

const aws = require('aws-sdk');
aws.config.loadFromPath('app//config/aws-config.json');


const i18n = require('i18n'),
    Reply = require('../models/reply'),
    Exam = require('../models/exam'),
    paginate = require('express-paginate'),
    awsConfig = require('../config/aws-config.json'),
    logger = require('noogger');


//========================================
// Create new Reply
//========================================
exports.createReply = (req, res, next) => {
    const fileType = req.body.fileType;
    const textContent = req.body.textContent ? req.body.textContent : '';
    const topicNumber = req.body.topicNumber;
    const profile = req.user.profile;

    const examId = req.params.id;

    //EXTRA INFORMATION FOR CREATION FILE AT AWS BUCKET AS HIDDEN FIELDS , TODO: FIND A BETTER A WAY
    // const courseId = req.body.courseId;
    // const courseYear = req.body.courseYear;
    // const courseExamType = req.body.courseExamType;
    //
    // if (!courseId || !courseYear || !courseExamType) {
    //     return res.status(422).json({
    //         title: i18n.__('REPLY_CREATE_ERROR_TITLE'),
    //         error: {message: 'Prepei na dwsoyme extra (hidden) fields stin html (i postman) me courseId, courseYear, courseExamType'}
    //     });
    // }
    //

    if (!profile) {
        return res.status(422).json({
            title: i18n.__('REPLY_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('USER_HAS_NOT_PROFILE')}
        });
    }

    if (!fileType) {
        return res.status(422).json({
            title: i18n.__('REPLY_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('REPLY_FILE_TYPE_REQUIRED')}
        });
    }

    if (!topicNumber) {
        return res.status(422).json({
            title: i18n.__('REPLY_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('REPLY_TOPIC_NUMBER_REQUIRED')}
        });
    }

    if(fileType == 'TXT' && textContent=='') {
        return res.status(422).json({
            title: i18n.__('REPLY_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('REPLY_TOPIC_TEXT_REQUIRED')}
        });
    }

    Exam.findById(examId)
        .populate('course')
        .exec()
        .then((exam)=> {

            const courseId = exam.course._id;
            const courseYear = exam.examType;
            const courseExamType = exam.course.courseType;

            if (!exam) {
                return res.status(404).json({
                    title: i18n.__('REPLY_CREATE_ERROR_TITLE'),
                    error: {message: i18n.__('EXAM_NOT_FOUND')}
                });
            }

            let reply = new Reply({
                fileType: fileType,
                textContent: textContent,
                topicNumber: topicNumber,
                exam: examId,
                profile: profile
            });


            reply.save((err, newReply) => {
                if (err) {
                    return next(err);
                }



                let files; // Array of files
                switch (fileType) {
                    case 'PDF':
                    case 'IMG':
                        files = req.files['files'];
                        newReply.filePath = files[0].key;
                        break;
                    case 'TXT':
                        const key = `courses/${courseId}/replies/${courseYear}/${courseExamType}/topics/${topicNumber}/reply.txt`;
                        const params = {Bucket: 'uni-exams-uploads', Key: key};
                        const s3 = new aws.S3({signatureVersion: 'v4', params: params});
                        s3.upload({Body: textContent}, function (err) {
                            if (err) {
                                throw err;
                            }
                        });
                        newReply.filePath = key;
                        break;
                    default:
                        return res.status(422).json({
                            title: i18n.__('REPLY_CREATE_ERROR_TITLE'),
                            error: {message: i18n.__('REPLY_FILE_TYPE_ERROR')}
                        });
                }

                newReply.save((err) => {
                    if (err) {
                        throw(err);
                    }

                    return res.status(201).json({
                        title: i18n.__('REPLY_CREATE_SUCCESS_TITLE'),
                        obj: newReply
                    });
                });


            });
        })
        .catch((err) => {
            console.log(err);
            logger.critical(JSON.stringify(err));
            return next(err);
        })
};

//========================================
// Get Reply List
//========================================
exports.getReplyList = (req, res, next) => {
    logger.info(`GET: /replies/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : getReplyList (${JSON.stringify(req.body)})`);

    const topicNumber = req.query.topicNumber;
    const examId = req.query.examId;

    let query = {};

    if (examId) {
        query.exam = examId;
    }

    if (topicNumber) {
        query.topicNumber = topicNumber;
    }

    Reply.paginate(query,
        {
            page: req.query.page,
            limit: 5,
            sort: '-isApproved -upvotes -createdAt',
            populate: [
                {
                    path: 'profile',
                    select: '_id firstName lastName imgUrl user'
                },
                {
                    path: 'exam',
                    populate: {
                        path: 'profile',
                        model: 'Profile'
                    }
                }
            ]
        })
        .then((result) => {

            let replies = result.docs;

            res.status(200).json({
                has_more: paginate.hasNextPages(req)(result.pages),
                total_pages: result.pages,
                obj: replies
            });

        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            next(err);
        });
};

//========================================
// Get Reply by Id
//========================================
exports.getReplyId = (req, res, next) => {
    logger.info(`GET: /replies/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : getReplyId (${JSON.stringify(req.body)})`);

    const replyId = req.params.id;

    Reply.findById(replyId)
        .populate({
            path: 'profile',
            select: '_id firstName lastName imgUrl user'
        })
        .exec()
        .then((reply) => {

            if (!reply) {
                return res.status(404).json({
                    title: i18n.__('REPLY_GET_ERROR_TITLE'),
                    error: {message: i18n.__('REPLY_NOT_FOUND')}
                });
            }

            switch (reply.fileType) {
                case 'IMG':
                case 'PDF':
                    reply.filePath = awsConfig.downloadUrl + reply.filePath;
                    return res.status(200).json({
                        title: i18n.__('REPLY_GET_SUCCESS_TITLE'),
                        obj: reply
                    });

                case 'TXT':
                    return res.status(200).json({
                        title: i18n.__('REPLY_GET_SUCCESS_TITLE'),
                        obj: reply
                    });
            }

        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            next(err);
        });

};

//========================================
// Add Upvote to Reply
//========================================
exports.addUpvote = (req, res, next) => {
    logger.info(`POST: /replies/upvote:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : addUpvote (${JSON.stringify(req.body)})`);

    const replyId = req.params.id;
    const currentUser = req.user;

    Reply.findById(replyId).exec()
        .then((reply) => {
            if (!reply) {
                return res.status(404).json({
                    title: i18n.__('REPLY_UPVOTE_ERROR_TITLE'),
                    error: {message: i18n.__('REPLY_NOT_FOUND')}
                });
            }

            //Check if user has already upvote
            if (reply.upvoteUsers.find(userId =>currentUser._id.equals(userId))) {
                return res.status(422).json({
                    title: i18n.__('REPLY_UPVOTE_ERROR_TITLE'),
                    error: {message: i18n.__('USER_ALREADY_UPVOTE')}
                });
            }

            //Otherwise add one to upvotes and store userId
            reply.upvoteUsers.push(currentUser._id);
            reply.upvotes++;

            return reply.save()

        })
        .then((reply) => {
            return res.status(200).json({
                title: i18n.__('REPLY_UPVOTE_SUCCESS_TITLE'),
                obj: reply
            });
        })
        .catch((err)=> {
            logger.critical(JSON.stringify(err));
            next(err);
        });

};


//========================================
// Remove Upvote to Reply
//========================================
exports.removeUpvote = (req, res, next) => {
    logger.info(`PATCH: /replies/remove/upvote:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : removeUpvote (${JSON.stringify(req.body)})`);

    const replyId = req.params.id;
    const currentUser = req.user;

    Reply.findById(replyId).exec()
        .then((reply) => {
            if (!reply) {
                return res.status(404).json({
                    title: i18n.__('REPLY_REMOVE_UPVOTE_ERROR_TITLE'),
                    error: {message: i18n.__('REPLY_NOT_FOUND')}
                });
            }

            //Check if user has already upvote
            if (!reply.upvoteUsers.find(userId =>currentUser._id.equals(userId))) {
                return res.status(422).json({
                    title: i18n.__('REPLY_REMOVE_UPVOTE_ERROR_TITLE'),
                    error: {message: i18n.__('USER_HAS_NOT_UPVOTE')}
                });
            }

            //Otherwise subtract one from upvotes and remove userId
            reply.upvoteUsers.splice(reply.upvoteUsers.indexOf(currentUser._id), 1);
            reply.upvotes--;

            if (reply.upvotes < 0) reply.upvotes = 0;

            return reply.save()

        })
        .then((reply) => {
            return res.status(200).json({
                title: i18n.__('REPLY_DELETE_UPVOTE_SUCCESS_TITLE'),
                obj: reply
            });
        })
        .catch((err)=> {
            logger.critical(JSON.stringify(err));
            next(err);
        });

};

//========================================
// Mark Approve to Reply
//========================================
exports.markApprove = (req, res, next) => {
    logger.info(`POST: /mark/approve:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : markApprove (${JSON.stringify(req.body)})`);

    const replyId = req.params.id;

    Reply.findById(replyId).exec()
        .then((reply) => {
            if (!reply) {
                return res.status(404).json({
                    title: i18n.__('REPLY_APPROVE_ERROR_TITLE'),
                    error: {message: i18n.__('REPLY_NOT_FOUND')}
                });
            }

            Reply.update({}, {isApproved : false}, {multi : true}).exec()
                .then(() => {
                    reply.isApproved = true;
                    return reply.save()
                })
                .then((reply) => {
                    return res.status(200).json({
                        title: i18n.__('REPLY_APPROVE_SUCCESS_TITLE'),
                        obj: reply
                    });
                })

        })
        .catch((err)=> {
            logger.critical(JSON.stringify(err));
            next(err);
        });

};

//========================================
// Unmark Upvote to Reply
//========================================
exports.unMarkApprove = (req, res, next) => {
    logger.info(`POST: /unmark/approve:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : unMarkApprove (${JSON.stringify(req.body)})`);

    const replyId = req.params.id;

    Reply.findById(replyId).exec()
        .then((reply) => {
            if (!reply) {
                return res.status(404).json({
                    title: i18n.__('REPLY_DISAPPROVE_ERROR_TITLE'),
                    error: {message: i18n.__('REPLY_NOT_FOUND')}
                });
            }

            reply.isApproved = false;
            return reply.save()
        })
        .then((reply) => {
            return res.status(200).json({
                title: i18n.__('REPLY_DISAPPROVE_SUCCESS_TITLE'),
                obj: reply
            });
        })
        .catch((err)=> {
            logger.critical(JSON.stringify(err));
            next(err);
        });

};

//========================================
// Delete Reply By Id
//========================================
exports.deleteReply = (req, res, next) => {
    logger.info(`DELETE: /replies/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : deleteReply (${JSON.stringify(req.body)})`);

    const replyId = req.params.id;

    Reply.findById(replyId).exec()
        .then((reply) => {

            if (!reply) {
                return res.status(404).json({
                    title: i18n.__('REPLY_DELETE_ERROR_TITLE'),
                    error: {message: i18n.__('REPLY_NOT_FOUND')}
                });
            }


            reply.remove((err, deletedReply) => {
                if (err) {
                    next(err);
                }

                if (!deletedReply) {
                    return res.status(404).json({
                        title: i18n.__('REPLY_DELETE_ERROR_TITLE'),
                        error: {message: i18n.__('REPLY_NOT_FOUND')}
                    });
                }

                //Delete aws s3 file key
                let params = {Bucket: 'uni-exams-uploads'};
                params.Delete = {Objects: []};
                params.Delete.Objects.push({Key: deletedReply.filePath});
                const s3 = new aws.S3({signatureVersion: 'v4', params: params});
                s3.deleteObjects(params, function (err, data) {
                    if (err) throw err
                });

                res.status(200).json({
                    title: i18n.__('REPLY_DELETE_SUCCESS_TITLE'),
                    obj: deletedReply
                });

            });


        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            next(err);
        });
};

