/**
 * Created by bdrosatos on 6/10/2016.
 */

"use strict";


const i18n = require('i18n'),
    Department = require('../models/department'),
    Course = require('../models/course'),
    Profile = require('../models/profile'),
    paginate = require('express-paginate'),
    logger = require('noogger');


//Private methods


//

//========================================
// Create new course Route
//========================================

exports.createCourse = (req, res, next) => {
    const name = req.body.name;
    const courseType = req.body.courseType;
    const information = req.body.information;

    const departmentId = req.params.id;

    if (!name || !courseType) {
        return res.status(422).json({
            title: i18n.__('COURSE_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('COURSE_NO_REQUIRED_FIELDS')}
        });
    }

    Department.findById(departmentId, (err, department) => {
        if (err) {
            next(err);
        }

        if (!department) {
            return res.status(404).json({
                title: i18n.__('COURSE_CREATE_ERROR_TITLE'),
                error: {message: i18n.__('DEPARTMENT_NOT_FOUND')}
            });
        }

        const course = new Course({
            name: name,
            courseType: courseType,
            information: information,
            department: departmentId
        });

        course.save((err, newCourse) => {
            if (err) {
                next(err);
            }

            department.courses.push(newCourse);

            department.save((err) => {
                if (err) {
                    next(err);
                }

                res.status(201).json({
                    title: i18n.__('COURSE_CREATE_SUCCESS_TITLE'),
                    obj: newCourse
                })
            });

        });
    });
};

//========================================
// Get List Courses Route
//========================================

exports.getCoursesList = (req, res, next) => {

    const profileId = req.user.profile;
    let query = {};

    if (req.query.depId) {
        query.department = req.query.depId
    }

    if (req.query.name) {
        query['name'] = {'$regex': req.query.name, '$options': 'i'};
    }

    Course.paginate(query, {
        page: req.query.page,
        populate: 'department',
        sort: 'name'
    }, (err, result) => {
        if (err)
            return next(err);

        let courses = result.docs;

        Profile.findById(profileId, (err, profile) => {
            const followingExams = profile.followCourses;


            for (let i = 0; i < courses.length; i++) {
                // console.log(courses[i]);
                //checks if the course is on followList
                if (followingExams.indexOf(courses[i]._id) > -1) {
                    courses[i].isFollowed = true;
                } else {
                    courses[i].isFollowed = false;
                }

            }
            res.status(200).json({
                has_more: paginate.hasNextPages(req)(result.pages),
                total_pages: result.pages,
                total: result.total,
                obj: courses
            });
        });

    });

};

//========================================
// Get Course By ID
//========================================

exports.getCourseById = (req, res, next) => {

    const profileId = req.user.profile;
    const courseId = req.params.id;

    Course.findById(courseId).populate('department').exec((err, course) => {
        if (err) {
            next(err);
        }

        if (!course) {
            return res.status(404).json({
                title: i18n.__('COURSE_GET_ERROR_TITLE'),
                error: {message: i18n.__('COURSE_NOT_FOUND')}
            });
        }

        Profile.findById(profileId, (err, profile) => {
            const followingExams = profile.followCourses;

            //checks if the course is on followList, but with style ;)
            if (followingExams.indexOf(course.id) > -1) {
                course.isFollowed = true;
            } else {
                course.isFollowed = false;
            }

            res.status(200).json({
                title: i18n.__('COURSE_GET_SUCCESS_TITLE'),
                obj: course,
            });
        });


    });

};

//========================================
// Update Course By ID
//========================================
exports.updateCourse = (req, res, next) => {

    const newName = req.body.name;
    const newCourseType = req.body.courseType;
    const newInformation = req.body.information;

    Course.findById(req.params.id).select('_id name courseType information').exec((err, course) => {
        if (err) {
            next(err);
        }

        if (!course) {
            return res.status(404).json({
                title: i18n.__('COURSE_UPDATE_ERROR_TITLE'),
                error: {message: i18n.__('COURSE_NOT_FOUND')}
            });
        }

        course.name = newName;
        course.courseType = newCourseType;
        course.information = newInformation;

        course.save((err, updatedCourse) => {
            if (err) {
                next(err);
            }

            res.status(200).json({
                title: i18n.__('COURSE_UPDATE_TITLE'),
                obj: updatedCourse
            });
        });

    });

};

//========================================
// Delete Course By ID
//========================================

exports.deleteCourse = (req, res, next) => {

    Course.findById(req.params.id, (err, course) => {
        if (err) {
            next(err);
        }

        if (!course) {
            return res.status(404).json({
                title: i18n.__('COURSE_DELETE_ERROR_TITLE'),
                error: {message: i18n.__('COURSE_NOT_FOUND')}
            });
        }

        Department.findOneAndUpdate({_id: course.department}, {$pull: {courses: course}}).exec();


        course.remove((err) => {
            if (err) next(err);
            res.status(200).json({
                title: i18n.__('COURSE_DELETE_SUCCESS_TITLE'),
                obj: course
            });
        });

    });

};