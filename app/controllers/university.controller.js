/**
 * Created by stavr on 10/3/2016.
 */

"use strict";


const i18n = require('i18n'),
    University = require('../models/university'),
    paginate = require('express-paginate'),
    logger = require('noogger');


//Model University
// _id: String
// *name: String
// *shortName: String
// information: String
// departments: Department[]


//===============================
//   Create University Route   //
//===============================
exports.createUniversity = (req, res, next) => {
    logger.info(`POST: /universities/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : createUniversity (${JSON.stringify(req.body)})`);

    const name = req.body.name;
    const shortName = req.body.shortName;
    const information = req.body.information;

    if (!name) {
        return res.status(422).json({
            title: i18n.__('UNIVERSITY_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('UNIVERSITY_CREATE_ERROR_NAME ')}
        });
    }

    if (!shortName) {
        return res.status(422).json({
            title: i18n.__('UNIVERSITY_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('UNIVERSITY_CREATE_ERROR_SHORT_NAME ')}
        });
    }

    University.findOne({'name': name}).exec()
        .then((existUniversity) => {
            if (existUniversity) {
                return res.status(422).json({
                    title: i18n.__('UNIVERSITY_CREATE_ERROR_TITLE'),
                    error: {message: i18n.__('UNIVERSITY_CREATE_ERROR_EXISTS ')}
                })
            }

            const university = new University({
                name: name,
                shortName: shortName,
                information: information
            });

            university.save()
                .then((university) => {
                    return res.status(201).json({
                        title: i18n.__('UNIVERSITY_CREATE_SUCCESS_TITLE'),
                        obj: university
                    })
                })
        })
        .then((response) => {
            return response;
        })
        .catch((err) => {
            console.log(err);
            logger.critical(JSON.stringify(err));
            return next(err);
        })
};

//===============================
//  Get All Universities Route   //
//===============================

exports.getAllUniversities = (req, res, next) => {
    logger.info(`GET: /universities/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : getAllUniversities (${JSON.stringify(req.body)})`);

    const queryParams = {};

    if (req.query.name) {
        queryParams['name'] = {'$regex': req.query.name, '$options': 'i'};
    }

    if (req.query.shortName) {
        queryParams['shortName'] = {'$regex': req.query.shortName, '$options': 'i'};
    }

    if (req.query.page) {


        University.paginate(queryParams, {
            page: req.query.page,
            sort: 'name',
            populate: {
                path: 'departments',
                select: '_id name shortName information'
            }
        })
            .then((result) => {

                const universities = result.docs;

                res.status(200).json({
                    has_more: paginate.hasNextPages(req)(result.pages),
                    total_pages: result.pages,
                    total: result.total,
                    obj: universities
                });

            })
            .catch((err) => {
                logger.critical(JSON.stringify(err));
                return next(err);

            });

    } else {
        University.find({}).select('_id name').exec()
            .then((result) => {
                return res.status(200).json({
                    title: i18n.__('UNIVERSITY_GET_MAP_SUCCESS_TITLE'),
                    obj: result
                });
            })
            .catch((err) => {
                logger.critical(JSON.stringify(err));
                return next(err);
            });
    }
};

//===============================
//  Get University By ID Route   //
//===============================
exports.getUniversityById = (req, res, next) => {
    logger.info(`GET: /universities/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : getUniversityById (${JSON.stringify(req.body)})`);

    University.findById(req.params.id)
        .populate({
            path: 'departments',
            select: '_id name shortName information'
        }).exec()
        .then((university) => {
            if (!university) {
                return res.status(404).json({
                    title: i18n.__('UNIVERSITY_GET_ERROR_TITLE'),
                    error: {message: i18n.__('UNIVERSITY_NOT_FOUND')}
                });
            }

            return res.status(200).json({
                title: i18n.__('UNIVERSITY_GET_SUCCESS_TITLE'),
                obj: university
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        });
};


//========================================
// Delete University By id
//========================================
exports.deleteUniversity = (req, res, next) => {
    logger.info(`DELETE: /universities/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : deleteUniversity (${JSON.stringify(req.body)})`);

    const uniId = req.params.id;

    University.findById(uniId).exec()
        .then((university) => {

            if (!university) {
                return res.status(404).json({
                    title: i18n.__('UNIVERSITY_DELETE_ERROR_TITLE'),
                    error: {message: i18n.__('UNIVERSITY_NOT_FOUND')}
                });
            }

            university.remove((err) => {
                if (err) {
                    logger.critical(JSON.stringify(err));
                    next(err);
                }

                res.status(200).json({
                    title: i18n.__('UNIVERSITY_DELETE_TITLE'),
                    obj: university
                });
            });


        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err);
        });
};

//========================================
// Update University By id
//========================================
exports.updateUniversity = (req, res, next) => {
    logger.info(`PATCH: /universities/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : updateUniversity (${JSON.stringify(req.body)})`);

    const newName = req.body.name;
    const newShortName = req.body.shortName;
    const newInformation = req.body.information;

    University.findById(req.params.id).select('_id name shortName information').exec()
        .then((university) => {
            if (!university) {
                return res.status(404).json({
                    title: i18n.__('UNIVERSITY_UPDATE_ERROR_TITLE'),
                    error: {message: i18n.__('UNIVERSITY_NOT_FOUND')}
                });
            }

            if (newName) university.name = newName;
            if (newShortName) university.shortName = newShortName;
            if (newInformation) university.information = newInformation;

            return university.save();

        })
        .then((university) => {
            return res.status(200).json({
                title: i18n.__('UNIVERSITY_UPDATE_TITLE'),
                obj: university
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            next(err);
        });

};
