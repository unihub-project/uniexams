/**
 * Created by bdrosatos on 25/9/2016.
 */

"use strict";


const i18n = require('i18n'),
    fs = require('fs'),
    logger = require('noogger');

//========================================
// Language change Route
//========================================

exports.changeLanguage = (req, res, next) => {
    logger.info(`POST: /language/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : changeLanguage (${JSON.stringify(req.body)})`);
    i18n.setLocale(req.params['l']);
    res.locals.language = req.params['l'];
    return res.status(200).json({
        title: 'Language changed Successfully',
        message: `Set to: ${i18n.getLocale()}`
    })
};