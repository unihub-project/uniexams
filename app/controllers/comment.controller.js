/**
 * Created by stavros on 18/10/2016.
 */
'use strict';

const i18n = require('i18n'),
    paginate = require('express-paginate'),
    async = require('async'),
    Reply = require('../models/reply'),
    Profile = require('../models/profile'),
    Comment = require('../models/comment'),
    logger = require('noogger');

exports.createComment = (req, res, next) => {
    logger.info(`POST: /comments/reply/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : createComment (${JSON.stringify(req.body)})`);
    const replyId = req.params.id;
    const profile = req.user.profile;
    const content = req.body.content | '';

    Reply.findById(replyId).exec()
        .then((reply) => {
            if (!reply) {
                return res.status(404).json({
                    title: i18n.__('COMMENT_CREATE_ERROR'),
                    error: {message: i18n.__('COMMENT_CREATE_ERROR_REPLY_NOT_EXISTS')}
                });
            }
            const comment = new Comment({
                content: content,
                isPartOfSolution: false,
                reply: replyId,
                profile: profile
            });

            comment.save((err, result) => {
                if (err) {
                    return next(err);
                }

                return res.status(200).json({
                    title: i18n.__('COMMENT_CREATE_TITLE_SUCCESS'),
                    obj: result
                });
            });
        })
        .catch((err) => {

            return next(err)
        })
};


//========================================
// Get Comment List
//========================================
exports.getCommentList = (req, res, next) => {
    logger.info(`GET: /comments/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : getReplyList (${JSON.stringify(req.body)})`);

    const replyId = req.query.replyId;

    let query = {};

    if (replyId) {
        query.reply = replyId;
    }

    Comment.paginate(query,
        {
            page: req.query.page,
            limit: 5,
            sort: '-createdAt',
            populate: [
                {
                    path: 'profile',
                    select: '_id firstName lastName imgUrl user'
                }
            ]
        })
        .then((result) => {

            let comments = result.docs;

            res.status(200).json({
                has_more: paginate.hasNextPages(req)(result.pages),
                total_pages: result.pages,
                obj: comments
            });

        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            next(err);
        });
};

exports.getCommentById = (req, res, next) => {
    logger.info(`GET: /comments/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : getCommentById (${JSON.stringify(req.body)})`);
    const id = req.params.id;


    Comment.findById(id).populate('profile').exec()
        .then((comment) => {
            if (!comment) {
                return res.status(404).json({
                    title: i18n.__('COMMENT_GET_ERROR'),
                    error: {message: i18n.__('COMMENT_NOT_EXISTS')}
                });
            }

            res.status(200).json({
                title: i18n.__('COMMENT_GET_SUCCESS_TITLE'),
                obj: comment
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err)
        })
};


exports.updateComment = (req, res, next) => {
    logger.info(`PATCH: /comments/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : updateComment (${JSON.stringify(req.body)})`);
    const id = req.params.id;

    const content = req.body.content;

    Comment.findById(id).exec()
        .then((comment) => {
            if (!comment) {
                return res.status(404).json({
                    title: i18n.__('COMMENT_UPDATE_ERROR'),
                    error: {message: i18n.__('COMMENT_NOT_EXISTS')}
                });
            }

            comment.content = content;

            return comment.save();
        })
        .then((comment) => {
            res.status(200).json({
                title: i18n.__('COMMENT_UPDATE_SUCCESS_TITLE'),
                obj: comment
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err)
        })
};

exports.deleteComment = (req, res, next) => {
    logger.info(`DELETE: /comments/params:${JSON.stringify(req.params)},query:${JSON.stringify(req.query)} : deleteComment (${JSON.stringify(req.body)})`);
    const id = req.params.id;


    Comment.findById(id).exec()
        .then((comment) => {
            if (!comment) {
                return res.status(404).json({
                    title: i18n.__('COMMENT_GET_ERROR'),
                    error: {message: i18n.__('COMMENT_NOT_EXISTS')}
                });
            }

            comment.remove((err) => {
                if (err) {
                    logger.critical(JSON.stringify(err));
                    next(err);
                }
                res.status(200).json({
                    title: i18n.__('COMMENT_DELETE_SUCCESS_TITLE'),
                    obj: comment
                });
            });
        })
        .catch((err) => {
            logger.critical(JSON.stringify(err));
            return next(err)
        });
};
