/**
 * Created by bdrosatos on 9/10/2016.
 */

"use strict";

const aws = require('aws-sdk');
aws.config.loadFromPath('app//config/aws-config.json');


const i18n = require('i18n'),
    Exam = require('../models/exam'),
    Course = require('../models/course'),
    paginate = require('express-paginate'),
    awsConfig = require('../config/aws-config.json'),
    logger = require('noogger');


//========================================
// Create new Exam
//========================================
exports.createExam = (req, res, next) => {
    const year = req.body.year;
    const examType = req.body.examType;
    const information = req.body.information;
    const numberOfTopics = req.body.numberOfTopics;
    const fileType = req.body.fileType;
    const textContent = req.body.textContent ? req.body.textContent : '';
    const profile = req.user.profile;

    if (!profile) {
        return res.status(422).json({
            title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('USER_HAS_NOT_PROFILE')}
        });
    }

    if (!year) {
        return res.status(422).json({
            title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('COURSE_YEAR_REQUIRED')}
        });
    }

    if (!examType) {
        return res.status(422).json({
            title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('COURSE_EXAM_TYPE_REQUIRED')}
        });
    }

    if (!numberOfTopics) {
        return res.status(422).json({
            title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('COURSE_NUMBER_OF_TOPICS_REQUIRED')}
        });
    }

    if (!fileType) {
        return res.status(422).json({
            title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('COURSE_FILE_TYPE_REQUIRED')}
        });
    }

    Course.findById(req.params.id, (err, course) => {
        if (err) {
            next(err);
        }

        if (!course) {
            return res.status(404).json({
                title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
                error: {message: i18n.__('COURSE_NOT_FOUND')}
            });
        }

        let exam = new Exam({
            year: year,
            examType: examType,
            numberOfTopics: numberOfTopics,
            fileType: fileType,
            textContent: textContent,
            information: information,
            course: req.params.id,
            profile: profile
        });


        exam.save((err, newExam) => {
            if (err) {
                return res.status(422).json({
                    title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
                    error: {message: i18n.__('EXAM_UNIQUE_ERROR')}
                });
            }

            let files; // Array of files
            switch (fileType) {
                case 'PDF':
                case 'IMG':
                    files = req.files['files'];
                    files.forEach((file) => {
                        newExam.filePath.push(file.key);
                    });
                    break;
                case 'TXT':
                    const key = `courses/${req.params.id}/exams/${req.body.year}/${req.body.examType}/exam.txt`;
                    const params = {Bucket: 'uni-exams-uploads', Key: key};
                    const s3 = new aws.S3({signatureVersion: 'v4', params: params});
                    s3.upload({Body: textContent}, function (err) {
                        if (err) {
                            throw err;
                        }
                    });


                    newExam.filePath.push(key);
                    break;
                default:
                    return res.status(422).json({
                        title: i18n.__('EXAM_CREATE_ERROR_TITLE'),
                        error: {message: i18n.__('EXAM_FILE_TYPE_ERROR')}
                    });
            }

            newExam.save((err, exam) => {
                if (err) {
                    throw(err);
                }

                course.exams.push(exam);

                course.save((err) => {
                    if (err) {
                        throw(err);
                    }
                    return res.status(201).json({
                        title: i18n.__('EXAM_CREATE_SUCESS_TITLE'),
                        obj: newExam
                    });
                });
            });
        });
    });
};

//========================================
// Get Exam List
//========================================
exports.getExamList = (req, res, next) => {

    let query = {};

    if (req.query.courseId) {
        query = {
            course: req.query.courseId
        }
    }

    Exam.paginate(query, {page: req.query.page, populate: 'course'}, (err, result) => {
        if (err)
            return next(err);

        let exams = result.docs;

        res.status(200).json({
            has_more: paginate.hasNextPages(req)(result.pages),
            total_pages: result.pages,
            obj: exams
        });

    });
};

//========================================
// Get Exam by Id
//========================================
exports.getExamById = (req, res, next) => {

    const examId = req.params.id;

    Exam.findById(examId)
        .populate({
            path: 'course',
            select: '_id name'
        })
        .populate({
            path: 'profile',
            select: '_id firstName lastName user'
        })
        .exec((err, exam) => {
            if (err) {
                next(err);
            }

            if (!exam) {
                return res.status(404).json({
                    title: i18n.__('EXAM_GET_ERROR_TITLE'),
                    error: {message: i18n.__('EXAM_NOT_FOUND')}
                });
            }

            switch (exam.fileType) {

                case 'IMG':
                case 'PDF':
                    exam.filePath.forEach((key, i) => {
                        exam.filePath[i] = awsConfig.downloadUrl + key;
                    });

                    return res.status(200).json({
                        title: i18n.__('EXAM_GET_SUCCESS_TITLE'),
                        obj: exam
                    });

                case 'TXT':
                    return res.status(200).json({
                        title: i18n.__('EXAM_GET_SUCCESS_TITLE'),
                        obj: exam
                    });
            }

        });

};


//========================================
// Delete Exam By Id
//========================================
exports.deleteExam = (req, res, next) => {

    const examId = req.params.id;

    Exam.findById(examId, (err, exam) => {
        if (err) {
            next(err);
        }

        if (!exam) {
            return res.status(404).json({
                title: i18n.__('EXAM_DELETE_ERROR_TITLE'),
                error: {message: i18n.__('EXAM_NOT_FOUND')}
            });
        }


        Course.findOneAndUpdate({_id: exam.course}, {$pull: {exams: examId}}, (err) => {
            if (err) {
                next(err);
            }

            exam.remove((err, deletedExam) => {
                if (err) {
                    next(err);
                }

                if (!deletedExam) {
                    return res.status(404).json({
                        title: i18n.__('EXAM_DELETE_ERROR_TITLE'),
                        error: {message: i18n.__('EXAM_NOT_FOUND')}
                    });
                }

                //Delete aws s3 file key
                let params = {Bucket: 'uni-exams-uploads'};
                params.Delete = {Objects: []};
                deletedExam.filePath.forEach((key) => {
                    params.Delete.Objects.push({Key: key});
                });
                const s3 = new aws.S3({signatureVersion: 'v4', params: params});
                s3.deleteObjects(params, function (err, data) {
                    if (err) throw err
                });

                res.status(200).json({
                    title: i18n.__('EXAM_DELETE_SUCCESS_TITLE'),
                    obj: deletedExam
                });

            });
        });

    });
};

