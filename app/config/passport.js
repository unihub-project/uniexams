/**
 * Created by bdrosatos on 25/9/2016.
 */
const passport = require('passport'),
    config = require('./app'),
    i18n = require('i18n'),
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,
    LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    User = require('../models/user'),
    Profile = require('../models/profile');

const localOptions = {usernameField: 'email'};

const facebookOptions = {
    clientID: config.facebookAuth.clientID,
    clientSecret: config.facebookAuth.clientSecret,
    callbackURL: config.facebookAuth.callbackURL,
    profileFields: ['id', 'name', 'photos', 'email']
};

// Setting up local login strategy
const localLogin = new LocalStrategy(localOptions, function (email, password, done) {
    User.findOne({email: email}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(null, false, {message: i18n.__('LOGIN_ERROR_WRONG_CREDENTIALS')});
        }

        if (!user.password) {
            return done(null, false, {message: i18n.__('LOGIN_ERROR_SOCIAL_AUTHENTICATION')});

        }

        user.comparePassword(password, function (err, isMatch) {
            if (err) {
                return done(err);
            }
            if (!isMatch) {
                return done(null, false, {message: i18n.__('LOGIN_ERROR_WRONG_CREDENTIALS')});
            }

            return done(null, user);
        });
    });
});

// Setting up facebook login strategy
const facebookLogin = new FacebookStrategy(facebookOptions, function (token, refreshToken, profile, done) {
        process.nextTick(function () {
            User.findOne({'facebook.id': profile.id})
                .populate({
                    path: 'profile',
                    select: '_id firstName lastName defaultDepartment imgUrl followExams'
                })
                .exec(function (err, user) {
                    if (err)
                        return done(err);
                    if (user) {
                        return done(null, user);
                    } else {

                        User.findOne({'email': profile.emails[0].value}, (err, existedUser) => {

                            if (err) return done(err);
                            if (existedUser) return done({
                                title: i18n.__('REGISTER_ERROR_TITLE'),
                                message: i18n.__('EXIST_EMAIL')
                            });

                            const newUser = new User();
                            newUser.facebook.id = profile.id;
                            newUser.facebook.token = token;
                            newUser.email = profile.emails[0].value;
                            newUser.isConfirmed = true;
                            newUser.save(function (err, user) {
                                if (err)
                                    throw err;

                                const newProfile = new Profile({
                                    firstName: profile.name.givenName,
                                    lastName: profile.name.familyName,
                                    user: user._id
                                });

                                newProfile.save(function (err, profile) {
                                    if (err)
                                        throw err;
                                    user.profile = profile._id;
                                    user.save(function (err, updatedUser) {
                                        if (err)
                                            throw err;

                                        return done(null, updatedUser);
                                    });

                                });
                            });
                        });
                    }
                })
        });
    })
    ;

const jwtOptions = {
    // Telling Passport to check authorization headers for JWT
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    // Telling Passport where to find the secret
    secretOrKey: config.secret
};

// Setting up JWT login strategy
const jwtLogin = new JwtStrategy(jwtOptions, function (payload, done) {
    User.findById(payload._id)
        .populate({
            path: 'profile',
            select: '_id firstName lastName defaultDepartment imgUrl followExams'
        })
        .exec((err, user) => {
            if (err) {
                return done(err, false);
            }

            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        });
});

passport.use(jwtLogin);
passport.use(localLogin);
passport.use(facebookLogin);