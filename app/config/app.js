/**
 * Created by bdrosatos on 25/9/2016.
 */
module.exports = {
    database: 'mongodb://uni-exams:uniexams2016@ds041586.mlab.com:41586/uni-exams-db-dev',
    testDatabase: 'mongodb://uniexamstest:uniexams2016@ds057816.mlab.com:57816/uni-exams-test-db',
    secret: 'SUPERSECRETKEYFORUNIEXAMS',
    facebookAuth: {
        'clientID': '1760075784255280',
        'clientSecret': '16e45d385cb9c84f312f95c6911be5d5',
        'callbackURL': 'http://localhost:5035/api/users/authenticate/facebook/callback'
    },
    port: process.env.PORT || 5035,
    email_options: {
        auth: {
            api_user: 'johnmara',
            api_key: 'johnbill137'
        }
    }
};