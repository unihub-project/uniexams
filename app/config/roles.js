/**
 * Created by bdrosatos on 2/10/2016.
 */
const Enum = require('es6-enum');

const ROLE = Enum("Undergraduate", "Postgraduate", "Teacher", "Admin");

module.exports = ROLE;