/**
 * Created by stavros on 22/2/2017.
 */

const AuthController = require('../controllers/auth.controller'),
    UserController = require('../controllers/user.controller'),
    express = require('express'),
    passport = require('passport'),
    passportService = require('../config/passport'),
    userAuth = require('../middlewares/sameUser'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    paginate = require('express-paginate'),
    router = express.Router();


//Test if credentials are ok
router.post('/login', AuthController.login);

router.get('/testJWT', requireAuth, isOk);
router.get('/testJWTAdmin', requireAuth, AuthController.roleAuthorization('Admin'), isOk)

function isOk(req, res, next) {
    return res.status(200).json({
        title: 'isAdmin'
    });
}

module.exports = router;