"use strict";


const AppController = require('../controllers/app.controller'),
    express = require('express'),
    router = express.Router(),
    i18n = require("i18n");


// Index route, render index.pug
router.get('/', (req, res, next) => {
    res.render('index');
});

// API for change locale
router.post('/api/language/:l', AppController.changeLanguage);


module.exports = router;
