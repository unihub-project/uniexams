/**
 * Created by bdrosatos on 6/10/2016.
 */
"use strict";

const CourseController = require('../controllers/course.controller'),
    AuthController = require('../controllers/auth.controller'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    passport = require('passport'),
    paginate = require('express-paginate'),
    express = require('express'),
    router = express.Router();


router.post('/department/:id', requireAuth, AuthController.roleAuthorization('Admin'), CourseController.createCourse);

// router.get('/department/:id', requireAuth, paginate.middleware(10, 50), CourseController.getAllCoursesOfDepartment);

router.get('/', requireAuth, paginate.middleware(10, 50), CourseController.getCoursesList)
    .get('/:id', requireAuth, CourseController.getCourseById)
    .patch('/:id', requireAuth, AuthController.roleAuthorization('Admin'), CourseController.updateCourse)
    .delete('/:id', requireAuth, AuthController.roleAuthorization('Admin'), CourseController.deleteCourse);

module.exports = router;
