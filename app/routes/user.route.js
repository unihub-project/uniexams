/**
 * Created by bdrosatos on 25/9/2016.
 */

"use strict";


const AuthController = require('../controllers/auth.controller'),
    UserController = require('../controllers/user.controller'),
    express = require('express'),
    passport = require('passport'),
    passportService = require('../config/passport'),
    userAuth = require('../middlewares/sameUser'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    paginate = require('express-paginate'),
    router = express.Router();


//Authorization

//Register user
router.post('/', AuthController.register);

//Update User, params: id
router.patch('/:id', requireAuth, userAuth.sameUser(require('../models/user')), AuthController.updateUser);

//Log in user
router.post('/authenticate', AuthController.login);
//Log in user from facebook
router.get('/authenticate/facebook', passport.authenticate('facebook', {scope: 'email'}));
//Log in user from facebook callback
router.get('/authenticate/facebook/callback', AuthController.loginFromFacebook);
//Confirm user, params: token
router.get('/confirm/:token', AuthController.confirmUser);

// Get users list
router.get('/', requireAuth, AuthController.roleAuthorization('Admin'), paginate.middleware(10, 50), UserController.getUsersList);

//GET user by ID
router.get('/:id', requireAuth, AuthController.roleAuthorization('Admin'), UserController.getUserByID);

//DELETE user by ID
router.delete('/:id', requireAuth, AuthController.roleAuthorization('Admin'), UserController.deleteUser);


module.exports = router;