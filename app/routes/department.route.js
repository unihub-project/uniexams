/**
 * Created by bdrosatos on 3/10/2016.
 */

"use strict";


const DepartmentController = require('../controllers/department.controller'),
    AuthController = require('../controllers/auth.controller'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    passport = require('passport'),
    paginate = require('express-paginate'),
    express = require('express'),
    router = express.Router();


router.post('/university/:id', requireAuth, AuthController.roleAuthorization('Admin'), DepartmentController.createDepartment);
router.get('/university/:id', DepartmentController.getAllDepartmentsOfUniversity);

router.get('/', requireAuth, DepartmentController.getDepartmentList)
    .get('/my/', requireAuth, DepartmentController.getMyDepartment)
    .get('/:id', requireAuth, DepartmentController.getDepartmentById)
    .patch('/:id', requireAuth, AuthController.roleAuthorization('Admin'), DepartmentController.updateDepartment)
    .delete('/:id', requireAuth, AuthController.roleAuthorization('Admin'), DepartmentController.deleteDepartment);

module.exports = router;
