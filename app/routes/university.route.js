/**
 * Created by stavr on 10/3/2016.
 */

"use strict";


const UniversityController = require("../controllers/university.controller"),
    AuthController = require('../controllers/auth.controller.js'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    express = require('express'),
    passport = require('passport'),
    paginate = require('express-paginate'),

    router = express.Router();



router
    .post('/', requireAuth, AuthController.roleAuthorization('Admin'), UniversityController.createUniversity)
    .get('/', UniversityController.getAllUniversities)
    .get('/:id', requireAuth, UniversityController.getUniversityById) // Get University by id
    .delete('/:id', requireAuth, AuthController.roleAuthorization('Admin'), UniversityController.deleteUniversity)
    .patch('/:id', requireAuth, AuthController.roleAuthorization('Admin'), UniversityController.updateUniversity);

module.exports = router;