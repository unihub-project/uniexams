/**
 * Created by bdros on 9/28/2016.
 */

"use strict";


const ProfileController = require('../controllers/profile.controller'),
    AuthController = require('../controllers/auth.controller.js'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    express = require('express'),
    passport = require('passport'),
    paginate = require('express-paginate'),
    passportService = require('../config/passport'),
    userAuth = require('../middlewares/sameUser'),
    router = express.Router();


router
    .post('/', requireAuth, ProfileController.createProfile) //Create Profile route
    .patch('/:id', requireAuth, userAuth.sameUser(require('../models/profile')), ProfileController.updateProfile) //Update Profile [Profile Owner, Admin]
    .get('/', requireAuth, AuthController.roleAuthorization('Admin'), paginate.middleware(10, 50), ProfileController.getProfileList) //Get Profile list [ADMIN]
    .get('/:id', requireAuth, ProfileController.getProfileById) // Get profile by id
    .patch('/follow/course/:id', requireAuth, ProfileController.followCourse)
    .patch('/unfollow/course/:id', requireAuth, ProfileController.unFollowCourse);

module.exports = router;