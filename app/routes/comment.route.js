"use strict";


const commentController = require('../controllers/comment.controller'),
    userAuth = require('../middlewares/sameUser'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    AuthController = require('../controllers/auth.controller'),
    passport = require('passport'),
    paginate = require('express-paginate'),
    express = require('express'),
    router = express.Router();


router
    .post('/reply/:id', requireAuth, commentController.createComment)
    .get('/', requireAuth, paginate.middleware(5, 50), commentController.getCommentList)
    .get('/:id', requireAuth, commentController.getCommentById)
    .patch('/:id', requireAuth, userAuth.sameUser(require('../models/comment')), commentController.updateComment)
    .delete(':id', requireAuth, userAuth.sameUser(require('../models/comment')), commentController.deleteComment);


module.exports = router;
