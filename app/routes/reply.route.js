/**
 * Created by bdrosatos on 20/10/2016.
 */

"use strict";

const aws = require('aws-sdk');
aws.config.loadFromPath('app//config/aws-config.json');

const ReplyController = require('../controllers/reply.controller'),
    userAuth = require('../middlewares/sameUser'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    passport = require('passport'),
    paginate = require('express-paginate'),
    express = require('express'),
    router = express.Router(),
    multer = require('multer'),
    multerS3 = require('multer-s3'),
    s3 = new aws.S3({signatureVersion: 'v4'}),
    upload = multer({
        storage: multerS3({
            s3: s3,
            bucket: 'uni-exams-uploads',
            acl: 'public-read',
            metadata: function (req, file, cb) {
                cb(null, {fieldName: file.fieldname});
            },
            key: function (req, file, cb) {
                cb(null, `courses/${req.body.courseId}/replies/${req.body.courseYear}/${req.body.courseExamType}/topics/${req.params.id}/${file.originalname}`);
            }
        }),
        limits: {fileSize: 3e+6}
    });


router.post('/exam/:id', requireAuth, upload.fields([
    {
        name: 'files',
        maxCount: 1
    }]), ReplyController.createReply);

router.get('/', requireAuth, paginate.middleware(5, 50), ReplyController.getReplyList)
    .get('/:id', requireAuth, ReplyController.getReplyId)
    .patch('/add/upvote/:id', requireAuth, ReplyController.addUpvote)
    .patch('/remove/upvote/:id', requireAuth, ReplyController.removeUpvote)
    .patch('/mark/approve/:id', requireAuth, userAuth.sameUser(require('../models/reply')), ReplyController.markApprove)
    .patch('/unmark/approve/:id', requireAuth, userAuth.sameUser(require('../models/reply')), ReplyController.unMarkApprove)
    .delete('/:id', requireAuth, userAuth.sameUser(require('../models/reply')), ReplyController.deleteReply);


module.exports = router;
