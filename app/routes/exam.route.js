/**
 * Created by bdrosatos on 9/10/2016.
 */
"use strict";

const aws = require('aws-sdk');
aws.config.loadFromPath('app//config/aws-config.json');

const ExamController = require('../controllers/exam.controller'),
    userAuth = require('../middlewares/sameUser'),
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    passport = require('passport'),
    paginate = require('express-paginate'),
    express = require('express'),
    router = express.Router(),
    multer = require('multer'),
    multerS3 = require('multer-s3'),
    s3 = new aws.S3({signatureVersion: 'v4'}),
    upload = multer({
        storage: multerS3({
            s3: s3,
            bucket: 'uni-exams-uploads',
            acl: 'public-read',
            metadata: function (req, file, cb) {
                cb(null, {fieldName: file.fieldname.replace(/ /g,"")});
            },
            key: function (req, file, cb) {
                cb(null, `courses/${req.params.id}/exams/${req.body.year}/${req.body.examType}/${file.originalname.replace(/ /g,"")}`);
            }
        }),
        limits: {fileSize: 3e+6}
    });


router.post('/course/:id', requireAuth, upload.fields([
    {
        name: 'files',
        maxCount: 4
    }, {
        name: 'files',
        maxCount: 6
    }]), ExamController.createExam);

router.get('/', requireAuth, paginate.middleware(10, 50), ExamController.getExamList)
    .get('/:id', requireAuth, ExamController.getExamById)
    .delete('/:id', requireAuth, userAuth.sameUser(require('../models/exam')), ExamController.deleteExam);


module.exports = router;
