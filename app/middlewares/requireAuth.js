/**
 * Created by bdrosatos on 25/2/2017.
 */

const passport = require('passport'),
    i18n = require("i18n");


exports.requireAuth = (req, res, next) => {
    passport.authenticate('jwt', {session: false}, function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.status(401).json({
                title: 'WORD_UNAUTHORIZED',
                error: {message: 'INVALID_TOKEN'}
            });
        }

        req.user = user;   // Forward user information to the next middleware
        next();
    })(req, res, next);
};