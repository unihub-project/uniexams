/**
 * Created by stavr on 10/12/2016.
 */

"use strict";

const i18n = require('i18n'),
    Profile = require('../models/profile'),
    User = require('../models/user');

// Role authorization check
exports.sameUser = (Type) => {
    return (req, res, next) => {

        if (req.user.role == 'Admin') return next();

        if (Type === User) { // auth.controller
            if (req.user._id.equals(req.params.id)) {
                return next();
            } else {
                return res.status(401).json({
                    title: i18n.__('WORD_UNAUTHORIZED'),
                    error: {message: i18n.__('NOT_AUTHORIZED')}
                });
            }
        }

        if (Type === Profile) { // profile.controller

            Profile.findById(req.params.id, (err, profile) => {

                if(err) return next(err);

                if(!profile) return next();

                if (profile.user.equals(req.user._id)) {
                    return next();
                } else {
                    return res.status(401).json({
                        title: i18n.__('WORD_UNAUTHORIZED'),
                        error: {message: i18n.__('NOT_AUTHORIZED')}
                    });
                }
            });
        }else{

            Profile.findOne({user: req.user._id}, (err, CurrentProfile) => {
                if (err) return next(err);

                Type.findOne({profile: CurrentProfile._id}, (err, CurrentType)=> { // other controllers
                    if (err) return next(err);

                    if (CurrentType) {
                        return next();
                    }

                    return res.status(401).json({
                        title: i18n.__('WORD_UNAUTHORIZED'),
                        error: {message: i18n.__('NOT_AUTHORIZED')}
                    });
                })
            });
        }

    }
};