const webpack = require('webpack');

module.exports = {
    entry: {
        'vendor': './assets/app/vendor.ts',
        'app': './assets/app/main.ts'
    },
    stats: {
        warnings: false
    },

    resolve: {
        extensions: ['', '.js', '.ts']
    },

    node: {
        fs: "empty"
    },

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loaders: [
                    'awesome-typescript-loader',
                    'angular2-template-loader',
                    'angular2-router-loader'
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [
                    /node_modules(\\|\/)@angular/
                ],
                exclude: [
                    /\.umd\.js$/
                ],
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.json$/,
                loader: "json"
            },
            {
                test: /\.html$/,
                loader: 'html'
            }, {
                test: /\.(pug|jade)$/,
                loaders: ['raw-loader', 'pug-html-loader']
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.(sass|scss)$/,
                exclude: /node_modules/,
                loaders: ['raw-loader', 'sass-loader'] // sass-loader not scss-loader
            }
        ]
    },

    plugins: [
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            './assets/app' // location of your src
        ),
        new webpack.optimize.CommonsChunkPlugin({name: 'vendor', filename: 'vendor.bundle.js'})


    ]
};