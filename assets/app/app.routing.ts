import { Routes, RouterModule } from "@angular/router";

import { AuthenticationComponent } from "./auth/authentication.component";
import {HomeComponent} from "./home.component";
import {IsLoggedInAuthGuard} from "./auth/shared/isLoggedIn.guard";
import {IsAdminAuthGuard} from "./admin/shared/isAdmin.guard";
import {AdminComponent} from "./admin/admin.component";
import {PageNotFoundComponent} from "./page.not.found.component";
import {MyDepartmentComponent} from "./undergraduate/myDepartment/myDepartment.component";
import {CourseComponent} from "./undergraduate/course/course.component";
import {LandingPage} from "./landing/landing.page";

const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'welcome', pathMatch: 'full' },
    { path: 'welcome', component: LandingPage, loadChildren: './landing/landing.module#LandingModule' },
    { path: 'home', component: HomeComponent},
    { path: 'my-department', component: MyDepartmentComponent},
    { path: 'course', component: CourseComponent, loadChildren: './undergraduate/course/course.module#CourseModule' },
    { path: 'auth', component: AuthenticationComponent, loadChildren: './auth/auth.module#AuthModule' },
    { path: 'admin', component: AdminComponent, loadChildren: './admin/admin.module#AdminModule', canActivate: [IsLoggedInAuthGuard, IsAdminAuthGuard] },
    { path: '**', component: PageNotFoundComponent},
];

export const routing = RouterModule.forRoot(APP_ROUTES);