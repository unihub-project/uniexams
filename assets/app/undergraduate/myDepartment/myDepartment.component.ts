/**
 * Created by stavr on 12/13/2016.
 */
import {Component} from "@angular/core";
import {DepartmentService} from "../../shared/services/department.service";
import {UniversityService} from "../../shared/services/university.service";
import {Department} from "../../shared/models/departent.model";
import {University} from "../../shared/models/university.model";
import {CourseService} from "../../shared/services/course.service";
import {Course} from "../../shared/models/course.model";
import {FormGroup, FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {ToastService} from "../../shared/toasts/shared/toast.service";
import {TranslateService} from "ng2-translate";


@Component({
    selector: 'app-mydepartment',
    templateUrl: 'myDepartment.component.pug'
})


export class MyDepartmentComponent {
    constructor(private translateService: TranslateService,
                private toastService: ToastService,
                private departmentService: DepartmentService,
                private universityService: UniversityService,
                private courseService: CourseService,
                private router: Router) {
    }

    department = new Department("", "");
    university = new University("", "");
    courses = [];

    searchForm: FormGroup;
    searchCriteria: Course = new Course();

    noDefaultDepartment: boolean = false;

    totalPages: number = 0;
    total: number = 0;
    page: number = 1;
    infoLoading: boolean;
    courseLoading: boolean;

    ngOnInit() {
        this.infoLoading = true;
        this.courseLoading = true;

        this.searchForm = new FormGroup({
            name: new FormControl(null)
        });

        this.departmentService.getMyDepartment()
            .subscribe((myDepartment) => {

                    this.department = new Department(
                        myDepartment['obj']['name'],
                        myDepartment['obj']['shortName'],
                        myDepartment['obj']['information'],
                        myDepartment['obj']['_id'],
                        myDepartment['obj']['university'],
                        myDepartment['obj']['courses']
                    );


                    this.universityService.getById(myDepartment['obj']['university'])
                        .subscribe((myUniversity)=> {
                            this.university = new University(
                                myUniversity['obj']['name'],
                                myUniversity['obj']['shortName'],
                                myUniversity['obj']['information'],
                                myUniversity['obj']['_id'],
                                myUniversity['obj']['departments']
                            );
                            this.infoLoading = false;
                        }, (err)=> console.error(err));

                    this.getList();
                },
                (error)=> {
                    this.noDefaultDepartment = true;
                });

    }


    onSearchSubmit() {
        this.searchCriteria.name = this.searchForm.value.name;
        this.getList()
    }

    getNewPage(page: number) {
        this.page = page;
        this.getList();
    }

    getList() {
        this.courseLoading = true;
        this.courses = [];
        this.searchCriteria.department = new Department();
        this.searchCriteria.department.id = this.department['id'];
        this.courseService.getList(this.page, this.searchCriteria)
            .subscribe((courses)=> {

                this.totalPages = courses['total_pages'];
                this.total = courses["total"];

                courses['obj'].forEach((course: Course) => {

                    this.courses.push(new Course(
                        course['name'],
                        course['courseType'],
                        course['information'],
                        course['_id'],
                        course['exams'],
                        course['department'],
                        course['exams'].length,
                        course['isFollowed']
                    ))
                });
                this.courseLoading = false;
            })
    }


    onSelectCourse(id: String) {
        this.router.navigate(['/course', id]);
    }

    onEditProfileClick() {
        this.router.navigate(['/auth/profile/edit']);
    }

    setFollow( course: Course){
        this.courseService.setFollow(course.id)
            .subscribe(
                data => {
                    this.translateService.get("COURSE.FOLLOW.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            course.isFollow = true;
                        })

                },
                error => {
                    console.error(error);
                }
            )

    }

    removeFollow( course: Course){
        this.courseService.removeFollow(course.id)
            .subscribe(
                data => {
                    this.translateService.get("COURSE.UNFOLLOW.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            course.isFollow = false;
                        })

                },
                error => {
                    console.error(error);
                }
            )
    }
}