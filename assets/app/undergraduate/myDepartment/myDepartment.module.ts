import {NgModule} from '@angular/core';
import {SharedModule} from "../../shared/shared.module";
import {MyDepartmentComponent} from "./myDepartment.component";



@NgModule({
    //components
    declarations: [
        MyDepartmentComponent
    ],
    //modules
    imports: [
        SharedModule
    ],

    // services
    providers: [

    ]

})
export class MyDepartmentModule {

}/**
 * Created by stavr on 12/13/2016.
 */
