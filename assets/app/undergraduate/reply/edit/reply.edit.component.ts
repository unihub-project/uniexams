/**
 * Created by bdrosatos on 27/2/2017.
 */

import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {ReplyService} from "../../../shared/services/reply.service";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {Reply} from "../../../shared/models/reply.model";
import {FormGroup, FormControl} from "@angular/forms";

@Component({
    selector: 'app-reply-edit',
    templateUrl: 'reply.edit.component.pug',
    styleUrls: ['../reply.component.sass']
})


export class ReplyEditComponent implements OnInit {

    @Input('reply') reply: Reply;
    @Output('cancel') cancelEvent : EventEmitter<boolean> = new EventEmitter<boolean>();

    editReplyForm: FormGroup;


    constructor(private replyService: ReplyService,
                private toastService: ToastService) {
    }

    ngOnInit(): void {
        this.editReplyForm = new FormGroup({
            textContent: new FormControl(this.reply.textContent),
        });
    }


    onCancel() {
        this.cancelEvent.next(false);
    }


}