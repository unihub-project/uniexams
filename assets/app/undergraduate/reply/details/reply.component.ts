/**
 * Created by stavros on 20/2/2017.
 */
import {Component, Input, OnInit, EventEmitter, Output} from "@angular/core";
import {ReplyService} from "../../../shared/services/reply.service";
import {Reply} from "../../../shared/models/reply.model";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {AuthService} from "../../../auth/shared/auth.service";
import moment = require("moment");
import Duration = moment.Duration;

@Component({
    selector: 'app-reply',
    templateUrl: 'reply.component.pug',
    styleUrls: ['../reply.component.sass']
})


export class ReplyComponent implements OnInit {

    @Input() reply: Reply;

    @Output("refreshList") refreshListEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    elapsedTimeCreated: any;
    alreadyVote: boolean = false;
    canEdit: boolean = false;

    editToggle: boolean = false;
    commentToggle: boolean = false;

    constructor(private replyService: ReplyService,
                public authService: AuthService,
                private toastService: ToastService) {
    }

    ngOnInit(): void {
        //Check if current user has already upvote this reply
        if (this.reply.upvoteUsers.indexOf(this.authService.getCurrentUserId()) > -1) {
            this.alreadyVote = true;
        }

        //Check if current user is author of reply or admin so he can edit/delete reply
        if (this.authService.isAdmin() || this.authService.getCurrentUserId() == this.reply.profile.user.id) {
            this.canEdit = true;
        }

        this.setElapsedTimeCreated()
    }

    onUpvote = (replyId: string) => {
        this.replyService.addUpvote(replyId)
            .subscribe((data) => {
                    this.reply.upvoteUsers = data["obj"]["upvoteUsers"];
                    this.reply.upvotes = data["obj"]["upvotes"];
                    this.alreadyVote = true;
                    this.toastService.handleSuccess({title: data.title, message: 'Upvote added'});

                },
                error => {
                    console.error(error);
                });
    };

    onDownvote = (replyId: string) => {
        this.replyService.removeUpvote(replyId)
            .subscribe((data) => {
                    this.reply.upvoteUsers = data["obj"]["upvoteUsers"];
                    this.reply.upvotes = data["obj"]["upvotes"];
                    this.alreadyVote = false;
                    this.toastService.handleSuccess({title: data.title, message: 'Upvote removed'});

                },
                error => {
                    console.error(error);
                });
    };

    onMarkApprove = (replyId: string) => {
        this.replyService.markApprove(replyId)
            .subscribe((data) => {
                    this.reply.isApproved = data["obj"]["isApproved"];
                    this.toastService.handleSuccess({title: data.title, message: 'Marked as approved'});
                    this.refreshListEvent.next(true);

                },
                error => {
                    console.error(error);
                });
    };

    onUnMarkApprove = (replyId: string) => {
        this.replyService.unMarkApprove(replyId)
            .subscribe((data) => {
                    this.reply.isApproved = data["obj"]["isApproved"]
                    this.toastService.handleSuccess({title: data.title, message: 'Unmarked approve'});
                    this.refreshListEvent.next(false);

                },
                error => {
                    console.error(error);
                });
    };

    onDelete = (replyId: string) => {
        this.replyService.deleteById(replyId)
            .subscribe((data) => {
                    this.toastService.handleSuccess({title: data.title, message: 'Reply deleted successfully'});
                    this.refreshListEvent.next(false);
                },
                error => {
                    console.error(error);
                }
            )
    };


    setElapsedTimeCreated(): void {
        let duration: Duration = moment.duration(moment().diff(this.reply.createdAt));

        if (duration.years() > 0) {
            this.elapsedTimeCreated = {years: duration.years()};
        } else if (duration.months() > 0) {
            this.elapsedTimeCreated = {months: duration.months()};
        } else if (duration.days() > 0) {
            this.elapsedTimeCreated = {days: duration.days()};
        } else if (duration.hours() > 0) {
            this.elapsedTimeCreated = {hours: duration.hours()};
        } else if (duration.minutes() > 0) {
            this.elapsedTimeCreated = {minutes: duration.minutes()};
        } else if (duration.seconds() > 0) {
            this.elapsedTimeCreated = {seconds: duration.seconds()};
        }
    }


}