/**
 * Created by stavros on 20/2/2017.
 */
import {Component, Input, SimpleChange, OnChanges, OnInit, EventEmitter, Output} from "@angular/core";
import {ReplyService} from "../../../shared/services/reply.service";
import {Reply} from "../../../shared/models/reply.model";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {FormGroup, FormControl} from "@angular/forms";
import {TranslateService} from "ng2-translate";
import {Exam} from "../../../shared/models/exam.model";

@Component({
    selector: 'app-reply-create',
    templateUrl: 'reply.create.component.pug',
    styleUrls: ['../reply.component.sass']
})


export class ReplyCreateComponent implements OnInit, OnChanges {

    @Input('topicNumber') topicNumber: number;
    @Input('examId') examId: string;
    @Output("refreshList") refreshListEvent: EventEmitter<boolean> = new EventEmitter<boolean>();


    reply: Reply = new Reply();
    createReplyForm: FormGroup;

    constructor(private replyService: ReplyService,
                private toastService: ToastService) {
    }

    ngOnInit(): void {
        this.createReplyForm = new FormGroup({
            textContent: new FormControl(null),
        });
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName === 'topicNumber') {
                console.log(this.topicNumber);
            }
        }
    }

    onSubmit() {
        this.reply.exam = new Exam();
        this.reply.exam.id = this.examId;
        this.reply.textContent = this.createReplyForm.value.textContent;
        this.reply.topicNumber = this.topicNumber;
        this.reply.fileType = "TXT";

        this.replyService.create(this.reply)
            .subscribe(
                data => {
                    this.toastService.handleSuccess({message: data['title']});
                    this.createReplyForm.reset();
                    this.refreshListEvent.next(false);

                },
                error => console.error((error))
            );
    }

}