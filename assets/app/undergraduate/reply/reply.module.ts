/**
 * Created by stavros on 19/2/2017.
 */


import {NgModule} from '@angular/core';
import {SharedModule} from "../../shared/shared.module";
import {ReplyListComponent} from "./list/reply.list.component";
import {ReplyComponent} from "./details/reply.component";
import {ReplyCreateComponent} from "./create/reply.create.component";
import {CKEditorModule} from "ng2-ckeditor";
import {ReplyEditComponent} from "./edit/reply.edit.component";
import {CommentModule} from "./comment/comment.module";


@NgModule({
    //components
    declarations: [
        ReplyComponent,
        ReplyListComponent,
        ReplyCreateComponent,
        ReplyEditComponent
    ],
    //modules
    imports: [
        SharedModule,
        CKEditorModule,
        CommentModule
    ],
    exports: [
        ReplyComponent,
        ReplyListComponent,
        ReplyCreateComponent
    ],
    // services
    providers: []

})
export class ReplyModule {
}

