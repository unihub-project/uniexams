/**
 * Created by stavros on 19/2/2017.
 */

import {Component, OnInit, Input} from "@angular/core";
import {TranslateService} from "ng2-translate";
import {ReplyService} from "../../../../shared/services/reply.service";
import {ToastService} from "../../../../shared/toasts/shared/toast.service";
import {CommentService} from "../../../../shared/services/comment.service";
import {Reply} from "../../../../shared/models/reply.model";
import {Comment} from "../../../../shared/models/comment.model";
import {User} from "../../../../shared/models/user.model";
import {Profile} from "../../../../shared/models/profile.model";

@Component({
    selector: 'app-comment-list',
    templateUrl: 'comment.list.component.pug',
    styleUrls: ['../../reply.component.sass']

})


export class CommentListComponent implements OnInit {


    @Input('reply') reply: Reply;

    page: number = 1;
    total: number = 0;
    comments: Comment[] = [];
    loading: boolean;


    constructor(private translateService: TranslateService,
                private replyService: ReplyService,
                private commentService: CommentService,
                private toastService: ToastService) {
    }

    ngOnInit(): void {
        this.getReplyComments()
    }


    getReplyComments() {

        let searchCriteria: Comment = new Comment();
        searchCriteria.reply = this.reply;

        this.commentService.getList(this.page, searchCriteria)
            .subscribe((data) => {
                    this.loading = false;
                    this.total = data["total"];
                    const commentsArray = data['obj'];
                    console.log(commentsArray);
                    this.comments = [];
                    for (let i = 0; i < commentsArray.length; i++) {
                        const commentProfile = new Profile(
                            commentsArray[i]['profile']['firstName'],
                            null, //default department
                            commentsArray[i]['profile']['lastName'],
                            commentsArray[i]['profile']['_id'],
                            null,
                            new User(null, null, commentsArray[i]['profile']['user'])
                        );
                        const newComment = new Comment(
                            commentsArray[i]['_id'],
                            commentsArray[i]['content'],
                            this.reply,
                            commentProfile,
                            commentsArray[i]['createdAt']
                        );
                        this.comments.push(newComment);
                    }
                },
                error => {
                    console.error(error);
                }
            );
    }


}