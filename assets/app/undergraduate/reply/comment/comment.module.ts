/**
 * Created by bdrosatos on 27/2/2017.
 */
import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {CKEditorModule} from "ng2-ckeditor";
import {CommentListComponent} from "./list/comment.list.component";
import {CommentComponent} from "./details/comment.component";
import {CommentCreateComponent} from "./create/comment.create.component";


@NgModule({
    //components
    declarations: [
        CommentCreateComponent,
        CommentListComponent,
        CommentComponent
    ],
    //modules
    imports: [
        SharedModule,
        CKEditorModule
    ],
    exports: [
        CommentCreateComponent,
        CommentListComponent,
        CommentComponent
    ],
    // services
    providers: []

})
export class CommentModule {
}

