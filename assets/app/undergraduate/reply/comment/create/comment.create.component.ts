/**
 * Created by stavros on 20/2/2017.
 */
import {Component, Input, OnInit, EventEmitter, Output} from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";
import {TranslateService} from "ng2-translate";
import {CommentService} from "../../../../shared/services/comment.service";
import {Reply} from "../../../../shared/models/reply.model";
import {Comment} from "../../../../shared/models/comment.model";
import {ReplyService} from "../../../../shared/services/reply.service";
import {ToastService} from "../../../../shared/toasts/shared/toast.service";

@Component({
    selector: 'app-comment-create',
    templateUrl: 'comment.create.component.pug',
    // styleUrls: ['../comment.component.sass']
})


export class CommentCreateComponent implements OnInit {

    @Input('reply') reply: Reply;
    @Output("cancel") cancelEvent: EventEmitter<boolean> = new EventEmitter<boolean>();


    comment: Comment = new Comment();
    createCommentForm: FormGroup;

    constructor(private replyService: ReplyService,
                private commentService: CommentService,
                private toastService: ToastService) {
    }

    ngOnInit(): void {
        this.createCommentForm = new FormGroup({
            textContent: new FormControl(''),
        });
    }

    onSubmit() {
        console.log(this.reply)
        this.comment.reply = this.reply;
        this.comment.content = this.createCommentForm.value.textContent;
        this.commentService.create(this.comment)
            .subscribe(
                data => {
                    this.toastService.handleSuccess({message: data['title']});
                    this.createCommentForm.reset();
                },
                error => console.error((error))
            );
    }

}