/**
 * Created by stavros on 20/2/2017.
 */
import {Component, OnInit, Input} from "@angular/core";
import {ToastService} from "../../../../shared/toasts/shared/toast.service";
import {ReplyService} from "../../../../shared/services/reply.service";
import {AuthService} from "../../../../auth/shared/auth.service";
import {Comment} from "../../../../shared/models/comment.model";


@Component({
    selector: 'app-comment',
    templateUrl: 'comment.component.pug',
    styleUrls: ['../../reply.component.sass']
})


export class CommentComponent implements OnInit {


    @Input('comment') comment: Comment;

    constructor(private replyService: ReplyService,
                public authService: AuthService,
                private toastService: ToastService) {
    }

    ngOnInit(): void {

    }

}