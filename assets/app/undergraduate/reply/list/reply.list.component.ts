/**
 * Created by stavros on 19/2/2017.
 */

import {Component, Input, OnInit, Output, EventEmitter} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {ReplyService} from "../../../shared/services/reply.service";
import {Reply} from "../../../shared/models/reply.model";
import {Exam} from "../../../shared/models/exam.model";
import {Profile} from "../../../shared/models/profile.model";
import {User} from "../../../shared/models/user.model";


@Component({
    selector: 'app-reply-list',
    templateUrl: 'reply.list.component.pug',
    styleUrls: ['../reply.component.sass']

})


export class ReplyListComponent implements OnInit {


    constructor(private router: Router,
                private translateService: TranslateService,
                private replyService: ReplyService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute) {
    }

    @Input('numberOfTopics') numberOfTopics: Number;

    @Output("refresh") refreshEvent: EventEmitter<boolean> = new EventEmitter<boolean>();


    topicsArray: Number[] = [];
    examId: string;
    currentTopic: number;
    totalPages: number = 0;
    total: number = 0;
    page: number = 1;
    currentReplies: Reply[] = [];
    loading: boolean;

    ngOnInit(): void {
        this.loading = true;
        this.topicsArray = [];
        for (let i = 1; i <= this.numberOfTopics; i++) {
            this.topicsArray.push(i);
        }
        this.currentTopic = 1;
        this.examId = this.activatedRoute.parent.firstChild.firstChild.params['_value']['id'];
        this.getTopicReplies();

    }

    onChangeTopic(topicNumber) {
        this.currentTopic = topicNumber;
        this.page = 1;
        this.getTopicReplies();
    }

    getNewPage(page: number) {
        this.page = page;
        this.getTopicReplies();
    }


    getTopicReplies() {
        this.currentReplies = [];

        this.loading = true;
        let searchCriteria: Reply = new Reply();
        let exam: Exam = new Exam();
        exam.id = this.examId;
        searchCriteria.exam = exam;
        searchCriteria.topicNumber = this.currentTopic;
        //
        this.replyService.getList(this.page, searchCriteria).subscribe(
            data => {
                this.loading = false;
                this.totalPages = data["total_pages"];
                this.total = data["total"];
                const repliesArray = data['obj'];
                this.currentReplies = [];
                for (let i = 0; i < repliesArray.length; i++) {
                    const exam = new Exam(
                        repliesArray[i]['exam']
                    );
                    const replyProfile = new Profile(
                        repliesArray[i]['profile']['firstName'],
                        null, //default department
                        repliesArray[i]['profile']['lastName'],
                        repliesArray[i]['profile']['_id'],
                        null,
                        new User(null, null, repliesArray[i]['profile']['user'])
                    );
                    exam.profile = new Profile(
                        repliesArray[i]['exam']['profile']['firstName'],
                        null, //default department
                        repliesArray[i]['exam']['profile']['lastName'],
                        repliesArray[i]['exam']['profile']['_id'],
                        null,
                        new User(null, null, repliesArray[i]['exam']['profile']['user'])
                    );
                    const newReply = new Reply(
                        repliesArray[i]['_id'],
                        exam, //exam
                        repliesArray[i]['topicNumber'],
                        replyProfile,//profile,
                        repliesArray[i]['upvotes'],
                        repliesArray[i]['upvoteUsers'],
                        repliesArray[i]['isApproved'],
                        repliesArray[i]['fileType'],
                        repliesArray[i]['filePath'],
                        null, //files
                        repliesArray[i]['textContent'],
                        repliesArray[i]['createdAt']);
                    this.currentReplies.push(newReply);
                }
            },
            error => {
                console.error(error);
            }
        )
    }

    refreshList(firstPage: boolean) {
        this.getNewPage(firstPage ? 1 : this.page);

    }

}