/**
 * Created by stavr on 30-Jan-17.
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {ExamCreateComponent} from "./create/exam.create.component";
import {examRouting} from "./exam.routing";
import {ExamPreviewComponent} from "./preview/exam.preview.component";
import {CKEditorModule} from "ng2-ckeditor";
import {ReplyModule} from "../reply/reply.module";


@NgModule({
    //components
    declarations: [
        ExamCreateComponent,
        ExamPreviewComponent
    ],
    //modules
    imports: [
        SharedModule,
        ReplyModule,
        CKEditorModule,

        examRouting
    ],

    // services
    providers: []

})
export class ExamModule {

}