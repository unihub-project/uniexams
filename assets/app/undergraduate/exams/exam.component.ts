
/**
 * Created by bdrosatos on 27/1/2017.
 */
import {Component} from "@angular/core";


@Component({
    selector: 'app-exam',
    template: `<router-outlet></router-outlet>`
})


export class ExamComponent{
    constructor() {
    }


}
