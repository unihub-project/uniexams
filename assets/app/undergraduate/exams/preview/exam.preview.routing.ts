/**
 * Created by bdrosatos on 19/2/2017.
 */

import {RouterModule, Routes} from "@angular/router";
import {ExamPreviewComponent} from "./exam.preview.component";
import {ExamPreviewTxtComponent} from "./txt/exam.preview.txt.component";
import {ExamPreviewImgComponent} from "./img/exam.preview.img.component";
import {ExamPreviewPdfComponent} from "./pdf/exam.preview.pdf.component";

const EXAM_PREVIEW_ROUTES: Routes = [
    {path: 'img/:id', component: ExamPreviewImgComponent},
    {path: 'txt/:id', component: ExamPreviewTxtComponent},
    {path: 'pdf/:id', component: ExamPreviewPdfComponent},
];

export const examPreviewRouting = RouterModule.forChild(EXAM_PREVIEW_ROUTES);