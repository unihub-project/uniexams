/**
 * Created by stavr on 30-Jan-17.
 */

import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Router, ActivatedRoute, Params} from "@angular/router";

import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../../shared/toasts/shared/toast.service";
import {ExamService} from "../../../../shared/services/exam.service";

@Component({
    selector: 'app-exam-preview-txt',
    templateUrl: 'exam.preview.txt.component.pug'
})


export class ExamPreviewTxtComponent {

    htmlSting: string = '<h1>Loading Exam..</h1>';
    examId: string;

    constructor(private router: Router,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private examService: ExamService) {
    }

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            this.examId = params['id'];
            this.examService.getById(this.examId).subscribe(
                data => {
                    this.htmlSting = data['obj']['textContent']
                },
                error => {
                    console.error(error);
                }
            )
        })
    }


}