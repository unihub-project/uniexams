/**
 * Created by stavr on 30-Jan-17.
 */

import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Router, ActivatedRoute, Params} from "@angular/router";

import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../../shared/toasts/shared/toast.service";
import {ExamService} from "../../../../shared/services/exam.service";

@Component({
    selector: 'app-exam-preview-img',
    templateUrl: 'exam.preview.img.component.pug'
})


export class ExamPreviewImgComponent {

    imgURLs: string[];
    examId: string;

    constructor(private router: Router,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private examService: ExamService) {
    }

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            this.examId = params['id'];
            this.examService.getById(this.examId).subscribe(
                data => {
                    console.log(data);
                    // this.htmlSting = data['obj']['textContent']
                    this.imgURLs = data['obj']['filePath'];
                },
                error => {
                    console.error(error);
                }
            )
        })

    }


}