/**
 * Created by stavr on 30-Jan-17.
 */

import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute, Params} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {Reply} from "../../../shared/models/reply.model";
import {Exam} from "../../../shared/models/exam.model";
import {ReplyService} from "../../../shared/services/reply.service";
import {ExamService} from "../../../shared/services/exam.service";


@Component({
    selector: 'app-exam-preview',
    templateUrl: 'exam.preview.component.pug'
})


export class ExamPreviewComponent implements OnInit {

    searchCriteria: Reply = new Reply();
    pageNumber: number = 1;
    examId: string;
    exam: Exam = new Exam();
    numberOfTopics: number;

    constructor(private router: Router,
                private translateService: TranslateService,
                private replyService: ReplyService,
                private examService: ExamService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        // this.exam.numberOfTopics = 1;
        this.examId = this.activatedRoute.firstChild.params['_value']['id'];


        this.examService.getById(this.examId)
            .subscribe(
                data => {
                    this.exam = new Exam(
                        data['obj']['_id'],
                        data['obj']['year'],
                        data['obj']['examType'],
                        data['obj']['fileType'],
                        data['obj']['filePath'],
                        null, //files,
                        data['obj']['textContent'],
                        data['obj']['information'],
                        data['obj']['numberOfTopics'],
                        null,
                        null
                    );
                    this.numberOfTopics = this.exam.numberOfTopics;
                    console.log(this.exam);
                },
                error => {
                    console.error(error);
                }
            )
    }

}