/**
 * Created by stavr on 30-Jan-17.
 */

import {NgModule} from '@angular/core';
import {SharedModule} from "../../../shared/shared.module";
import {ExamPreviewTxtComponent} from "./txt/exam.preview.txt.component";
import {examPreviewRouting} from "./exam.preview.routing";
import {ExamPreviewImgComponent} from "./img/exam.preview.img.component";
import {PdfViewerComponent} from "ng2-pdf-viewer";
import {ExamPreviewPdfComponent} from "./pdf/exam.preview.pdf.component";
import {ReplyModule} from "../../reply/reply.module";

@NgModule({
    //components
    declarations: [
        ExamPreviewTxtComponent,
        ExamPreviewImgComponent,
        ExamPreviewPdfComponent,
        PdfViewerComponent
    ],
    //modules
    imports: [
        SharedModule,
        examPreviewRouting
    ],

    // services
    providers: []

})
export class ExamPreviewModule {

}