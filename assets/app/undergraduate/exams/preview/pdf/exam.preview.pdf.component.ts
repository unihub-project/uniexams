/**
 * Created by stavr on 30-Jan-17.
 */

import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Router, ActivatedRoute, Params} from "@angular/router";

import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../../shared/toasts/shared/toast.service";
import {ExamService} from "../../../../shared/services/exam.service";

@Component({
    selector: 'app-exam-preview-pdf',
    templateUrl: 'exam.preview.pdf.component.pug'
})


export class ExamPreviewPdfComponent {

    pdfURL: string;
    examId: string;
    totalPages: number;
    currentPage: number = 1;
    loading: boolean;
    constructor(private router: Router,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private examService: ExamService) {
    }

    ngOnInit() {
        this.loading = true;
        this.activatedRoute.params.forEach((params: Params) => {
            this.examId = params['id'];
            this.examService.getById(this.examId).subscribe(
                data => {
                    console.log(data);
                    // this.htmlSting = data['obj']['textContent']
                    this.pdfURL = data['obj']['filePath'][0];
                },
                error => {
                    console.error(error);
                }
            )
        })

    }

    afterLoadPdf(pdf) { //PDFDocumentProxy
        this.totalPages = parseInt(pdf['pdfInfo']['numPages']);
        this.loading = false;
    }

    nextPage() {
        if(this.currentPage < this.totalPages) {
            this.currentPage++;
        }
    }

    previousPage() {
        if(this.currentPage > 1) {
            this.currentPage--;
        }
    }

}