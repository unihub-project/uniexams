/**
 * Created by stavr on 30-Jan-17.
 */

import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Router, ActivatedRoute, Params} from "@angular/router";

import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {CourseService} from "../../../shared/services/course.service";
import {ExamService} from "../../../shared/services/exam.service";
import {Course} from "../../../shared/models/course.model";
import {Exam} from "../../../shared/models/exam.model";


@Component({
    selector: 'app-exam-create',
    templateUrl: 'exam.create.component.pug'
})


export class ExamCreateComponent {
    constructor(private router: Router,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private courseService: CourseService,
                private examService: ExamService) {
    }

    isSelectedExamType: boolean = false;
    selectedExamType: string;
    selectedFileType: string;

    isText: boolean = false;
    isFile: boolean = false;

    course: Course = new Course();

    exam: Exam = new Exam();

    courseId: string = null;

    createExamForm: FormGroup;

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            this.courseId = params['id'];
            this.courseService.getById(this.courseId)
                .subscribe((data) => {
                        this.course.id = this.courseId;
                        this.course.name = data['obj']['name'];
                    },
                    (error) => {
                        console.log(error);
                    });

        });

        this.createExamForm = new FormGroup({
            year: new FormControl(null, Validators.required),
            examType: new FormControl(null),
            fileType: new FormControl(null),
            information: new FormControl(null),
            textContent: new FormControl(null),
            topicNumber: new FormControl(null ,Validators.required)
        });
    }

    fileChange(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.exam.files = fileList;
        }
    }

    onSubmit() {
        this.exam.year = this.createExamForm.value.year;
        this.exam.examType = this.selectedExamType;
        this.exam.fileType = this.selectedFileType;
        this.exam.textContent = this.createExamForm.value.textContent;
        this.exam.information = this.createExamForm.value.information;
        this.exam.course = this.course;
        this.exam.numberOfTopics = this.createExamForm.value.topicNumber;

        this.examService.create(this.exam)
            .subscribe(
                data => {

                    this.translateService.get("EXAM.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            this.createExamForm.reset();
                        })
                },
                error => console.error((error))
            );
    }

    selectExamType(examType) {
        this.selectedExamType = examType["id"];
        this.isSelectedExamType = true;
    }

    selectFileType(fileType) {
        this.selectedFileType = fileType["id"];
        if (this.selectedFileType == 'TXT') {
            this.isText = true;
            this.isFile = false;
        } else if (this.selectedFileType == 'IMG' || this.selectedFileType == 'PDF') {
            this.isFile = true;
            this.isText = false;
        } else {
            this.isFile = false;
            this.isText = false;
        }
    }

    newTopic(){

    }

    fileTypeList: Object = [
        {
            id: 'TXT',
            text: 'EXAM.FILETYPE.TEXT'
        },
        {
            id: 'IMG',
            text: 'EXAM.FILETYPE.IMAGE'
        },
        {
            id: 'PDF',
            text: 'EXAM.FILETYPE.PDF'
        }
    ];

    examTypesList: Object = [
        {
            id: 'WINTER',
            text: 'EXAM.EXAMTYPE.WINTER'
        },
        {
            id: 'SPRING',
            text: 'EXAM.EXAMTYPE.SPRING'
        },
        {
            id: 'SEPTEMBER',
            text: 'EXAM.EXAMTYPE.SEPTEMBER'
        },
        {
            id: 'EXTRA',
            text: 'EXAM.EXAMTYPE.EXTRA'
        }
    ]
}