/**
 * Created by bdrosatos on 19/2/2017.
 */

import {Routes, RouterModule} from "@angular/router";
import {ExamCreateComponent} from "./create/exam.create.component";
import {ExamPreviewComponent} from "./preview/exam.preview.component";

const EXAMS_ROUTES: Routes = [

    {path: 'create/:id', component: ExamCreateComponent},
    {path: 'preview', component: ExamPreviewComponent, loadChildren: './preview/exam.preview.module#ExamPreviewModule'},


];

export const examRouting = RouterModule.forChild(EXAMS_ROUTES);

