
/**
 * Created by bdrosatos on 27/1/2017.
 */
import {Component} from "@angular/core";


@Component({
    selector: 'app-course',
    template: `<router-outlet></router-outlet>`
})


export class CourseComponent{
    constructor() {
    }


}
