import {NgModule} from '@angular/core';
import {SharedModule} from "../../shared/shared.module";
import {CourseDetailsComponent} from "./course.details.component";
import {courseRouting} from "./course.routing";
import {ExamComponent} from "../exams/exam.component";



@NgModule({
    //components
    declarations: [
        CourseDetailsComponent,
        ExamComponent
    ],
    //modules
    imports: [
        SharedModule,
        courseRouting
    ],

    // services
    providers: [

    ]

})
export class CourseModule {

}/**
 * Created by stavr on 12/13/2016.
 */
