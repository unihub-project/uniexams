/**
 * Created by bdrosatos on 27/1/2017.
 */
import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute, Params} from "@angular/router";
import {CourseService} from "../../shared/services/course.service";
import {ExamService} from "../../shared/services/exam.service";
import {Course} from "../../shared/models/course.model";
import {Exam} from "../../shared/models/exam.model";
import {ToastService} from "../../shared/toasts/shared/toast.service";
import {TranslateService} from "ng2-translate";


@Component({
    selector: 'app-course-details',
    templateUrl: 'course.details.component.pug'
})


export class CourseDetailsComponent implements OnInit {
    constructor(private courseService: CourseService,
                private examService: ExamService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private toastService: ToastService,
                private translateService: TranslateService,) {
    }

    courseId: string;
    course: Course = new Course();
    searchCriteria: Exam = new Exam(null);
    exams: Exam[] = [];
    loading: boolean = false;

    totalPages: number = 0;
    total: number = 0;
    page: number = 1;

    ngOnInit() {
        this.loading = true;
        this.activatedRoute.params.forEach((params: Params) => {
            this.courseId = params['id'];
            this.courseService.getById(this.courseId)
                .subscribe((data) => {
                        this.course.name = data['obj']['name'];
                        this.course.isFollow = data['obj']['isFollowed'];
                        this.course.id = this.courseId;
                    },
                    (error) => {
                        console.log(error);
                    });

            this.getExamList();


        });


    }

    getExamList() {
        this.searchCriteria.course = new Course();
        this.searchCriteria.course.id = this.courseId;
        this.examService.getList(this.page, this.searchCriteria)
            .subscribe((data) => {
                    this.loading = false;
                    const exams = data['obj'];
                    exams.forEach((exam)=> {
                        this.exams.push(new Exam(
                            exam['_id'],
                            exam['year'],
                            exam['examType'],
                            exam['fileType'], // fileType
                            null, //filepath,
                            null, //files
                            null, //textContent
                            exam['information'],
                            null, //course
                            null // profile
                            )
                        );
                    })
                }, (error) => {
                    console.log(error);
                }
            )
    }

    onSelectCourse(exam: Exam) {
        switch (exam.fileType) {
            case 'PDF':
                this.router.navigate(['course/exam/preview/pdf', exam.id]);
                break;
            case 'TXT':
                this.router.navigate(['course/exam/preview/txt', exam.id]);
                break;
            case 'IMG':
                this.router.navigate(['course/exam/preview/img', exam.id]);
                break;
            default:
                this.toastService.handleError({
                    title: 'Something bad happened',
                    error: {
                        message: 'Exam type is not valid.'
                    }
                });

        }
    }

    newExam() {
        this.router.navigate(['/course/exam/create/', this.courseId]);
    }

    setFollow( course: Course){
        this.courseService.setFollow(course.id)
            .subscribe(
                data => {
                    this.translateService.get("COURSE.FOLLOW.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            course.isFollow = true;
                        })

                },
                error => {
                    console.error(error);
                }
            )

    }

    removeFollow( course: Course){
        this.courseService.removeFollow(course.id)
            .subscribe(
                data => {
                    this.translateService.get("COURSE.UNFOLLOW.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            course.isFollow = false;
                        })

                },
                error => {
                    console.error(error);
                }
            )
    }
}
