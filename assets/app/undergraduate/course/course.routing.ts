/**
 * Created by bdrosatos on 19/2/2017.
 */

import {Routes, RouterModule} from "@angular/router";
import {CourseDetailsComponent} from "./course.details.component";
import {ExamComponent} from "../exams/exam.component";

const COURSE_ROUTES: Routes = [

    {path: ':id', component: CourseDetailsComponent},
    {path: 'exam', component: ExamComponent, loadChildren: '../exams/exam.module#ExamModule'},


];

export const courseRouting = RouterModule.forChild(COURSE_ROUTES);

