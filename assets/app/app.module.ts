import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule, Http} from "@angular/http";
import {LocationStrategy, HashLocationStrategy, PathLocationStrategy} from "@angular/common";
import {AppComponent} from "./app.component";
import {AuthenticationComponent} from "./auth/authentication.component";
import {HeaderComponent} from "./header.component";
import {routing} from "./app.routing";
import {AuthService} from "./auth/shared/auth.service";
import {ToastComponent} from "./shared/toasts/toast.component";
import {ToastService} from "./shared/toasts/shared/toast.service";
import {HomeComponent} from "./home.component";
import {IsLoggedInAuthGuard} from "./auth/shared/isLoggedIn.guard";
import {AdminComponent} from "./admin/admin.component";
import {IsAdminAuthGuard} from "./admin/shared/isAdmin.guard";
import {PageNotFoundComponent} from "./page.not.found.component";
import {UniversityService} from "./shared/services/university.service";
import {LanguageService} from "./language.service";
import {SharedModule} from "./shared/shared.module";
import {TranslateModule, TranslateLoader, TranslateStaticLoader} from "ng2-translate";
import {ToastModule, ToastOptions} from "ng2-toastr";
import {DepartmentService} from "./shared/services/department.service";
import {ConfirmOptions, Position} from "angular2-bootstrap-confirm";
import {Positioning} from "ng2-bootstrap/positioning";
import {MyDepartmentModule} from "./undergraduate/myDepartment/myDepartment.module";
import {CourseService} from "./shared/services/course.service";
import {UserService} from "./shared/services/users.service";
import {ExamService} from "./shared/services/exam.service";
import {ReplyService} from "./shared/services/reply.service";
import {CourseComponent} from "./undergraduate/course/course.component";

import "ng2-toastr/bundles/ng2-toastr.min.css";
import {LandingPage} from "./landing/landing.page";
import {CommentService} from "./shared/services/comment.service";
import {CustomOption} from "./shared/toasts/toastr-options";


export function httpFactory(http: Http) {
    return new TranslateStaticLoader(http, 'locales', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        AuthenticationComponent,
        LandingPage,
        HomeComponent,
        AdminComponent,
        CourseComponent,
        PageNotFoundComponent,
        ToastComponent],
    imports: [
        BrowserModule,
        HttpModule,
        ToastModule.forRoot(),
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: httpFactory,
            deps: [Http]
        }),
        SharedModule.forRoot(),
        routing,
        MyDepartmentModule
    ],
    providers: [
        {provide: LocationStrategy, useClass: PathLocationStrategy},
        {provide: Position, useClass: Positioning},
        {provide: ToastOptions, useClass: CustomOption},
        LanguageService,
        AuthService,
        ToastService,
        UniversityService,
        DepartmentService,
        CourseService,
        ExamService,
        ReplyService,
        CommentService,
        UserService,
        CourseService,
        ConfirmOptions,

        IsLoggedInAuthGuard,
        IsAdminAuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {

}