/**
 * Created by bdros on 27-Oct-16.
 */
import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs";
import {ToastService} from "./shared/toasts/shared/toast.service";
import {AbstractService} from "./shared/abstract.service";

@Injectable()
export class LanguageService {
    constructor(private http: Http, private toastService: ToastService) {
    }

    changeLanguage(language: string) {
        return this.http.post(`${AbstractService.API_ENDPOINT}/language/${language}`, null)
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

}