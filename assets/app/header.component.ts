import {Component} from "@angular/core";
import {AuthService} from "./auth/shared/auth.service";

@Component({
    selector: 'app-header',
    templateUrl: "header.component.pug"
})
export class HeaderComponent {
    constructor(public authService: AuthService) {
    }

    isAdmin() {
        return this.authService.isAdmin();
    }

    isLoggedIn() {
        return this.authService.isLoggedIn();
    }


}