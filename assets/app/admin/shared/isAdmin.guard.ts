/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {AuthService} from "../../auth/shared/auth.service";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../shared/toasts/shared/toast.service";

@Injectable()
export class IsAdminAuthGuard implements CanActivate {

    constructor(private authService: AuthService, private translateService: TranslateService, private toastService: ToastService) {
    }

    canActivate() {

        if (!this.authService.isAdmin()) {
            this.translateService.get('WORDS.FORBIDDEN').subscribe((forbidden: string) => {
                this.translateService.get('SENTENCES.NOT_AUTHORIZED').subscribe((notAuthorized: string) => {
                    this.toastService.handleError({
                        title: forbidden,
                        error: {message: notAuthorized},
                        status: 403
                    });
                });
            });
        }


        return this.authService.isAdmin();
    }
}