/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Component, OnInit} from "@angular/core";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";
import {Router} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {FormGroup, FormControl} from "@angular/forms";


@Component({
    selector: 'app-admin-university-list',
    templateUrl: 'university.list.admin.component.pug'
})
export class UniversityListAdminComponent implements OnInit {

    constructor(public universityService: UniversityService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private router: Router) {
    }

    universities: University[] = [];
    totalPages: number = 0;
    total: number = 0;
    page: number = 1;
    loading: boolean;


    searchForm: FormGroup;
    searchCriteria: University = new University("", "");

    ngOnInit() {
        this.searchForm = new FormGroup({
            name: new FormControl(null),
            shortName: new FormControl(null)
        });

        this.getList();
    }

    getList() {
        this.universities = [];
        this.loading = true;
        this.universityService.getList(this.page, this.searchCriteria)
            .subscribe(data => {
                    this.loading = false;
                    this.totalPages = data["total_pages"];
                    this.total = data["total"];
                    data['obj'].forEach((university: University) => {

                        this.universities.push(new University(university['name'], university['shortName'], university['information'] ? university['information'] : null,
                            university['_id']));
                    });
                },
                error => console.error(error))
    }

    getNewPage(page: number) {
        this.page = page;
        this.getList();
    }


    onSearchSubmit() {
        this.searchCriteria.name = this.searchForm.value.name;
        this.searchCriteria.shortName = this.searchForm.value.shortName;
        this.getList();
    }

    onSelect(id: string) {
        this.router.navigate(['/admin/universities', id]);
    }

    onEdit(id: string) {
        this.router.navigate(['/admin/universities/edit', id]);
    }


    onDelete(id: string) {
        this.universityService.deleteById(id)
            .subscribe(
                data => {
                    this.translateService.get("UNIVERSITY.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            const deleteUniFilter = this.universities.filter((university: University)=> {

                                return university.id === id;
                            });
                            this.universities.splice(this.universities.indexOf(deleteUniFilter[0]), 1);

                            if (this.universities.length == 0) {
                                this.totalPages--;
                                this.getNewPage(--this.page);
                            } else {
                                this.getList();
                            }
                        });
                });
    }


}