import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";

@Component({
    selector: 'app-admin-university-create',
    templateUrl: 'university.create.admin.component.pug'
})

export class UniversityCreateAdminComponent {
    myForm: FormGroup;

    constructor(private translateService: TranslateService,
                private universityService: UniversityService,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.myForm = new FormGroup({
            name: new FormControl(null, Validators.required),
            shortName: new FormControl(null, Validators.required),
            information: new FormControl(null)
        });
    }

    onSubmit() {
        const university = new University(
            this.myForm.value.name,
            this.myForm.value.shortName,
            this.myForm.value.information
        );
        this.universityService.create(university)
            .subscribe(
                data => {

                    this.translateService.get("UNIVERSITY.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            this.myForm.reset();
                        })
                },
                error => console.error((error))
            );
    }
}