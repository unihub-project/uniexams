import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
    selector: 'app-admin-university-edit',
    templateUrl: 'university.edit.admin.component.pug'
})

export class UniversityEditAdminComponent implements OnInit {


    constructor(private universityService: UniversityService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    public selectedId: string;
    university: University;
    myForm: FormGroup;

    ngOnInit() {

        this.myForm = new FormGroup({
            name: new FormControl(null, Validators.required),
            shortName: new FormControl(null, Validators.required),
            information: new FormControl(null)
        });


        this.activatedRoute.params.forEach((params: Params) => {
            this.selectedId = params['id'];
            console.log("id = "+this.selectedId);
            this.universityService.getById(this.selectedId)
                .subscribe(data => {
                    this.university = new University(data['obj']['name'], data['obj']['shortName'], data['obj']['information'] ? data['obj']['information'] : null
                        , data['obj']['_id']);
                    // console.log("id = "+this.selectedId);
                    this.myForm = new FormGroup({
                        name: new FormControl(this.university.name, Validators.required),
                        shortName: new FormControl(this.university.shortName, Validators.required),
                        information: new FormControl(this.university.information)
                    });
                    // this.myForm.value.name = this.university.name;
                    // this.myForm.value.shortName = this.university.shortName;
                    // this.myForm.value.information = this.university.information;

                });
        });

    }

    onSubmit() {
        const university = new University(
            this.myForm.value.name,
            this.myForm.value.shortName,
            this.myForm.value.information,
            this.selectedId
        );
        this.universityService.update(university)
            .subscribe(
                data => {

                    this.translateService.get("UNIVERSITY.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            this.router.navigate(['/admin/universities/list']);
                        })
                },
                error => console.error((error))
            );
    }
}