/**
 * Created by bdros on 27-Oct-16.
 */
/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Component} from "@angular/core";

import {AuthService} from "../../auth/shared/auth.service";

@Component({
    selector: 'app-admin-university',
    templateUrl: 'university.admin.component.pug'
})
export class UniversityAdminComponent {

    constructor(public authService: AuthService) {
    }

    isAdmin() {
        return this.authService.isAdmin();
    }

}