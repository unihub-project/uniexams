/**
 * Created by stavr on 10/30/2016.
 */

import {Component} from "@angular/core";

import {AuthService} from "../../auth/shared/auth.service";

@Component({
    selector: 'app-admin-course',
    templateUrl: 'course.admin.component.pug'
})
export class CourseAdminComponent {

    constructor(public authService: AuthService) {
    }

    isAdmin() {
        return this.authService.isAdmin();
    }

}
