/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Component, OnInit} from "@angular/core";

import {ActivatedRoute, Router, Params} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {DepartmentService} from "../../../shared/services/department.service";
import {Department} from "../../../shared/models/departent.model";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";
import {Course} from "../../../shared/models/course.model";
import {CourseService} from "../../../shared/services/course.service";

@Component({
    selector: 'app-admin-course-detail',
    templateUrl: 'course.details.admin.component.pug'
})

export class CourseDetailAdminComponent {
    public selectedId: string;
    course: Course;
    loading: boolean;

    ngOnInit() {
        this.loading = true;
        this.activatedRoute.params.forEach((params: Params) => {
            this.selectedId = params['id'];
            this.courseService.getById(this.selectedId)
                .subscribe(data => {
                    this.loading = false;

                    this.course = new Course(
                        data['obj']['name'],
                        data['obj']['courseType'],
                        data['obj']['information'],
                        data['obj']['_id'],
                        data['obj']['exams'],
                        data['obj']['department'],
                        data['obj']['exams'].length
                    );
                });
        });
    }

    onDelete() {
        this.courseService.deleteById(this.selectedId)
            .subscribe(
                data => {
                    this.translateService.get("COURSE.DELETE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            this.router.navigate(['/admin/departments/',this.course.department['_id']]);

                        });
                });
    };


    constructor(private courseService: CourseService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private translateService: TranslateService) {
    }


}