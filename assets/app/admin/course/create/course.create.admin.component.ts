import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {University} from "../../../shared/models/university.model";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {UniversityService} from "../../../shared/services/university.service";
import {DepartmentService} from "../../../shared/services/department.service";
import {Department} from "../../../shared/models/departent.model";
import {AppUtils} from "../../../shared/app.utils";
import {Params, ActivatedRoute} from "@angular/router";
import {CourseService} from "../../../shared/services/course.service";
import {Course} from "../../../shared/models/course.model";

@Component({
    selector: 'app-admin-course-create',
    templateUrl: 'course.create.admin.component.pug'
})

export class CourseCreateAdminComponent {
    myForm: FormGroup;
    departmentId: string;
    courseType: string;
    hasSelected = false;

    constructor(private translateService: TranslateService,
                private activatedRoute: ActivatedRoute,
                private courseService: CourseService,
                private universityService: UniversityService,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            this.departmentId = params['id'];
        });
        this.myForm = new FormGroup({
            name: new FormControl(null, Validators.required),
            information: new FormControl(null)
        });

    }

    onSubmit() {
        const department = new Department(
            null, //name
            null,//shortname
            null, //information
            this.departmentId
        );
        const course = new Course(this.myForm.value.name,
            this.courseType,
            this.myForm.value.information,
            null, //id
            null, //exams[]
            department,
            null //exam counter
        );

        this.courseService.create(course)
            .subscribe(
                data => {

                    this.translateService.get("COURSE.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            this.myForm.reset();
                            this.removed();
                        })
                },
                error => console.error((error))
            );
        // const department = new Department(
        //     this.myForm.value.name,
        //     this.myForm.value.shortName,
        //     this.myForm.value.information,
        //     null,
        //     new University(null, null, null, this.selectedUniversityId)
        // );
        // this.departmentService.create(department)
        //     .subscribe(
        //         data => {
        //
        //             this.translateService.get("UNIVERSITY.CREATE.SUCCESS_MESSAGE")
        //                 .subscribe((succesMessage: string) => {
        //                     this.toastService.handleSuccess({
        //                         message: succesMessage,
        //                     });
        //                     this.myForm.reset();
        //                     this.removed(null);
        //                 })
        //         },
        //         error => console.error((error))
        //     );
    }

    selected(courseType) {
        this.courseType = courseType["id"];
        this.hasSelected = true;
    }

    removed() {
        this.courseType = null;
        this.hasSelected = false;
    }

    courseTypeList: Object = [
        {
            id: 'IPOXREOTIKO',
            text: 'COURSE.COURSE_TYPE.IPOXREOTIKO'
        },
        {
            id: 'EPILOGIS',
            text: 'COURSE.COURSE_TYPE.EPILOGIS'
        }
    ]
}