/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {FormGroup, FormControl} from "@angular/forms";
import {Department} from "../../../shared/models/departent.model";
import {DepartmentService} from "../../../shared/services/department.service";
import {University} from "../../../shared/models/university.model";
import {UniversityService} from "../../../shared/services/university.service";
import {AppUtils} from "../../../shared/app.utils";


@Component({
    selector: 'app-admin-course-list',
    templateUrl: 'course.list.admin.component.pug'
})
export class CourseListAdminComponent implements OnInit {

    departments: Department[] = [];
    universities: University[] = [];
    totalPages: number = 0;
    total: number = 0;
    page: number = 1;
    loading: boolean;

    searchForm: FormGroup;
    searchCriteria: Department = new Department("", "");

    ngOnInit() {
        this.universityService.getAll()
            .subscribe((data) => {
                this.universities = AppUtils.getDataForSelect(data['obj'], "name");
            });

        this.searchForm = new FormGroup({
            name: new FormControl(null),
            shortName: new FormControl(null)
        });

        this.getDepartmentsList();
    }

    getNewPage(page: number) {
        this.page = page;
        this.getDepartmentsList();
    }

    getDepartmentsList() {
        this.loading = true;
        this.departments = [];
        this.departmentService.getList(this.page, this.searchCriteria)
            .subscribe(data => {
                    this.loading = false;
                    this.totalPages = data["total_pages"];
                    this.total = data["total"];
                    data['obj'].forEach((department: Department) => {

                        this.departments.push(new Department(department['name'], department['shortName'], department['information'] ? department['information'] : null,
                            department['_id'], new University(department['university'].name, department['university'].shortName, department['university'].information,
                                department['university']['_id'])));
                    });
                },
                error => console.error(error))
    }

    onSearchSubmit() {
        this.searchCriteria.name = this.searchForm.value.name;
        this.searchCriteria.shortName = this.searchForm.value.shortName;
        console.log(JSON.stringify(this.searchCriteria));
        this.getDepartmentsList();
    }

    onSelect(id: string) {
        this.router.navigate(['/admin/departments', id]);
    }


    onDelete(id: string) {
        this.departmentService.deleteById(id)
            .subscribe(
                data => {
                    this.translateService.get("DEPARTMENT.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            const deleteDepFilter = this.departments.filter((department: Department)=> {

                                return department.id === id;
                            });
                            this.departments.splice(this.departments.indexOf(deleteDepFilter[0]), 1);

                            if (this.departments.length == 0) {
                                this.totalPages--;
                                this.getNewPage(--this.page);
                            } else {
                                this.getDepartmentsList();
                            }
                        });
                });
    }

    onEdit(id: string) {
        this.router.navigate(['/admin/departments/edit', id]);
    }

    onSelectSearchUniversity(university) {
        this.searchCriteria.university = new University(null, null, null, university.id);
        this.getDepartmentsList()
    }

    onRemoveSearchUniversity(university) {
        this.searchCriteria.university = null;
        this.getDepartmentsList()
    }

    constructor(public departmentService: DepartmentService,
                public universityService: UniversityService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private router: Router) {
    }


}