import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {University} from "../../../shared/models/university.model";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {User} from "../../../shared/models/user.model";
import {UserService} from "../../../shared/services/users.service";

@Component({
    selector: 'app-admin-user-edit',
    templateUrl: 'user.edit.admin.component.pug'
})

export class UserEditAdminComponent implements OnInit {


    constructor(private userService: UserService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    public selectedId: string;
    user: User;
    myForm: FormGroup;
    role: String;
    hasSelectedRole: boolean = false;
    public roles: Array<String> = ['Admin','Undergraduate', 'Teacher'];

    ngOnInit() {

        this.myForm = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]),
            role: new FormControl(null),
            isConfirmed: new FormControl(null),
        });


        this.activatedRoute.params.forEach((params: Params) => {
            this.selectedId = params['id'];
            console.log("id = " + this.selectedId);
            this.userService.getById(this.selectedId)
                .subscribe(data => {
                    console.log(data);
                    this.user = new User(
                        data['obj']['email'],
                        /*password*/ null,
                        data['obj']['_id'],
                        /*profileid*/ null,
                        data['obj']['role'],
                        data['obj']['isConfirmed'],
                    );
                    // console.log("id = "+this.selectedId);
                    this.myForm = new FormGroup({
                        email: new FormControl(this.user.email, [Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]),
                        role: new FormControl(this.user.role),
                        isConfirmed: new FormControl(this.user.isConfirmed),
                    });
                    // this.myForm.value.name = this.university.name;
                    // this.myForm.value.shortName = this.university.shortName;
                    // this.myForm.value.information = this.university.information;

                });
        });

    }

    onSubmit() {


        const user =new User(
            this.myForm.value.email,
            /*password*/ null,
            this.user.id,
            /*profileid*/ null,
            this.myForm.value.role,
            this.myForm.value.isConfirmed,
        );
        this.userService.update(user)
            .subscribe(
                data => {

                    this.translateService.get("UNIVERSITY.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            this.router.navigate(['/admin/users/list']);
                        })
                },
                error => console.error((error))
            );
        console.log("submited");
    }


    selected(role){
        this.role = role;
        this.hasSelectedRole = true;
    }

    removed(role){
        this.role = null;
        this.hasSelectedRole = false;
    }
}