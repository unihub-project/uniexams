/**
 * Created by stavr on 12-Jan-17.
 */

import {Component} from "@angular/core";

import {AuthService} from "../../auth/shared/auth.service";

@Component({
    selector: 'app-admin-user',
    templateUrl: 'user.admin.component.pug'
})
export class UserAdminComponent {

    constructor(public authService: AuthService) {
    }

    isAdmin() {
        return this.authService.isAdmin();
    }

}