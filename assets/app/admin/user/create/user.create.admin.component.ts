import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {User} from "../../../shared/models/user.model";
import {AuthService} from "../../../auth/shared/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-admin-user-create',
    templateUrl: 'user.create.admin.component.pug'
})

export class UserCreateAdminComponent {
    myForm: FormGroup;

    constructor(private translateService: TranslateService,
                private authService: AuthService,
                private toastService: ToastService,
                private router: Router) {
    }

    ngOnInit() {
        this.myForm = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]),
            password: new FormControl(null, Validators.required),
        });
    }

    onSubmit() {
        const user = new User(
            this.myForm.value.email,
            this.myForm.value.password,
            // this.myForm.value.firstName,
            // this.myForm.value.lastName
        );
        this.authService.signup(user)
            .subscribe(
                data => {
                    this.translateService.get("SIGNUP.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            this.router.navigate(['/admin/users/']);
                        })
                },
                error => console.error(error)
            );
    }
}