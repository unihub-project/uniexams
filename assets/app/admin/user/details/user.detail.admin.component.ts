/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Component, OnInit} from "@angular/core";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";
import {ActivatedRoute, Router, Params} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";

@Component({
    selector: 'app-admin-user-detail',
    templateUrl: 'user.detail.admin.component.pug'
})
export class UserDetailAdminComponent implements OnInit {
    public selectedId: string;
    university: University;
    depLists: string[];
    loading: boolean;

    ngOnInit() {
        this.loading = true;
        this.activatedRoute.params.forEach((params: Params) => {
            this.selectedId = params['id'];
            this.universityService.getById(this.selectedId)
                .subscribe(data => {
                    this.loading = false;
                    this.university = new University(
                        data['obj']['name'],
                        data['obj']['shortName'],
                        data['obj']['information'] ? data['obj']['information'] : null,
                        data['obj']['_id'],
                        data['obj']['departments']
                    );
                   this.depLists =  data['obj']['departments'];


                });

        });
    }

    onDelete() {
        this.universityService.deleteById(this.selectedId)
            .subscribe(
                data => {
                    this.translateService.get("UNIVERSITY.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            this.router.navigate(['/admin/universities/list']);

                        });
                });
    };

    onSeeDepartment(id: string){
        this.router.navigate(['/admin/departments', id]);
    }

    constructor(private universityService: UniversityService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }


}