/**
 * Created by stavros on 14/1/2017.
 */
import {Component, OnInit} from "@angular/core";
import {University} from "../../../shared/models/university.model";
import {Router} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {FormGroup, FormControl} from "@angular/forms";
import {UserService} from "../../../shared/services/users.service";
import {User} from "../../../shared/models/user.model";


@Component({
    selector: 'app-admin-user-list',
    templateUrl: 'user.list.admin.component.pug'
})
export class UserListAdminComponent implements OnInit {

    constructor(public userService: UserService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private router: Router) {
    }

    users: User[] = [];
    totalPages: number = 0;
    total: number = 0;
    page: number = 1;
    loading: boolean;


    searchForm: FormGroup;
    searchCriteria: University = new University("", "");

    ngOnInit() {
        this.searchForm = new FormGroup({
            name: new FormControl(null),
            shortName: new FormControl(null)
        });

        this.getList();
    }

    getList() {
        this.users = [];
        this.loading = true;
        this.userService.getList(this.page, this.searchCriteria)
            .subscribe(data => {
                    this.loading = false;
                    this.totalPages = data["total_pages"];
                    this.total = data["total"];
                    data['obj'].forEach((user: User) => {
                        this.users.push(new User(user['email'], null, user['_id'], null, user['role']));

                    });
                },
                error => console.error(error));
    }

    getNewPage(page: number) {
        this.page = page;
        this.getList();
    }


    onSearchSubmit() {
        this.searchCriteria.name = this.searchForm.value.name;
        this.searchCriteria.shortName = this.searchForm.value.shortName;
        this.getList();
    }

    onSelect(id: string) {
        this.router.navigate(['/admin/users', id]);
    }

    onEdit(id: string) {
        this.router.navigate(['/admin/users/edit', id]);
    }


    onDelete(id: string) {
        this.userService.deleteById(id)
            .subscribe(
                data => {
                    this.translateService.get("USER.DELETE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            const deleteUniFilter = this.users.filter((user: User)=> {

                                return user.id === id;
                            });
                            this.users.splice(this.users.indexOf(deleteUniFilter[0]), 1);

                            if (this.users.length == 0) {
                                this.totalPages--;
                                this.getNewPage(--this.page);
                            } else {
                                this.getList();
                            }
                        });
                });
    }


}