import {NgModule} from "@angular/core";

import {adminRouting} from "./admin.routing";
import {SharedModule} from "../shared/shared.module";
import {UniversityListAdminComponent} from "./university/list/university.list.admin.component";
import {UniversityDetailAdminComponent} from "./university/details/university.detail.admin.component";
import {UniversityAdminComponent} from "./university/university.admin.component";
import {UniversityCreateAdminComponent} from "./university/create/university.create.admin.component";
import {UniversityEditAdminComponent} from "./university/edit/university.edit.admin.component";
import {DepartmentAdminComponent} from "./department/department.admin.component";
import {DepartmentListAdminComponent} from "./department/list/department.list.admin.component";
import {DepartmentCreateAdminComponent} from "./department/create/department.create.admin.component";
import {DepartmentEditAdminComponent} from "./department/edit/department.edit.admin.component";
import {DepartmentDetailAdminComponent} from "./department/details/department.details.admin.component";
import {UserAdminComponent} from "./user/user.admin.compoent";
import {UserListAdminComponent} from "./user/list/user.list.admin.component";
import {UserCreateAdminComponent} from "./user/create/user.create.admin.component";
import {UserEditAdminComponent} from "./user/edit/user.edit.admin.component";
import {UserDetailAdminComponent} from "./user/details/user.detail.admin.component";
import {CourseAdminComponent} from "./course/course.admin.component";
import {CourseListAdminComponent} from "./course/list/course.list.admin.component";
import {CourseCreateAdminComponent} from "./course/create/course.create.admin.component";
import {CourseDetailAdminComponent} from "./course/details/course.details.admin.component";

@NgModule({
    declarations: [
        UniversityAdminComponent,
        UniversityListAdminComponent,
        UniversityDetailAdminComponent,
        UniversityCreateAdminComponent,
        UniversityEditAdminComponent,
        DepartmentAdminComponent,
        DepartmentListAdminComponent,
        DepartmentCreateAdminComponent,
        DepartmentEditAdminComponent,
        DepartmentDetailAdminComponent,
        UserAdminComponent,
        UserListAdminComponent,
        UserCreateAdminComponent,
        UserEditAdminComponent,
        UserDetailAdminComponent,
        CourseAdminComponent,
        CourseListAdminComponent,
        CourseCreateAdminComponent,
        CourseDetailAdminComponent
    ],
    imports: [
        SharedModule,
        adminRouting
    ]
})
export class AdminModule {

}