import {Component} from "@angular/core";
import {AuthService} from "../auth/shared/auth.service";

@Component({
    selector: 'app-admin',
    template: `
        <header class="row spacing">
            <nav class="col-md-12">
                <ul class="nav nav-tabs">
                    <li routerLinkActive="active" *ngIf="isAdmin()"><a [routerLink]="['users']">Users</a></li>
                    <li routerLinkActive="active" *ngIf="isAdmin()"><a [routerLink]="['universities']">Universities</a></li>
                    <li routerLinkActive="active" *ngIf="isAdmin()"><a [routerLink]="['departments']">Departments</a></li>
                    <!--<li routerLinkActive="active" *ngIf="isAdmin()"><a [routerLink]="['courses']">Courses</a></li>-->
                </ul>
            </nav>
        </header>
        <div class="container">
           <router-outlet></router-outlet>
        </div>
    `
})
export class AdminComponent {
    constructor(public authService: AuthService) {
    }

    isAdmin() {
        return this.authService.isAdmin();
    }
}