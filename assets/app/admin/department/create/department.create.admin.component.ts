import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {University} from "../../../shared/models/university.model";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {UniversityService} from "../../../shared/services/university.service";
import {DepartmentService} from "../../../shared/services/department.service";
import {Department} from "../../../shared/models/departent.model";
import {AppUtils} from "../../../shared/app.utils";

@Component({
    selector: 'app-admin-department-create',
    templateUrl: 'department.create.admin.component.pug'
})

export class DepartmentCreateAdminComponent {
    myForm: FormGroup;
    selectedUniversityId: string;
    universityList : any[];
    hasSelected = false;

    constructor(private translateService: TranslateService,
                private departmentService: DepartmentService,
                private universityService: UniversityService,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.universityService.getAll()
            .subscribe(data =>{
                this.universityList = AppUtils.getDataForSelect(data['obj'], "name");
                console.log(data);
            });


        this.myForm = new FormGroup({
            name: new FormControl(null, Validators.required),
            shortName: new FormControl(null, Validators.required),
            information: new FormControl(null)
        });

    }

    onSubmit() {
        const department = new Department(
            this.myForm.value.name,
            this.myForm.value.shortName,
            this.myForm.value.information,
            null,
            new University(null, null, null, this.selectedUniversityId)
        );
        this.departmentService.create(department)
            .subscribe(
                data => {

                    this.translateService.get("UNIVERSITY.CREATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                message: succesMessage,
                            });
                            this.myForm.reset();
                            this.removed(null);
                        })
                },
                error => console.error((error))
            );
    }

    selected(university){
        this.selectedUniversityId = university["id"];
        this.hasSelected = true;
        console.log(this.selectedUniversityId);
        console.log(university);
    }

    removed(university){
        this.selectedUniversityId = null;
        this.hasSelected = false;
    }
}