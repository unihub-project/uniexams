
/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Component, OnInit} from "@angular/core";

import {ActivatedRoute, Router, Params} from "@angular/router";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {DepartmentService} from "../../../shared/services/department.service";
import {Department} from "../../../shared/models/departent.model";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";

@Component({
    selector: 'app-admin-department-detail',
    templateUrl: 'department.details.admin.component.pug'
})

export class DepartmentDetailAdminComponent {
    public selectedId: string;
    department: Department;
    courseLists: string[];
    loading: boolean;

    ngOnInit() {
        this.loading = true;
        this.activatedRoute.params.forEach((params: Params) => {
            this.selectedId = params['id'];
            this.departmentService.getById(this.selectedId)
                .subscribe(data => {
                    this.loading = false;

                    // constructor(public name: string,
                    //     public shortName: string,
                    //     public information?: string,
                    //     public id?: string,
                    //     public university?: University
                    // ) {}

                    console.log(data['obj']);
                    this.department = new Department(
                        data['obj']['name'],
                        data['obj']['shortName'],
                        data['obj']['information'] ? data['obj']['information'] : null,
                        data['obj']['_id'],
                        data['obj']['university']
                    );
                    this.courseLists =  data['obj']['courses'];


                });

        });
    }

    onDelete() {
        this.departmentService.deleteById(this.selectedId)
            .subscribe(
                data => {
                    this.translateService.get("DEPARTMENT.DELETE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            this.router.navigate(['/admin/department/list']);

                        });
                });
    };

    onSeeCourse(id: string){
        this.router.navigate(['/admin/courses', id]);
    }

    newCourse(){
        this.router.navigate(['/admin/courses/create', this.selectedId]);
    }

    constructor(private universityService: UniversityService,
                private departmentService: DepartmentService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }


}