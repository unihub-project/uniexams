/**
 * Created by stavr on 10/30/2016.
 */

import {Component} from "@angular/core";

import {AuthService} from "../../auth/shared/auth.service";

@Component({
    selector: 'app-admin-department',
    templateUrl: 'department.admin.component.pug'
})
export class DepartmentAdminComponent {

    constructor(public authService: AuthService) {
    }

    isAdmin() {
        return this.authService.isAdmin();
    }

}
