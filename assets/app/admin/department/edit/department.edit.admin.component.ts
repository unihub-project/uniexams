import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {DepartmentService} from "../../../shared/services/department.service";
import {Department} from "../../../shared/models/departent.model";
import {UniversityService} from "../../../shared/services/university.service";
import {University} from "../../../shared/models/university.model";
import {AppUtils} from "../../../shared/app.utils";

@Component({
    selector: 'app-admin-department-edit',
    templateUrl: 'department.edit.admin.component.pug'
})

export class DepartmentEditAdminComponent implements OnInit {


    constructor(private universityService: UniversityService,
                private departmentService: DepartmentService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    public selectedId: string;
    myForm: FormGroup;
    selectedUniversityId: string;
    department: Department;
    universityList : any[];
    initSelectBox : any[];
    hasSelected: boolean= false;
    loading: boolean = false;


    ngOnInit() {
        this.myForm = new FormGroup({
            name: new FormControl(null, Validators.required),
            shortName: new FormControl(null, Validators.required),
            information: new FormControl(null)
        });

        this.loading = true;
        this.activatedRoute.params.forEach((params: Params) => {
            this.selectedId = params['id'];
            console.log("id = "+this.selectedId);
            this.departmentService.getById(this.selectedId)
                .subscribe(data => {
                    const department = data['obj'];
                    const university = data['obj']['university'];
                    this.department = new Department(department['name'], department['shortName'], department['information'] ? department['information'] : null
                        , department['_id'], new University(university['name'], university['shortName'], university['information'] ? university['information'] : null
                            , university['_id']));
                    this.myForm = new FormGroup({
                        name: new FormControl(this.department.name, Validators.required),
                        shortName: new FormControl(this.department.shortName, Validators.required),
                        information: new FormControl(this.department.information)
                    });

                    this.universityService.getAll()
                        .subscribe((data) => {
                            this.loading = false;
                            this.universityList = AppUtils.getDataForSelect(data['obj'], "name");
                            this.initSelectBox = this.universityList.filter((element) => {
                                return element.id === this.department.university.id;
                            });

                        });

                });
        });

    }

    selected(university){
        this.selectedUniversityId = university["id"];
        this.hasSelected = true;
    }

    removed(university){
        this.selectedUniversityId = null;
        this.hasSelected = false;
    }

    onSubmit() {
        const department = new Department(
            this.myForm.value.name,
            this.myForm.value.shortName,
            this.myForm.value.information,
            this.selectedId,
            new University(null, null, null, this.selectedUniversityId)
        );
        this.departmentService.update(department)
            .subscribe(
                data => {

                    this.translateService.get("DEPARTMENT.UPDATE.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });
                            this.router.navigate(['/admin/departments/list']);
                        })
                },
                error => console.error((error))
            );
    }
}