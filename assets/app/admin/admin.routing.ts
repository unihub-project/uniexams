import {Routes, RouterModule} from "@angular/router";
import {UniversityListAdminComponent} from "./university/list/university.list.admin.component";
import {UniversityAdminComponent} from "./university/university.admin.component";
import {UniversityDetailAdminComponent} from "./university/details/university.detail.admin.component";
import {UniversityCreateAdminComponent} from "./university/create/university.create.admin.component";
import {UniversityEditAdminComponent} from "./university/edit/university.edit.admin.component";
import {DepartmentAdminComponent} from "./department/department.admin.component";
import {DepartmentListAdminComponent} from "./department/list/department.list.admin.component";
import {DepartmentCreateAdminComponent} from "./department/create/department.create.admin.component";
import {DepartmentEditAdminComponent} from "./department/edit/department.edit.admin.component";
import {DepartmentDetailAdminComponent} from "./department/details/department.details.admin.component";
import {UserAdminComponent} from "./user/user.admin.compoent";
import {UserDetailAdminComponent} from "./user/details/user.detail.admin.component";
import {UserEditAdminComponent} from "./user/edit/user.edit.admin.component";
import {UserCreateAdminComponent} from "./user/create/user.create.admin.component";
import {UserListAdminComponent} from "./user/list/user.list.admin.component";
import {CourseAdminComponent} from "./course/course.admin.component";
import {CourseCreateAdminComponent} from "./course/create/course.create.admin.component";
import {CourseDetailAdminComponent} from "./course/details/course.details.admin.component";


const ADMIN_ROUTES: Routes = [
    {path: '', redirectTo: 'universities', pathMatch: 'full'},
    {
        path: 'universities', component: UniversityAdminComponent, children: [
        {
            path: 'list', component: UniversityListAdminComponent
        },
        {
            path: 'create', component: UniversityCreateAdminComponent
        },
        {
            path: 'edit/:id', component: UniversityEditAdminComponent
        },
        {
            path: ':id', component: UniversityDetailAdminComponent
        },
        {
            path: '', redirectTo: 'list'
        }
    ]
    },
    {
        path: 'departments', component: DepartmentAdminComponent,
        children: [
            {
                path: 'list', component: DepartmentListAdminComponent
            },
            {
                path: 'create', component: DepartmentCreateAdminComponent
            },
            {
                path: 'edit/:id', component: DepartmentEditAdminComponent
            },
            {
                path: ':id', component: DepartmentDetailAdminComponent
            },
            {
                path: '', redirectTo: 'list'
            }
        ]
    },
    {
        path: 'courses', component: CourseAdminComponent,
        children: [
            {
                path: 'list', component: DepartmentListAdminComponent
            },
            {
                path: 'create/:id', component: CourseCreateAdminComponent
            },
            {
                path: 'edit/:id', component: DepartmentEditAdminComponent
            },
            {
                path: ':id', component: CourseDetailAdminComponent
            },
            {
                path: '', redirectTo: 'list'
            }
        ]
    },
    {
        path: 'users', component: UserAdminComponent,
        children: [
            {
                path: 'list', component: UserListAdminComponent
            },
            {
                path: 'create', component: UserCreateAdminComponent
            },
            {
                path: 'edit/:id', component: UserEditAdminComponent
            },
            {
                path: ':id', component: UserDetailAdminComponent
            },
            {
                path: '', redirectTo: 'list'
            }
        ]
    }
];


export const adminRouting = RouterModule.forChild(ADMIN_ROUTES);
