/**
 * Created by bdros on 27-Oct-16.
 */
import {Component, OnInit, OnDestroy} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-page-not-found',
    template: `
      <div class="container">
        <h2 class="col-md-6 col-md-offset-3">
            ERROR 404 PAGE NOTE FOUND!
        </h2> 
      </div>
    `
})

export class PageNotFoundComponent {
}