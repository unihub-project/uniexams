/**
 * Created by bdrosatos on 28/10/2016.
 */
// RxJS
import 'rxjs';
import 'reflect-metadata';

// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';