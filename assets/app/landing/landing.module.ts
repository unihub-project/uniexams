/**
 * Created by stavros on 23/2/2017.
 */

import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {LandingPage} from "./landing.page";
import {AuthModule} from "../auth/auth.module";
import {SigninComponent} from "../auth/signin/signin.component";
import {AuthenticationComponent} from "../auth/authentication.component";


@NgModule({
    //components
    declarations: [

    ],
    //modules
    imports: [
        SharedModule,
        AuthModule
    ],

    // services
    providers: [

    ]

})
export class LandingModule {

}