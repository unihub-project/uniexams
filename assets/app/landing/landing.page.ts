/**
 * Created by stavros on 23/2/2017.
 */

import {Component} from "@angular/core";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../shared/toasts/shared/toast.service";
import {AuthService} from "../auth/shared/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-landing-page',
    templateUrl: './landing.page.pug'
})
export class LandingPage {
    constructor(private translateService: TranslateService,
                private toastService: ToastService,
                private authService: AuthService,
                private router: Router,) {
    }

    ngOnInit() {
        if (this.authService.isLoggedIn()) {
            this.router.navigate(['/home']);
        }
    }
}