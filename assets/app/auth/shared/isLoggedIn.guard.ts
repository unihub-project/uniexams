/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {AuthService} from "./auth.service";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../shared/toasts/shared/toast.service";

@Injectable()
export class IsLoggedInAuthGuard implements CanActivate {

    constructor(private authService: AuthService, private translateService: TranslateService, private toastService: ToastService) {
    }

    canActivate() {

        if (!this.authService.isLoggedIn()) {
            this.translateService.get('WORDS.FORBIDDEN').subscribe((forbidden: string) => {
                this.translateService.get('SENTENCES.ONLY_LOGGED_IN').subscribe((onlyLoggedIn: string) => {
                    this.toastService.handleError({
                        title: forbidden,
                        error: {message: onlyLoggedIn},
                        status: 403
                    });
                });
            });
        }
        return this.authService.isLoggedIn();
    }
}
