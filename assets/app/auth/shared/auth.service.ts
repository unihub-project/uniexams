import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs";
import {User} from "../../shared/models/user.model";
import {Profile} from "../../shared/models/profile.model";
import {ToastService} from "../../shared/toasts/shared/toast.service";
import {AbstractService} from "../../shared/abstract.service";

@Injectable()
export class AuthService {
    constructor(private http: Http, private toastService: ToastService) {
    }

    signup(user: User) {
        const body = JSON.stringify(user);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${AbstractService.API_ENDPOINT}/users`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    signin(user: User) {
        const body = JSON.stringify(user);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${AbstractService.API_ENDPOINT}/users/authenticate`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    createProfile(profile: Profile) {
        const body = JSON.stringify(profile);
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token')
        });
        return this.http.post(`${AbstractService.API_ENDPOINT}/profiles`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    editProfile(profile: Profile) {
        const body = JSON.stringify(profile);
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token')
        });
        return this.http.patch(`${AbstractService.API_ENDPOINT}/profiles/${profile.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getProfileById(id: string) {
        const headers = new Headers({
            'Authorization': localStorage.getItem('token')
        });
        return this.http.get(`${AbstractService.API_ENDPOINT}/profiles/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    logout(): void {
        localStorage.clear();
    }

    isLoggedIn(): boolean {
        return localStorage.getItem('token') !== null;
    }

    isAdmin(): boolean {
        if (localStorage.getItem('user') == null) return false;

        const user: User = JSON.parse(localStorage.getItem('user'));

        if (user.role === 'Admin') return true;
    }

    getCurrentUserId(): string {
        return JSON.parse(localStorage.getItem('user')).id
    }
}