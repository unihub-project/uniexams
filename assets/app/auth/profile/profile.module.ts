/**
 * Created by bdrosatos on 26/2/2017.
 */
import {EditProfileComponent} from "./edit/editProfile.component";
import {SharedModule} from "../../shared/shared.module";
import {profileRouting} from "./profile.routing";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
        EditProfileComponent
    ],
    imports: [
        SharedModule,
        profileRouting,
        FormsModule
    ]
})
export class ProfileModule {

}