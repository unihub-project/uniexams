/**
 * Created by bdrosatos on 26/2/2017.
 */
/**
 * Created by bdros on 25-Oct-16.
 */
import {Component} from "@angular/core";


@Component({
    selector: 'app-profile',
    template: `<router-outlet></router-outlet>`
})
export class ProfileComponent {
    constructor() {}

}