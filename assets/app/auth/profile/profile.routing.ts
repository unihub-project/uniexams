/**
 * Created by bdrosatos on 26/2/2017.
 */
import {Routes, RouterModule} from "@angular/router";
import {EditProfileComponent} from "./edit/editProfile.component";


const PROFILE_ROUTES: Routes = [
    {path: '', redirectTo: 'edit', pathMatch: 'full'},
    {path: 'edit', component: EditProfileComponent},
];

export const profileRouting = RouterModule.forChild(PROFILE_ROUTES);