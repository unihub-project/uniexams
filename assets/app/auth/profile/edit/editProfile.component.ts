/**
 * Created by bdros on 25-Oct-16.
 */
import {Component, OnInit, Input, Output} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {User} from "../../../shared/models/user.model";
import {Profile} from "../../../shared/models/profile.model";
import {AuthService} from "../../shared/auth.service";
import {ToastService} from "../../../shared/toasts/shared/toast.service";
import * as moment from "moment";
import {University} from "../../../shared/models/university.model";
import {UniversityService} from "../../../shared/services/university.service";
import {Department} from "../../../shared/models/departent.model";
import {DepartmentService} from "../../../shared/services/department.service";
import {AppUtils} from "../../../shared/app.utils";

@Component({
    selector: 'app-edit-profile',
    templateUrl: 'editProfile.component.pug'
})
export class EditProfileComponent implements OnInit {
    myForm: FormGroup;
    profileId: string = null;
    loading: boolean = false;
    @Input() @Output() birthDate: Date = null;
    isCollapsed: boolean = true;

    departments: Department[] = [];
    universities: University[] = [];
    universitySelected: boolean;

    selectedUniversityId: string = null;
    selectedDepartmentId: string = null;

    selectedUniversityName: string = null;
    selectedDepartmentName: string = null;

    constructor(private authService: AuthService,
                private toastService: ToastService,
                private universityService: UniversityService,
                private departmentService: DepartmentService,
                private router: Router) {
    }


    ngOnInit() {
        this.universitySelected = false;
        this.myForm = new FormGroup({
            firstName: new FormControl(null, [
                Validators.required
            ]),
            lastName: new FormControl(null)
        });
        const curentUser: User = JSON.parse(localStorage.getItem('user'));
        this.profileId = curentUser.profileId;
        if (this.profileId) {

            this.loading = true;
            this.authService.getProfileById(this.profileId)
                .subscribe(
                    data => {
                        this.loading = false;
                        const profile = data['obj'];
                        this.myForm = new FormGroup({
                            firstName: new FormControl(profile.firstName, [
                                Validators.required
                            ]),
                            lastName: new FormControl(profile.lastName)
                        });
                        this.birthDate = profile.birthDate ? moment(profile.birthDate, 'YYYY-MM-DD').toDate() : new Date();
                        this.selectedDepartmentId = profile["defaultDepartment"]['_id'];
                        this.selectedDepartmentName = profile["defaultDepartment"]['name'];
                        this.selectedUniversityId = profile["defaultDepartment"]['university']['_id'];
                        this.selectedUniversityName = profile["defaultDepartment"]['university']['name'];

                    }
                );
        }

        this.universityService.getAll()
            .subscribe((data) => {
                this.universities = AppUtils.getDataForSelect(data['obj'], "name");
            });
    }

    onSubmit() {
        if (!this.profileId) {
            //Create new profile
            const profile = new Profile(this.myForm.value.firstName, this.selectedDepartmentId, this.myForm.value.lastName, null,
                this.formatBithDate(this.birthDate));
            let user: User;
            this.authService.createProfile(profile)
                .subscribe(
                    data => {
                        this.toastService.handleSuccess({
                            title: '',
                            message: 'Profile created successfully'
                        });

                        user = JSON.parse(localStorage.getItem('user'));
                        user.profileId = data['obj']._id;
                        localStorage.setItem('user', JSON.stringify('user'));
                        this.router.navigateByUrl('/home');
                    },
                    error => console.error(error)
                );
        } else {
            //Update Profile
            const profile = new Profile(this.myForm.value.firstName, this.selectedDepartmentId, this.myForm.value.lastName, this.profileId,
                this.formatBithDate(this.birthDate));
            let user: User;
            this.authService.editProfile(profile)
                .subscribe(
                    data => {
                        this.toastService.handleSuccess({
                            title: '',
                            message: 'Profile updated successfully'
                        });
                    },
                    error => console.error(error)
                );
        }
    }

    getFormatedBirthdate(): string {
        return moment(this.birthDate).format('YYYY-MM-DD')
    }

    formatBithDate(date) {
        return moment(date, 'YYYY-MM-DD').add(1, 'day').toDate();
    }

    onSelectSearchUniversity(university) {
        this.departmentService.getAll(university.id)
            .subscribe((data) => {
                this.departments = AppUtils.getDataForSelect(data['obj']['departments'], "name");
                this.universitySelected = true;
            });
    }

    onSelectSearchDepartment(department) {
        this.selectedDepartmentId = department.id;
    }
}