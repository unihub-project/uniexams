import {Component, OnInit} from "@angular/core";
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {AuthService} from "../shared/auth.service";
import {User} from "../../shared/models/user.model";
import {Router} from "@angular/router";
import {ToastService} from "../../shared/toasts/shared/toast.service";
import {TranslateService} from "ng2-translate";
import {AppUtils} from "../../shared/app.utils";
import {UniversityService} from "../../shared/services/university.service";
import {Department} from "../../shared/models/departent.model";
import {University} from "../../shared/models/university.model";
import {DepartmentService} from "../../shared/services/department.service";
import {Profile} from "../../shared/models/profile.model";

@Component({
    selector: 'app-signup',
    templateUrl: 'signup.component.html'
})
export class SignupComponent implements OnInit {
    registerForm: FormGroup;

    departments: Department[] = [];
    universities: University[] = [];
    universitySelected: boolean;

    constructor(private authService: AuthService,
                private universityService: UniversityService,
                private departmentService: DepartmentService,
                private translateService: TranslateService,
                private toastService: ToastService,
                private builder: FormBuilder,
                private router: Router) {
    }

    onSubmit() {

        const user: User = new User(this.registerForm['controls']['user']['controls']['email'].value, this.registerForm['controls']['user']['controls']['passwords']['controls']['password'].value);
        const profile: Profile = new Profile(this.registerForm['controls']['profile']['controls']['firstName'].value, this.registerForm['controls']['profile']['controls']['departmentId'].value,
            this.registerForm['controls']['profile']['controls']['lastName'].value);

        this.authService.signup(user)
            .subscribe(
                user => {
                    localStorage.setItem('token', user['obj']['token']);
                    this.authService.createProfile(profile)
                        .subscribe(profile => {
                                localStorage.removeItem('token');
                                this.translateService.get("SIGNUP.SUCCESS_MESSAGE")
                                    .subscribe((succesMessage: string) => {

                                        this.toastService.handleSuccess({
                                            title: '',
                                            message: succesMessage,
                                        });
                                        this.registerForm.reset();
                                        this.router.navigate(['home']);
                                    })
                            },
                            error => {
                                localStorage.removeItem('token');
                                console.error(error)
                            }
                        )
                },
                error => console.error(error)
            );
    }

    ngOnInit() {

        this.registerForm = this.builder.group({
            'user': this.builder.group({
                'email': ['', Validators.compose([Validators.required,
                    Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                ])],
                'passwords': this.builder.group({
                    password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15)])],
                    repeatPassword: ['', Validators.required]
                }, {validator: this.areEqual}),
            }),
            'profile': this.builder.group({
                'firstName': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
                'lastName': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
                'departmentId': ['', Validators.required]
            })
        });


        this.universityService.getAll()
            .subscribe((data) => {
                this.universities = AppUtils.getDataForSelect(data['obj'], "name");
            });
    }

    onSelectUniversity(university) {
        this.departmentService.getAll(university.id)
            .subscribe((data)=> {
                this.departments = AppUtils.getDataForSelect(data['obj']['departments'], "name");
                this.universitySelected = true;
            });
    }

    onSelectDepartment(department) {
        this.registerForm['controls']['profile']['controls']['departmentId'].setValue(department.id);

        console.log(this.registerForm);
    }

    areEqual(group: FormGroup) {
        var valid = false;
        console.log(group.controls["password"].value === group.controls["repeatPassword"].value);
        if (group.controls["password"].value === group.controls["repeatPassword"].value)
            valid = true;

        if (valid) {
            return null;
        }

        return {
            areEqual: true
        };
    }
}