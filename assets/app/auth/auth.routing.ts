import {Routes, RouterModule} from "@angular/router";
import {SignupComponent} from "./signup/signup.component";
import {SigninComponent} from "./signin/signin.component";
import {LogoutComponent} from "./logout/logout.component";
import {ProfileComponent} from "./profile/profile.component";

const AUTH_ROUTES: Routes = [
    {path: '', redirectTo: 'signin', pathMatch: 'full'},
    {path: 'signup', component: SignupComponent},
    {path: 'signin', component: SigninComponent},
    {path: 'profile', component: ProfileComponent, loadChildren: './profile/profile.module#ProfileModule'},
    {path: 'logout', component: LogoutComponent}
];

export const authRouting = RouterModule.forChild(AUTH_ROUTES);