    import {Component} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {User} from "../../shared/models/user.model";
import {AuthService} from "../shared/auth.service";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../shared/toasts/shared/toast.service";

@Component({
    selector: 'app-signin',
    templateUrl: 'signin.component.html'
})
export class SigninComponent {
    myForm: FormGroup;

    constructor(private authService: AuthService, private translateService: TranslateService, private toastService: ToastService, private router: Router) {
    }

    onSubmit() {
        let user = new User(this.myForm.value.email, this.myForm.value.password);
        this.authService.signin(user)
            .subscribe(
                data => {
                    localStorage.setItem('token', data['obj'].token);
                    user = new User(
                        data['obj'].user['email'],
                        /*password*/ null,
                        data['obj'].user['_id'],
                        /*profileID*/ null,
                        data['obj'].user['role'],
                        /*isConfirmed*/ null,
                    );

                    if (!data['obj'].user['profile']) {
                        this.router.navigateByUrl('auth/profile/edit');
                        localStorage.setItem('user', JSON.stringify(user));
                    } else {
                        user.profileId = data['obj'].user['profile'];
                        localStorage.setItem('user', JSON.stringify(user));
                        this.router.navigateByUrl('/home');
                    }

                    this.translateService.get("SIGNIN.SUCCESS_MESSAGE")
                        .subscribe((succesMessage: string) => {
                            this.toastService.handleSuccess({
                                title: data["title"],
                                message: succesMessage,
                            });

                        })
                },
                error => console.error(error)
            );
    }

    ngOnInit() {
        if (localStorage.getItem('token')) {
            this.router.navigate(['home']);
        }
        this.myForm = new FormGroup({
            email: new FormControl(null, [
                Validators.required,
                Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            ]),
            password: new FormControl(null, Validators.required)
        });
    }
}