import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../shared/auth.service";
import {TranslateService} from "ng2-translate";
import {ToastService} from "../../shared/toasts/shared/toast.service";

@Component({
    selector: 'app-logout',
    template: ``
})
export class LogoutComponent implements OnInit{
    constructor(private authService: AuthService, private translateService: TranslateService, private toastService: ToastService, private router: Router) {
    }

    ngOnInit() {
        this.authService.logout();
        this.translateService.get(["MAIN_MENU.LOGOUT","LOGOUT.SUCCESS_MESSAGE"])
            .subscribe((messages) => {
                console.log(messages);
                this.toastService.handleSuccess({
                    title: messages["MAIN_MENU.LOGOUT"],
                    message: messages["LOGOUT.SUCCESS_MESSAGE"],
                });

            });
        this.router.navigate(['/welcome', 'signin']);
    }
}