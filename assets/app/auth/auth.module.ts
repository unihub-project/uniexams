import { NgModule } from '@angular/core';

import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { LogoutComponent } from "./logout/logout.component";
import { authRouting } from "./auth.routing";
import {SharedModule} from "../shared/shared.module";
import {ProfileComponent} from "./profile/profile.component";

@NgModule({
    declarations: [
        SigninComponent,
        SignupComponent,
        ProfileComponent,
        LogoutComponent
    ],
    imports: [
        SharedModule,
        authRouting
    ]
})
export class AuthModule {

}