import {Component, OnInit} from "@angular/core";
import {Error} from "./shared/error.model";
import {Success} from "./shared/success.model";
import {ToastsManager, Toast} from "ng2-toastr";
import {ToastService} from "./shared/toast.service";
import {Router} from "@angular/router";
import {TranslateService} from "ng2-translate";

@Component({
    selector: 'app-toast',
    template: '',
})
export class ToastComponent implements OnInit {
    success: Success;
    error: Error;

    constructor(private toastService: ToastService,
                private toastr: ToastsManager,
                private router: Router,
                private translateService : TranslateService) {
    }

    ngOnInit() {
        this.toastService.errorOccurred
            .subscribe(
                (error: Error) => {
                    if (error.message == 'INVALID_TOKEN') {
                        this.translateService.get("TOKEN.TOKEN_EXPIRED")
                            .subscribe((errorMessage: string) => {
                                 const tokenError = {
                                     error: {
                                         message: errorMessage
                                     }
                                 };
                                this.toastService.handleError(
                                   tokenError
                                );
                                this.router.navigateByUrl('/auth/logout');
                            });
                        return;
                    }

                    this.error = error;
                    this.toastr.error(error.message, error.title, {dismiss: 'auto'});

                }
            );

        this.toastService.successOccurred
            .subscribe(
                (success: Success) => {
                    this.success = success;
                    this.toastr.success(success.message, success.title, {dismiss: 'auto'});
                }
            );
    }
}