/**
 * Created by bdrosatos on 28/10/2016.
 */
export class Success {
    constructor(public title: string, public message: string) {}
}