import { EventEmitter } from "@angular/core";

import { Error } from "./error.model";
import {Success} from "./success.model";

export class ToastService {
    successOccurred = new EventEmitter<Success>();
    errorOccurred = new EventEmitter<Error>();

    handleSuccess(success: any) {
        const successData = new Error(success.title, success.message);
        this.successOccurred.emit(successData);
    }

    handleError(error: any) {
        const errorData = new Error(error.title, error.error.message, error.status);
        this.errorOccurred.emit(errorData);
    }
}