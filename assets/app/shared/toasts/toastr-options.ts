/**
 * Created by bdrosatos on 4/3/2017.
 */
import {ToastOptions} from 'ng2-toastr';

export class CustomOption extends ToastOptions {
    animate = 'flyRight';
    positionClass = 'toast-bottom-right';
    toastLife = 5000;
    maxShown = 2;
}