/**
 * Created by bdros on 29-Oct-16.
 */
import {NgModule} from "@angular/core";
import {PaginationComponent} from "./pagination.component";
import {TranslateModule} from "ng2-translate";
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [
        PaginationComponent
    ],
    imports: [
        CommonModule,
        TranslateModule
    ],
    exports: [
        PaginationComponent
    ]
})

export class PaginationModule {
}