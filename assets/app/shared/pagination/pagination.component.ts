/**
 * Created by bdros on 29-Oct-16.
 */
import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange} from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: 'pagination.component.pug',
    styleUrls: ['pagination.component.sass']
})

export class PaginationComponent implements  OnChanges {
    @Input() totalPages: number;
    @Input() total: number;
    @Input() currentPage: number;
    @Output("pageChanged") pageChangedEvent: EventEmitter<number> = new EventEmitter<number>();
    pages: number[] = [];



    onChangePage(page: number) {
        this.pageChangedEvent.next(page);
    }

    pageGenerator(){
        this.pages = [];
        for (let i = 1; i <= this.totalPages; i++) {
            this.pages.push(i);
        }
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName === 'totalPages') {
               this.pageGenerator();
            }
        }
    }

}