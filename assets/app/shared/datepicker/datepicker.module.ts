/**
 * Created by stavros on 9/3/2017.
 */
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DatePickerComponent} from "./datepicker.component";
import {TranslateModule} from "ng2-translate";
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
       DatePickerComponent
    ],
    imports: [
        CommonModule,
        TranslateModule,
        FormsModule
    ],
    exports: [
        DatePickerComponent
    ]
})

export class DatePickerModule {
}