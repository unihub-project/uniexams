/**
 * Created by stavros on 9/3/2017.
 */
import {Component, Input, Output, EventEmitter, DoCheck} from "@angular/core";
import {TranslateService} from "ng2-translate";
// declare var $: JQueryStatic;

@Component({
    selector: 'app-datepicker',
    templateUrl: 'datepicker.component.pug'
})

export class DatePickerComponent {
    @Input() @Output() date: Date;

    constructor(private translateService: TranslateService) {
    }



    ngOnInit() {

        if (!this.date) {
            this.date = new Date();
        }
    }

    onChangeDay(day) {
        this.date.setDate(+day);
    }

    onChangeMonth(month) {
        this.date.setMonth(+month);
    }

    onChangeYear(year) {
        this.date.setFullYear(+year);
    }
}