/**
 * Created by bdrosatos on 31/10/2016.
 */
export abstract class AbstractService {
    abstract create(Object: any)
    abstract update(Object: any)
    abstract getAll(searchCriteria: any)
    abstract getList(pageNumber: number, searchCriteria: any)
    abstract getById(id: String)
    abstract deleteById(id: String)

    public static get API_ENDPOINT(): string { return 'http://localhost:5035/api'; }

}