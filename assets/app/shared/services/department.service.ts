/**
 * Created by bdros on 27-Oct-16.
 */
import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";

import {Observable} from "rxjs";
import {ToastService} from "../toasts/shared/toast.service";
import {AbstractService} from "../abstract.service";
import {Department} from "../models/departent.model";

@Injectable()
export class DepartmentService extends AbstractService{
    constructor(private http: Http, private toastService: ToastService) {
        super();
    }

    create(department: Department) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        const body = {
            name: department.name,
            shortName: department.shortName,
            information: department.information || ''
        };
        return this.http.post(`${AbstractService.API_ENDPOINT}/departments/university/${department.university.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    update(department: Department) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        const body = {
            name: department.name,
            shortName: department.shortName,
            information: department.information || '',
            universityId: department.university.id
        };
        return this.http.patch(`${AbstractService.API_ENDPOINT}/departments/${department.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getAll(searchCriteria: string) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/departments/university/${searchCriteria ? searchCriteria : ''}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getList(pageNumber: number, searchCriteria: Department) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/departments?page=${pageNumber}&name=${searchCriteria.name ? searchCriteria.name : ''}&shortName=${searchCriteria.shortName ? searchCriteria.shortName : ''}&uniId=${searchCriteria.university ? searchCriteria.university.id : ''}`
                            , {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getById(id: String) {

        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/departments/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteById(id: String){
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.delete(`${AbstractService.API_ENDPOINT}/departments/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getMyDepartment() {

        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        return this.http.get(`${AbstractService.API_ENDPOINT}/departments/my`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }
}