import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";

import {Observable} from "rxjs";
import {ToastService} from "../toasts/shared/toast.service";
import {AbstractService} from "../abstract.service";
import {Course} from "../models/course.model";
import {Exam} from "../models/exam.model";
import {Reply} from "../models/reply.model";


@Injectable()
export class ReplyService extends AbstractService {

    constructor(private http: Http, private toastService: ToastService) {
        super();
    }

    create(reply: Reply) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        headers.delete("Content-Type");
        let formData : FormData = new FormData();

        formData.append('topicNumber', reply.topicNumber);
        formData.append('fileType', reply.fileType);
        formData.append('textContent', reply.textContent);

        if(reply.files){
            for (let i = 0; i < reply.files.length; i++) {
                let file = reply.files[i];
                formData.append('files', file, file.name);
            }
        }


        return this.http.post(`${AbstractService.API_ENDPOINT}/replies/exam/${reply.exam.id}`, formData, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    update(course: Course) {
        //TODO
    }

    getAll(searchCriteria: any) {
        //TODO
    }

    getList(pageNumber: number, searchCriteria: Reply) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/replies?page=${pageNumber}&examId=${searchCriteria.exam.id}&topicNumber=${searchCriteria.topicNumber}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/replies/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    addUpvote(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.patch(`${AbstractService.API_ENDPOINT}/replies/add/upvote/${id}`, {}, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    removeUpvote(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.patch(`${AbstractService.API_ENDPOINT}/replies/remove/upvote/${id}`, {}, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    markApprove(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.patch(`${AbstractService.API_ENDPOINT}/replies/mark/approve/${id}`, {}, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    unMarkApprove(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.patch(`${AbstractService.API_ENDPOINT}/replies/unmark/approve/${id}`, {}, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.delete(`${AbstractService.API_ENDPOINT}/replies/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

}