import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";

import {Observable} from "rxjs";
import {ToastService} from "../toasts/shared/toast.service";
import {AbstractService} from "../abstract.service";
import {Course} from "../models/course.model";


@Injectable()
export class CourseService extends AbstractService {

    constructor(private http: Http, private toastService: ToastService) {
        super();
    }

    create(course: Course) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        const body = {
            name: course.name,
            courseType: course.courseType,
            information: course.information || ''

        };
        return this.http.post(`${AbstractService.API_ENDPOINT}/courses/department/${course.department.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    update(course: Course) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        const body = {
            name: course.name,
            courseType: course.courseType,
            information: course.information || '',
            departmentId: course.department.id
        };
        return this.http.patch(`${AbstractService.API_ENDPOINT}/courses/${course.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getAll(searchCriteria: any) {
        return null;
    }

    getList(pageNumber: number, searchCriteria: Course) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/courses?page=${pageNumber}&depId=${searchCriteria.department.id}&name=${searchCriteria.name ? searchCriteria.name : ''}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/courses/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.delete(`${AbstractService.API_ENDPOINT}/courses/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    setFollow(id: String){
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.patch(`${AbstractService.API_ENDPOINT}/profiles/follow/course/${id}`,null, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    removeFollow(id: String){
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.patch(`${AbstractService.API_ENDPOINT}/profiles/unfollow/course/${id}`,null, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }
}