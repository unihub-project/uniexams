/**
 * Created by bdros on 27-Oct-16.
 */
import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs";
import {ToastService} from "../toasts/shared/toast.service";
import {University} from "../models/university.model";
import {AbstractService} from "../abstract.service";

@Injectable()
export class UniversityService extends AbstractService {
    constructor(private http: Http, private toastService: ToastService) {
        super();
    }

    create(university: University){
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        const body = {
            name: university.name,
            shortName: university.shortName,
            information: university.information || ''
        };
        return this.http.post(`${AbstractService.API_ENDPOINT}/universities`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    update(university: University) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        const body = {
            name: university.name,
            shortName: university.shortName,
            information: university.information || ''
        };
        return this.http.patch(`${AbstractService.API_ENDPOINT}/universities/${university.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getAll() {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/universities`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getList(pageNumber: number, searchCriteria: University) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/universities?page=${pageNumber}&name=${searchCriteria.name ? searchCriteria.name : ''}&shortName=${searchCriteria.shortName ? searchCriteria.shortName : '' }`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getById(id: String) {

        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/universities/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.delete(`${AbstractService.API_ENDPOINT}/universities/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }
}