/**
 * Created by stavr on 12-Jan-17.
 */


import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs";
import {ToastService} from "../toasts/shared/toast.service";
import {University} from "../models/university.model";
import {AbstractService} from "../abstract.service";
import {User} from "../models/user.model";

@Injectable()
export class UserService extends AbstractService {
    constructor(private http: Http, private toastService: ToastService) {
        super();
    }

    create(university: University) {
        // const headers = new Headers({'Authorization': localStorage.getItem('token')});
        //
        // const body = {
        //     name: university.name,
        //     shortName: university.shortName,
        //     information: university.information || ''
        // };
        // return this.http.post(`${AbstractService.API_ENDPOINT}/universities`, body, {headers: headers})
        //     .map((response: Response) => response.json())
        //     .catch((error: any) => {
        //         this.toastService.handleError(error.json());
        //         return Observable.throw(error.json());
        //     });
    }

    update(user: User) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        const body = {
            email: user.email,
            isConfirmed: user.isConfirmed,
            role: user.role,
        };
        return this.http.patch(`${AbstractService.API_ENDPOINT}/users/${user.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getAll() {
        // const headers = new Headers({'Authorization': localStorage.getItem('token')});
        // return this.http.get(`${AbstractService.API_ENDPOINT}/profiles`, {headers: headers})
        //     .map((response: Response) => response.json())
        //     .catch((error: any) => {
        //         this.toastService.handleError(error.json());
        //         return Observable.throw(error.json());
        //     });
    }

    getList(pageNumber: number, searchCriteria) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/users?page=${pageNumber}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getById(id: String) {

        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/users/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.delete(`${AbstractService.API_ENDPOINT}/users/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }
}