import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";

import {Observable} from "rxjs";
import {ToastService} from "../toasts/shared/toast.service";
import {AbstractService} from "../abstract.service";
import {Comment} from "../models/comment.model";


@Injectable()
export class CommentService extends AbstractService {

    constructor(private http: Http, private toastService: ToastService) {
        super();
    }

    create(comment: Comment) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});

        headers.append('Accept', 'application/json');

        const body = {
            content: comment.content
        };

        return this.http.post(`${AbstractService.API_ENDPOINT}/comments/reply/${comment.reply.id}`, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    update(comment: Comment) {
        //TODO
    }

    getAll(searchCriteria: any) {
        //TODO
    }

    getList(pageNumber: number, searchCriteria: Comment) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/comments?page=${pageNumber}&replyId=${searchCriteria.reply.id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/comments/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }


    deleteById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.delete(`${AbstractService.API_ENDPOINT}/comments/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

}