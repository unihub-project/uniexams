import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";

import {Observable} from "rxjs";
import {ToastService} from "../toasts/shared/toast.service";
import {AbstractService} from "../abstract.service";
import {Course} from "../models/course.model";
import {Exam} from "../models/exam.model";


@Injectable()
export class ExamService extends AbstractService {

    constructor(private http: Http, private toastService: ToastService) {
        super();
    }

    create(exam: Exam) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        headers.delete("Content-Type");

        let formData : FormData = new FormData();

        formData.append('year', exam.year);
        formData.append('examType', exam.examType);
        formData.append('fileType', exam.fileType);
        formData.append('textContent', exam.textContent);
        formData.append('information', exam.information);
        formData.append('numberOfTopics', exam.numberOfTopics);

        if(exam.files){
            for (let i = 0; i < exam.files.length; i++) {
                let file = exam.files[i];
                formData.append('files', file, file.name);
            }
        }


        return this.http.post(`${AbstractService.API_ENDPOINT}/exams/course/${exam.course.id}`, formData, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    update(course: Course) {
        //TODO
    }

    getAll(searchCriteria: any) {
        //TODO
    }

    getList(pageNumber: number, searchCriteria: Exam) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/exams?page=${pageNumber}&courseId=${searchCriteria.course.id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: any) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.get(`${AbstractService.API_ENDPOINT}/exams/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteById(id: String) {
        const headers = new Headers({'Authorization': localStorage.getItem('token')});
        return this.http.delete(`${AbstractService.API_ENDPOINT}/exams/${id}`, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.toastService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

}