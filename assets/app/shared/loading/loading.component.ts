/**
 * Created by bdros on 29-Oct-16.
 */
import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-loading',
    templateUrl: 'loading.component.pug',
    styleUrls: ['loading.component.sass']
})

export class LoadingComponent  {
    @Input() loading: boolean;
}