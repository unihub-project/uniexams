/**
 * Created by stavr on 10/30/2016.
 */

export class AppUtils {
    public static getDataForSelect(data: any[], param: string): any[] {
        let dataForSelect: any[] = [];

        data.forEach((element) => {
            dataForSelect.push({
                id: element._id,
                text: element[param],
            });
        });

        return dataForSelect;
    }
}