import {NgModule, ModuleWithProviders} from "@angular/core";
import {TranslateModule} from "ng2-translate";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";
import {ToastModule} from "ng2-toastr";
import {SelectModule} from "ng2-select";
import {ConfirmModule} from "angular2-bootstrap-confirm";
import {PaginationModule} from "./pagination/pagination.module";
import {LoadingModule} from "./loading/loading.module";
import {DatepickerModule} from "ng2-bootstrap/datepicker";
import {CollapseModule} from "ng2-bootstrap/collapse";
import {DatePickerModule} from "./datepicker/datepicker.module";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        DatepickerModule,
        CollapseModule,
        TranslateModule,
        ToastModule,
        LoadingModule,
        PaginationModule,
        ConfirmModule,
        SelectModule,
        DatePickerModule,

    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        DatepickerModule,
        CollapseModule,
        TranslateModule,
        ToastModule,
        LoadingModule,
        PaginationModule,
        ConfirmModule,
        SelectModule,
        DatePickerModule
    ],


})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}