/**
 * Created by bdros on 27-Oct-16.
 */
export class University {
    constructor(public name: string,
                public shortName: string,
                public information?: string,
                public id?: string,
                public departmentsId? : string[]
    ) {}
}