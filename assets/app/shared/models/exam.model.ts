import {Course} from "./course.model";
import {Profile} from "./profile.model";

/**
 * Created by stavr on 12/14/2016.
 */


export class Exam {
    constructor(
                public id ?: string,
                public year ?: string,
                public examType ?: string,
                public fileType ?: string,
                public filePath ?: string[],
                public files?: FileList,
                public textContent?: string,
                public information ?: string,
                public numberOfTopics ?: number,
                public course ?: Course,
                public profile ?: Profile) {}
}