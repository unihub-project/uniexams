export class User {
    constructor(public email: string,
                public password?: string,
                public id?: string,
                public profileId?: string,
                public role?: string,
                public isConfirmed? : boolean
    ) {}
}