import {Profile} from "./profile.model";
import {Exam} from "./exam.model";
/**
 * Created by stavros on 20/2/2017.
 */

export class Reply {
    constructor(public id ?: string,
                public exam?: Exam,
                public topicNumber ?: number,
                public profile ?: Profile,
                public upvotes ?: number,
                public upvoteUsers ?: Array<string>,
                public isApproved ?: boolean,
                public fileType ?: string,
                public filePath ?: string[],
                public files?: FileList,
                public textContent?: string,
                public createdAt?: any) {
    }
}