import {University} from "./university.model";
/**
 * Created by bdros on 27-Oct-16.
 */
export class Department {
    constructor(public name?: string,
                public shortName?: string,
                public information?: string,
                public id?: string,
                public university?: University,
                public courses?: string[]
    ) {}
}