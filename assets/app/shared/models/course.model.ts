import {Department} from "./departent.model";
import {Exam} from "./exam.model";
/**
 * Created by stavr on 12/13/2016.
 */

export class Course {
    constructor(public name?: string,
                public courseType?: string,
                public information?: string,
                public id?: string,
                public exams?: Exam[],
                public department?: Department,
                public examsCount?: number,
                public isFollow?: boolean
    ) {}
}