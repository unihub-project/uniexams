import {Profile} from "./profile.model";
import {Reply} from "./reply.model";
/**
 * Created by stavros on 20/2/2017.
 */

export class Comment {
    constructor(public id ?: string,
                public content ?: string,
                public reply?: Reply,
                public profile ?: Profile,
                public createdAt?: any) {
    }
}