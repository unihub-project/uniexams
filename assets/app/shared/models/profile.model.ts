import {User} from "./user.model";
/**
 * Created by bdros on 25-Oct-16.
 */
export class Profile {
    constructor(public firstName?: string,
                public defaultDepartment?: string,
                public lastName?: string,
                public id?: string,
                public birthDate?: Date,
                public user ?: User) {
    }
}