import {Component, ViewContainerRef} from "@angular/core";
import {TranslateService} from "ng2-translate";
import {LanguageService} from "./language.service";
import {ToastsManager} from "ng2-toastr";

@Component({
    selector: 'my-app',
    templateUrl: 'app.component.pug'
})
export class AppComponent {
    _opened: boolean = false;

    constructor(public translateService: TranslateService, public languageService: LanguageService, public toastr: ToastsManager, vRef: ViewContainerRef) {
        translateService.addLangs(["en", "el"]);
        // this language will be used as a fallback when a translation isn't found in the current language
        translateService.setDefaultLang('en');

        // the lang to use, if the lang isn't available, it will use the current loader to get them
        const browserLang = translateService.getBrowserLang();
        translateService.use(browserLang.match(/en|el/) ? browserLang : 'en');
        this.toastr.setRootViewContainerRef(vRef);


    }

    _toggleSidebar() {
        this._opened = !this._opened;
    }

    onLangChange(language: string) {
        this.translateService.use(language);
        this.languageService.changeLanguage(language)
            .subscribe((data) => console.log(data), (error) => console.log(error))
    }

    getImgSrc(language: string){
        return `../images/country-flags/${language}.png`;
    }

}