/**
 * Created by bdrosatos on 27/10/2016.
 */
import {Component, OnInit, OnDestroy} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {User} from "./shared/models/user.model";
import {TranslateService} from "ng2-translate";
import {ToastService} from "./shared/toasts/shared/toast.service";

@Component({
    selector: 'app-home',
    template: `
      <div class="container">
        <h1>{{ "HOME.WELCOME_MESSAGE" | translate }}</h1>
      </div>
    `
})
export class HomeComponent implements OnInit, OnDestroy {
    private subscription: Subscription;

    constructor(private activatedRoute: ActivatedRoute, private translateService: TranslateService, private toastService: ToastService) {
    }

    ngOnInit() {
        this.subscription = this.activatedRoute.queryParams
            .subscribe(
                (queryParam: any) => {
                    const token = queryParam['token'];
                    console.log(token);
                    if (token) {
                        localStorage.setItem('token', token);
                        const user: User = new User(queryParam['email'], null, queryParam['userId'], queryParam['role']);
                        user.profileId = queryParam['profileId'];

                        localStorage.setItem('user', JSON.stringify(user));

                        this.translateService.get("SIGNIN.SUCCESS_MESSAGE")
                            .subscribe((succesMessage: string) => {
                                this.toastService.handleSuccess({
                                    title: "Facebook",
                                    message: succesMessage,
                                });

                            })
                    }
                    const message = queryParam['message'];
                    if (message) {
                        this.toastService.handleError({
                            title: queryParam['title'],
                            error: {message: queryParam['message'] }
                        });
                    }
                }
            );
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


}