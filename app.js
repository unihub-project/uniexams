"use strict";

const express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    morgan = require('morgan'),
    cookieParser = require('cookie-parser'),
    RateLimit = require('express-rate-limit'),
    bodyParser = require('body-parser'),
    i18n = require("i18n"),
    logger = require('noogger'),
    config = require('./app/config/app');

//ROUTES
const appRoutes = require('./app/routes/app.route'),
    userRoutes = require('./app/routes/user.route'),
    profileRoutes = require('./app/routes/profile.route'),
    departmentRoutes = require('./app/routes/department.route'),
    universityRoutes = require('./app/routes/university.route'),
    courseRoutes = require('./app/routes/course.route'),
    examRoutes = require('./app/routes/exam.route'),
    replyRoutes = require('./app/routes/reply.route'),
    commentRoutes = require('./app/routes/comment.route');

//TEST ROUTES
const testRoutes = require('./app/routes/test.route');

//i18 use for translations based on browser locale (english, greek)
i18n.configure({
    locales: ['en', 'el'],
    directory: __dirname + '/app/locales',
    defaultLocale: 'el'
});

//Logger initialization
logger.init({
    consoleOutput: true,
    consoleOutputLevel: ['DEBUG', 'ERROR', 'WARNING'],

    dateTimeFormat: "DD-MM-YYYY HH:mm:ss.S",
    outputPath: "logs/",
    fileNameDateFormat: "DD-MM-YYYY",
    fileNamePrefix: "api-"
});

// app.enable('trust proxy'); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS if you use an ELB, custom Nginx setup, etc)

const apiLimiter = new RateLimit({
    windowMs: 5*60*1000, // 5 mins
    max: 300, // limit each IP to 300 api requests per 5 mins
    delayMs: 0, // disable delaying - full speed until the max limit is reached,
    handler: function (req, res) {
        return res.status(429).json(
            {
                title: '',
                error: { message: i18n.__("TOO_MANY_REQUESTS") }
            });
    }

});

let app = express();

app.disable('x-powered-by');

app.use(i18n.init);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// //CORS setup
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});


//  apply to all requests
app.use('/api/', apiLimiter);

//Routes
app.use('/api/users', userRoutes);
app.use('/api/profiles', profileRoutes);
app.use('/api/universities', universityRoutes);
app.use('/api/departments', departmentRoutes);
app.use('/api/courses', courseRoutes);
app.use('/api/exams', examRoutes);
app.use('/api/replies', replyRoutes);
app.use('/api/comments', commentRoutes);
app.use('/*', appRoutes);

//Test router
app.use('/test', testRoutes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            title: err,
            error: {message: err.message}
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
    res.status(err.status || 500);
});


module.exports = app;
